<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="gmpg.org/xfn/11" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
	<?php translogistic_page_loading(); ?>
	<?php translogistic_site_content_start(); ?>
	<?php $header_fixed = get_theme_mod( 'header_fixed' ); ?>
		<header class="header-wrapper<?php if( $header_fixed == '1' ) : echo " fixed-header"; endif; ?>">
			<div class="menu-area">
				<div class="container">
					<?php echo do_shortcode('[google-translator]'); ?>
					
<!--					-->
<!--					<nav class="navbar">-->
<!--						--><?php //wp_nav_menu( array( 'menu' => 'usermenu', 'theme_location' => 'usermenu', 'depth' => 5, 'container' => 'div', 'container_class' => 'collapse navbar-collapse', 'container_id' => 'bs-example-navbar-collapse-1', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => 'translogistic_walker::fallback', 'walker' => new translogistic_walker()) ); ?>
<!--					</nav>-->
<!--					-->
					<div class="social-container">
                        <?php translogistic_header_social_media_links(); ?>
						
                        <div class="reg-block" style="float: right">
	                        <?php if ( !is_user_logged_in() ): ?>
                                <a class="login" href="<?php echo get_home_url() ?>/sample-page">Login</a>
                                <span>/</span>
                                <a class="login" href="<?php echo get_home_url() ?>/user-test">Register</a>
                            <?php else: ?>
                                <a class="login" href="<?php echo get_home_url() ?>/user-test-2">Account</a>
	                        <?php endif;?>
                        </div>
                    </div>
				</div>
			</div>
			<div class="logo-area">
				<div class="container position">
                    <nav class="navbar">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only"><?php echo esc_html_e( 'Toggle Navigation', 'translogistic' ); ?></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                    </nav>
					<?php translogistic_site_logo(); ?>
					
					<?php wp_nav_menu( array( 'menu' => 'mainmenu', 'theme_location' => 'mainmenu', 'depth' => 5, 'container' => 'div', 'container_class' => 'collapse navbar-collapse', 'container_id' => 'bs-example-navbar-collapse-1', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => 'translogistic_walker::fallback', 'walker' => new translogistic_walker()) ); ?>
     
<!--					--><?php //translogistic_header_contact(); ?>
<!--					--><?php //translogistic_header_search(); ?>
					<?php if ( !is_user_logged_in() ): ?>
                    <a href="<?php echo get_home_url() ?>/user-test" class="register">Register</a>
					<?php endif;?>
				</div>
			</div>
			
		</header>
		<?php translogistic_wrapper_before(); ?>