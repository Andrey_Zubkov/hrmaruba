<?php
/*
	* The template for displaying page
*/
get_header(); ?>

	<?php
		while ( have_posts() ) : the_post();
		
			$values_layout_select = get_post_custom( get_the_ID() );
			
			if ( has_post_thumbnail( $post->ID ) ) :
				$page_banner_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'translogistic-page-banner' );
				$page_banner_image = ' style=background-image:url(' . esc_url( $page_banner_image[0] ) . ');';
			else :
				$page_banner_image = "";
			endif;
			
			$hide_page_title = isset( $values_layout_select['page_title_hide'] ) ? strip_tags( esc_attr( $values_layout_select['page_title_hide'][0] ) ) :'';
			$full_width_page = isset( $values_layout_select['full_width_page'] ) ? strip_tags( esc_attr( $values_layout_select['full_width_page'][0] ) ) :'';
			$page_title_excerpt = isset( $values_layout_select['page_title_excerpt'] ) ? $values_layout_select['page_title_excerpt'][0] :'';
	?>
	
			<?php translogistic_site_sub_content_start(); ?>
				
				<?php if ( has_post_thumbnail() ) : ?>
					<div class="page-content-banner"<?php echo esc_attr( $page_banner_image ); ?>></div>
				<?php endif; ?>
				
				<?php if( !$hide_page_title == "on" ) : ?>
					<div class="page-title-wrapper">
						<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
						<?php if( !empty( $page_title_excerpt ) ): ?><p><?php echo $page_title_excerpt; ?></p><?php endif; ?>
					</div>
				<?php endif; ?>
				
				<?php if( !$full_width_page == "on" ) : ?>
					<div class="container">
				<?php endif; ?>
					<?php if( !$full_width_page == "on" ) : ?>
						<?php translogistic_row_before(); ?>
					<?php endif; ?>
						
							<?php translogistic_post_content_area_start(); ?>
							
								<div class="page-content">
									<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>										
										
										<div class="page-content-bottom">
											<?php the_content(); ?>
										</div>
										
										<?php
											wp_link_pages( array(
												'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'translogistic' ) . '</span>',
												'after'       => '</div>',
												'link_before' => '<span>',
												'link_after'  => '</span>',
											) );
											edit_post_link( esc_html__( 'Edit Page', 'translogistic' ), '<span class="edit-link">', '</span>' );
										?>
									</article>
								</div>
								
								<?php
									if ( comments_open() || get_comments_number() ) {
										comments_template();
									}
								?>
								
							<?php translogistic_content_area_end(); ?>
							
							<?php translogistic_post_sidebar_start(); ?>
								<?php if ( is_active_sidebar( 'general-sidebar' ) ) : ?>
									<div class="sidebar-general sidebar">
										<?php dynamic_sidebar( 'general-sidebar' ); ?>
									</div>
								<?php endif; ?>
							<?php translogistic_sidebar_end(); ?>
								
					<?php if( !$full_width_page == "on" ) : ?>
						<?php translogistic_row_after(); ?>
					<?php endif; ?>
					
				<?php if( !$full_width_page == "on" ) : ?>
					</div>
				<?php endif; ?>
			
			<?php translogistic_site_sub_content_end(); ?>
		
	<?php endwhile; ?>
<?php get_footer();