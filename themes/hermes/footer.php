		<?php
		$hide_footer = get_theme_mod( 'hide_footer' );
		if( !$hide_footer == '1' ) :
		?>
			<footer class="footer">
				<?php translogistic_footer_top_widget(); ?>
				<div class="footer-bottom">
					<?php translogistic_footer_bottom_widget(); ?>
					<?php translogistic_go_top(); ?>
				</div>
			</footer>
		<?php endif; ?>
		<?php translogistic_wrapper_after(); ?>
	<?php translogistic_site_content_end(); ?>
	<?php wp_footer(); ?>
	</body>
</html>