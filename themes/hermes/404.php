<?php
/*
	* The template for displaying 404 page
*/
get_header(); ?>

	<?php translogistic_site_sub_content_start(); ?>
	
	<?php translogistic_page_content_banner(); ?>
		
		<div class="page-content">
			<article class="page page404">
				<div class="content404">
					<h1><?php echo esc_html_e( '404', 'translogistic' ); ?></h1>
					<h2><?php echo esc_html_e( 'page', 'translogistic' ); ?></h2>
					<p class="top-desc"><?php echo esc_html__( 'sorry that ', 'translogistic' ); ?><span><?php echo esc_html__( 'page does not ', 'translogistic' ); ?></span><?php echo esc_html__( 'exist', 'translogistic' ); ?></p>
					<p class="bottom-desc"><?php echo esc_html__( 'The page you are looking for can not be found. Go home by ', 'translogistic' ); ?><a href="<?php echo home_url(); ?>"><?php echo esc_html__( 'clicking here.', 'translogistic' ); ?></a></p>
				</div>
			</article>
		</div>
		
	<?php translogistic_site_sub_content_end(); ?>
	
<?php get_footer();