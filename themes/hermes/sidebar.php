<?php translogistic_sidebar_start(); ?>
	<?php if ( is_active_sidebar( 'general-sidebar' ) ) : ?>
		<div class="sidebar-general sidebar">
			<?php dynamic_sidebar( 'general-sidebar' ); ?>
		</div>
	<?php endif; ?>
<?php translogistic_sidebar_end(); ?>