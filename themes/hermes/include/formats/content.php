<?php
/*
	* The template used for displaying single content
*/
?>

<div class="category-post-list post-list single-list">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="post-wrapper">
			<div class="post-header">
				<h2><?php the_title(); ?></h2>
				<div class="category"><?php the_category( '', '' ); ?></div>
				<?php $hide_post_information = get_theme_mod( 'hide_post_information' ); ?>
				<?php if ( !$hide_post_information == '1' ) : ?>
					<ul class="post-information">
						<li class="author"><?php the_author_posts_link(); ?></li>
						<li class="date"><?php the_time( get_option( 'date_format' ) ); ?></li>
						<li class="comment"><a href="<?php the_permalink(); ?>#comments" title="<?php the_title_attribute(); ?>"><?php comments_number( esc_html__( '0 Comment', 'translogistic' ), esc_html__( '1 Comment', 'translogistic' ), esc_html__( '% Comments', 'translogistic' ) ); ?></a></li>
					</ul>
				<?php endif; ?>
			</div>
			<div class="post-content">
				<?php 
				$hide_post_featured_image = get_theme_mod( 'hide_post_featured_image' );
				if( !$hide_post_featured_image == '1' ) :
				?>
					<?php if ( has_post_thumbnail() ) : ?>
						<div class="post-image">
							<?php the_post_thumbnail( 'translogistic-blog-big-image' ); ?>
						</div>
					<?php endif; ?>
				<?php endif; ?>
				
				<?php the_content(); ?>
				
			</div>
			<?php
				$hide_post_tags = get_theme_mod( 'hide_post_tags' );
				$hide_post_share = get_theme_mod( 'hide_post_share' );
				if( !$hide_post_tags == '1' or !$hide_post_share == '1' ) :
			?>
				<div class="post-bottom">
					<?php
						wp_link_pages( array(
							'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'translogistic' ) . '</span>',
							'after'       => '</div>',
							'link_before' => '<span>',
							'link_after'  => '</span>',
						) );
					?>
					<?php if ( !$hide_post_tags == '1' ) : ?>
						<?php $tags_title = '' . esc_html__( 'Tags:', 'translogistic' ); ?>
						<?php the_tags( '<div class="single-tag-list"><span class="single-tag-list-title">' . $tags_title . '</span><span>', ', </span><span>', '</span></div>' ); ?>
					<?php endif; ?>
					<?php if( !$hide_post_share == '1' ) : ?>
						<?php translogistic_general_post_social_share(); ?>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		</div>
	</article>
</div>

<?php translogistic_related_posts(); ?>

<?php translogistic_single_nav(); ?>