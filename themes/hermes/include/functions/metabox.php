<?php
/*------------- PAGE LAYOUT META BOX -------------*/
function translogistic_layout_select_meta_box_add()
{
	add_meta_box( 'my-meta-box-id_layout_select', esc_html__( 'Layout Settings', 'translogistic' ), 'translogistic_layout_select_meta_box_layout_select', 'page', 'side', 'low' );
	add_meta_box( 'my-meta-box-id_layout_select', esc_html__( 'Layout Settings', 'translogistic' ), 'translogistic_layout_select_meta_box_layout_select', 'post', 'side', 'low' );
	add_meta_box( 'my-meta-box-page_title_excerpt', esc_html__( 'Page Title Excerpt', 'translogistic' ), 'translogistic_page_title_excerpt', 'page', 'normal', 'low' );
}
add_action( 'add_meta_boxes', 'translogistic_layout_select_meta_box_add' );

function translogistic_layout_select_meta_box_layout_select()
{
    // $post is already set, and contains an object: the WordPress post
    global $post;
    $values_layout_select = get_post_custom( $post->ID );
	$layout_select = isset( $values_layout_select['layout_select_meta_box_text'] ) ? strip_tags( esc_attr( $values_layout_select['layout_select_meta_box_text'][0] ) ) :'';
	$header_select = isset( $values_layout_select['header_layout_select_meta_box_text'] ) ? strip_tags( esc_attr( $values_layout_select['header_layout_select_meta_box_text'][0] ) ) :'';
	$page_title_hide_check = isset( $values_layout_select['page_title_hide'] ) ? strip_tags( esc_attr( $values_layout_select['page_title_hide'][0] ) ) :'';
	$full_width_page_check = isset( $values_layout_select['full_width_page'] ) ? strip_tags( esc_attr( $values_layout_select['full_width_page'][0] ) ) :'';
	$hide_top_widget_footer_check = isset( $values_layout_select['hide_top_widget_footer'] ) ? strip_tags( esc_attr( $values_layout_select['hide_top_widget_footer'][0] ) ) :'';
	$bottom_footer_hide_widget_check = isset( $values_layout_select['bottom_footer_hide_widget'] ) ? strip_tags( esc_attr( $values_layout_select['bottom_footer_hide_widget'][0] ) ) :'';
    if( $layout_select == "" ) {
		$layout_select = '';
	}
    // We'll use this nonce field later on when saving.
    wp_nonce_field( 'layout_select_meta_box_nonce', 'meta_box_nonce' );
	
	if($page_title_hide_check == "on") : $page_title_hide_checked = 'checked="checked"'; else: $page_title_hide_checked = ''; endif;
	if($full_width_page_check == "on") : $full_width_page_checked = 'checked="checked"'; else: $full_width_page_checked = ''; endif;
	if($hide_top_widget_footer_check == "on") : $hide_top_widget_footer_checked = 'checked="checked"'; else: $hide_top_widget_footer_checked = ''; endif;
	if($bottom_footer_hide_widget_check == "on") : $bottom_footer_hide_widget_checked = 'checked="checked"'; else: $bottom_footer_hide_widget_checked = ''; endif;
	?>
	
	<div class="metabox-group-title"><?php echo esc_html__( 'Sidebar', 'translogistic' ); ?></div>
	<div class="translogistic-metabox-group metabox-group-sidebar">
		<div class="metabox-group-sub-title"><?php echo esc_html__( 'Sidebar Style', 'translogistic' ); ?></div>
		<div class="metabox-group-content">
			<p>
				<select name='layout_select_meta_box_text'>
					<option value="" <?php if ( $layout_select == "" ) : echo "selected"; endif; ?>><?php echo esc_html__( 'Sidebar Select', 'translogistic' ); ?></option>
					<option value="fullwidth" <?php if ( $layout_select == "fullwidth" ) : echo "selected"; endif; ?>><?php echo esc_html__( 'Sidebar None', 'translogistic' ); ?></option>
					<option value="leftsidebar" <?php if ( $layout_select == "leftsidebar" ) : echo "selected"; endif; ?>><?php echo esc_html__( 'Left Sidebar', 'translogistic' ); ?></option>
					<option value="rightsidebar" <?php if ( $layout_select == "rightsidebar" ) : echo "selected"; endif; ?>><?php echo esc_html__( 'Right Sidebar', 'translogistic' ); ?></option>
				</select>
			</p>
			
			<p>
				<input type="checkbox" class="checkbox" <?php echo esc_attr( $full_width_page_checked ); ?> id="full_width_page" name="full_width_page" />
				<label for="full_width_page"><?php echo esc_html__( 'Full Width Page', 'translogistic' ); ?></label>
			</p>
		</div>
	</div>
	
	<div class="metabox-group-title is-single-none"><?php echo esc_html__( 'Header', 'translogistic' ); ?></div>
	<div class="translogistic-metabox-group metabox-group-header">
		<div class="metabox-group-content">			
			<p>
				<input type="checkbox" class="checkbox" <?php echo esc_attr( $page_title_hide_checked ); ?> id="page_title_hide" name="page_title_hide" />
				<label for="page_title_hide"><?php echo esc_html__( 'Page Title Hide', 'translogistic' ); ?></label>
			</p>
		</div>
	</div>
	
	<div class="metabox-group-title is-single-none"><?php echo esc_html__( 'Footer', 'translogistic' ); ?></div>
	<div class="translogistic-metabox-group metabox-group-footer">
		<div class="metabox-group-content">
			<p>
				<input type="checkbox" class="checkbox" <?php echo esc_attr( $hide_top_widget_footer_checked ); ?> id="hide_top_widget_footer" name="hide_top_widget_footer" />
				<label for="hide_top_widget_footer"><?php echo esc_html__( 'Hide Top Footer Widgets', 'translogistic' ); ?></label>
			</p>
			
			<p>
				<input type="checkbox" class="checkbox" <?php echo esc_attr( $bottom_footer_hide_widget_checked ); ?> id="bottom_footer_hide_widget" name="bottom_footer_hide_widget" />
				<label for="bottom_footer_hide_widget"><?php echo esc_html__( 'Hide Bottom Footer Widgets', 'translogistic' ); ?></label>
			</p>
		</div>
	</div>
	
    <?php    
}

function translogistic_page_title_excerpt()
{
    // $post is already set, and contains an object: the WordPress post
    global $post;
    $values = get_post_custom( $post->ID );
	
	$page_title_excerpt = isset( $values['page_title_excerpt'] ) ? stripcslashes( $values['page_title_excerpt'][0] ) :'';
    if( $page_title_excerpt == "" ) {
		$page_title_excerpt = '';
	}
	?>
	
	<input type="text" name="page_title_excerpt" value="<?php echo stripcslashes( $page_title_excerpt ); ?>" placeholder="<?php echo esc_html__( 'Page Title Excerpt', 'translogistic' ); ?>" style="width:100%;" />
	
    <?php    
}

function translogistic_layout_select_meta_box_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
     
    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['meta_box_nonce'] ) ) return;
     
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post', $post_id ) ) return;
     
    // now we can actually save the data
    $allowed_video = array( 
        'a' => array( // on allow a tags
            'href' => array() // and those anchors can only have href attribute
        )
    );
     
    // Make sure your data is set before trying to save it
    if( isset( $_POST['layout_select_meta_box_text'] ) )
        update_post_meta( $post_id, 'layout_select_meta_box_text', addslashes( $_POST['layout_select_meta_box_text'] ) );
	
    if( isset( $_POST['header_layout_select_meta_box_text'] ) )
        update_post_meta( $post_id, 'header_layout_select_meta_box_text', addslashes( $_POST['header_layout_select_meta_box_text'] ) );
	
	if ( isset($_POST['page_title_hide']) ) {
        update_post_meta( $post_id, 'page_title_hide', addslashes( $_POST['page_title_hide'] ) );
	} else {
		delete_post_meta($post_id, 'page_title_hide');
	}
	
	if ( isset($_POST['full_width_page']) ) {
        update_post_meta( $post_id, 'full_width_page', addslashes( $_POST['full_width_page'] ) );
	} else {
		delete_post_meta($post_id, 'full_width_page');
	}
	
	if ( isset($_POST['hide_top_widget_footer']) ) {
        update_post_meta( $post_id, 'hide_top_widget_footer', addslashes( $_POST['hide_top_widget_footer'] ) );
	} else {
		delete_post_meta($post_id, 'hide_top_widget_footer');
	}
	
	if ( isset($_POST['bottom_footer_hide_widget']) ) {
        update_post_meta( $post_id, 'bottom_footer_hide_widget', addslashes( $_POST['bottom_footer_hide_widget'] ) );
	} else {
		delete_post_meta($post_id, 'bottom_footer_hide_widget');
	}
         
    if( isset( $_POST['page_title_excerpt'] ) )
        update_post_meta( $post_id, 'page_title_excerpt', stripcslashes( $_POST['page_title_excerpt'] ) );

}
add_action( 'save_post', 'translogistic_layout_select_meta_box_save' );
/*------------- PAGE LAYOUT META BOX END -------------*/

/*------------- CARGO TRACKING START -------------*/
function translogistic_cargo_tracking_meta_box_add()
{
	add_meta_box( 'post_type_box_cargo_tracking', esc_html__( 'Cargo Tracking Meta', 'translogistic' ), 'translogistic_cargo_tracking_meta_box_cargo_tracking', 'translogistic_cargo', 'normal', 'high' );
}
add_action( 'add_meta_boxes', 'translogistic_cargo_tracking_meta_box_add' );

function translogistic_cargo_tracking_meta_box_cargo_tracking()
{
    // $post is already set, and contains an object: the WordPress post
    global $post;
    $values = get_post_custom( $post->ID );
	
	$cargo_tracking_code = isset( $values['cargo_tracking_code'] ) ? strip_tags( esc_attr( $values['cargo_tracking_code'][0] ) ) :'';
    if( $cargo_tracking_code == "" ) {
		$cargo_tracking_code = '';
	}
	
	$cargo_tracking_shipper_name = isset( $values['cargo_tracking_shipper_name'] ) ? strip_tags( esc_attr( $values['cargo_tracking_shipper_name'][0] ) ) :'';
    if( $cargo_tracking_shipper_name == "" ) {
		$cargo_tracking_shipper_name = '';
	}
	
	$cargo_tracking_shipper_tel = isset( $values['cargo_tracking_shipper_tel'] ) ? strip_tags( esc_attr( $values['cargo_tracking_shipper_tel'][0] ) ) :'';
    if( $cargo_tracking_shipper_tel == "" ) {
		$cargo_tracking_shipper_tel = '';
	}
	
	$cargo_tracking_shipper_email = isset( $values['cargo_tracking_shipper_email'] ) ? strip_tags( esc_attr( $values['cargo_tracking_shipper_email'][0] ) ) :'';
    if( $cargo_tracking_shipper_email == "" ) {
		$cargo_tracking_shipper_email = '';
	}
	
	$cargo_tracking_shipper_address = isset( $values['cargo_tracking_shipper_address'] ) ? strip_tags( esc_attr( $values['cargo_tracking_shipper_address'][0] ) ) :'';
    if( $cargo_tracking_shipper_address == "" ) {
		$cargo_tracking_shipper_address = '';
	}
	
	$cargo_tracking_shipping_from = isset( $values['cargo_tracking_shipping_from'] ) ? strip_tags( esc_attr( $values['cargo_tracking_shipping_from'][0] ) ) :'';
    if( $cargo_tracking_shipping_from == "" ) {
		$cargo_tracking_shipping_from = '';
	}
	
	$cargo_tracking_receiver_name = isset( $values['cargo_tracking_receiver_name'] ) ? strip_tags( esc_attr( $values['cargo_tracking_receiver_name'][0] ) ) :'';
    if( $cargo_tracking_receiver_name == "" ) {
		$cargo_tracking_receiver_name = '';
	}
	
	$cargo_tracking_receiver_tel = isset( $values['cargo_tracking_receiver_tel'] ) ? strip_tags( esc_attr( $values['cargo_tracking_receiver_tel'][0] ) ) :'';
    if( $cargo_tracking_receiver_tel == "" ) {
		$cargo_tracking_receiver_tel = '';
	}
	
	$cargo_tracking_receiver_email = isset( $values['cargo_tracking_receiver_email'] ) ? strip_tags( esc_attr( $values['cargo_tracking_receiver_email'][0] ) ) :'';
    if( $cargo_tracking_receiver_email == "" ) {
		$cargo_tracking_receiver_email = '';
	}
	
	$cargo_tracking_receiver_address = isset( $values['cargo_tracking_receiver_address'] ) ? strip_tags( esc_attr( $values['cargo_tracking_receiver_address'][0] ) ) :'';
    if( $cargo_tracking_receiver_address == "" ) {
		$cargo_tracking_receiver_address = '';
	}
	
	$cargo_tracking_shipping_delivery_place = isset( $values['cargo_tracking_shipping_delivery_place'] ) ? strip_tags( esc_attr( $values['cargo_tracking_shipping_delivery_place'][0] ) ) :'';
    if( $cargo_tracking_shipping_delivery_place == "" ) {
		$cargo_tracking_shipping_delivery_place = '';
	}
	
	$cargo_tracking_shipping_release_date = isset( $values['cargo_tracking_shipping_release_date'] ) ? strip_tags( esc_attr( $values['cargo_tracking_shipping_release_date'][0] ) ) :'';
    if( $cargo_tracking_shipping_release_date == "" ) {
		$cargo_tracking_shipping_release_date = '';
	}
	
	$cargo_tracking_shipping_release_hour = isset( $values['cargo_tracking_shipping_release_hour'] ) ? strip_tags( esc_attr( $values['cargo_tracking_shipping_release_hour'][0] ) ) :'';
    if( $cargo_tracking_shipping_release_hour == "" ) {
		$cargo_tracking_shipping_release_hour = '';
	}
	
	$cargo_tracking_shipping_delivery_date = isset( $values['cargo_tracking_shipping_delivery_date'] ) ? strip_tags( esc_attr( $values['cargo_tracking_shipping_delivery_date'][0] ) ) :'';
    if( $cargo_tracking_shipping_delivery_date == "" ) {
		$cargo_tracking_shipping_delivery_date = '';
	}
	
	$cargo_tracking_shipping_delivery_hour = isset( $values['cargo_tracking_shipping_delivery_hour'] ) ? strip_tags( esc_attr( $values['cargo_tracking_shipping_delivery_hour'][0] ) ) :'';
    if( $cargo_tracking_shipping_delivery_hour == "" ) {
		$cargo_tracking_shipping_delivery_hour = '';
	}
	
	$cargo_tracking_pcs = isset( $values['cargo_tracking_pcs'] ) ? strip_tags( esc_attr( $values['cargo_tracking_pcs'][0] ) ) :'';
    if( $cargo_tracking_pcs == "" ) {
		$cargo_tracking_pcs = '';
	}
	
	$cargo_tracking_weight = isset( $values['cargo_tracking_weight'] ) ? strip_tags( esc_attr( $values['cargo_tracking_weight'][0] ) ) :'';
    if( $cargo_tracking_weight == "" ) {
		$cargo_tracking_weight = '';
	}
	
	$cargo_tracking_height = isset( $values['cargo_tracking_height'] ) ? strip_tags( esc_attr( $values['cargo_tracking_height'][0] ) ) :'';
    if( $cargo_tracking_height == "" ) {
		$cargo_tracking_height = '';
	}
	
	$cargo_tracking_shipping_status = isset( $values['cargo_tracking_shipping_status'] ) ? strip_tags( esc_attr( $values['cargo_tracking_shipping_status'][0] ) ) :'';
    if( $cargo_tracking_shipping_status == "" ) {
		$cargo_tracking_shipping_status = '';
	}
	
	$cargo_tracking_product = isset( $values['cargo_tracking_product'] ) ? strip_tags( esc_attr( $values['cargo_tracking_product'][0] ) ) :'';
    if( $cargo_tracking_product == "" ) {
		$cargo_tracking_product = '';
	}
	
	$cargo_tracking_carrier = isset( $values['cargo_tracking_carrier'] ) ? strip_tags( esc_attr( $values['cargo_tracking_carrier'][0] ) ) :'';
    if( $cargo_tracking_carrier == "" ) {
		$cargo_tracking_carrier = '';
	}
	
	$cargo_tracking_freight = isset( $values['cargo_tracking_freight'] ) ? strip_tags( esc_attr( $values['cargo_tracking_freight'][0] ) ) :'';
    if( $cargo_tracking_freight == "" ) {
		$cargo_tracking_freight = '';
	}
	
	$cargo_tracking_cc = isset( $values['cargo_tracking_cc'] ) ? strip_tags( esc_attr( $values['cargo_tracking_cc'][0] ) ) :'';
    if( $cargo_tracking_cc == "" ) {
		$cargo_tracking_cc = '';
	}
	
	$barcode_image_url = isset( $values['barcode_image_url'] ) ? strip_tags( esc_attr( $values['barcode_image_url'][0] ) ) :'';
    if( $barcode_image_url == "" ) {
		$barcode_image_url = '';
	}
	
	$cargo_tracking_additional_note = isset( $values['cargo_tracking_additional_note'] ) ? strip_tags( esc_attr( $values['cargo_tracking_additional_note'][0] ) ) :'';
    if( $cargo_tracking_additional_note == "" ) {
		$cargo_tracking_additional_note = '';
	}
	
    wp_nonce_field( 'cargo_tracking_meta_box_nonce', 'meta_box_nonce' );
    ?>
	<div class="gloria-metabox-list">
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Tracking Code', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="text" name="cargo_tracking_code" placeholder="<?php echo esc_html__( 'Example: X145S8554', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_code ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Shipper Name', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="text" name="cargo_tracking_shipper_name" placeholder="<?php echo esc_html__( 'Example: John Doe', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_shipper_name ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Shipper Tel', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="tel" name="cargo_tracking_shipper_tel" placeholder="<?php echo esc_html__( 'Example: +88 0658 568 89', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_shipper_tel ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Shipper Email', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="tel" name="cargo_tracking_shipper_email" placeholder="<?php echo esc_html__( 'Example: cargo@gloriathemes.com', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_shipper_email ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Shipper Address', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="tel" name="cargo_tracking_shipper_address" placeholder="<?php echo esc_html__( 'Example: Walking Street, CA 159, California.', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_shipper_address ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Origin', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="text" name="cargo_tracking_shipping_from" placeholder="<?php echo esc_html__( 'Example: New York', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_shipping_from ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Receiver Name', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="text" name="cargo_tracking_receiver_name" placeholder="<?php echo esc_html__( 'Example: John Doe', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_receiver_name ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Receiver Tel', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="tel" name="cargo_tracking_receiver_tel" placeholder="<?php echo esc_html__( 'Example: +88 0658 568 89', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_receiver_tel ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Receiver Email', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="tel" name="cargo_tracking_receiver_email" placeholder="<?php echo esc_html__( 'Example: cargo@gloriathemes.com', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_receiver_email ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Receiver Address', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="tel" name="cargo_tracking_receiver_address" placeholder="<?php echo esc_html__( 'Example: Walking Street, CA 159, California.', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_receiver_address ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Destination', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="text" name="cargo_tracking_shipping_delivery_place" placeholder="<?php echo esc_html__( 'Example: Roma', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_shipping_delivery_place ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Cargo Release Date', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="text" name="cargo_tracking_shipping_release_date" placeholder="<?php echo esc_html__( 'Example: 12.06.2016', 'translogistic' ); ?>" class="datecalendar" value="<?php echo esc_attr( $cargo_tracking_shipping_release_date ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Cargo Release Hour', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="text" name="cargo_tracking_shipping_release_hour" placeholder="<?php echo esc_html__( 'Example: 13:02', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_shipping_release_hour ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Cargo Delivery Date', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="text" name="cargo_tracking_shipping_delivery_date" placeholder="<?php echo esc_html__( 'Example: 15.06.2016', 'translogistic' ); ?>" class="datecalendar" value="<?php echo esc_attr( $cargo_tracking_shipping_delivery_date ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Cargo Delivery Hour', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="text" name="cargo_tracking_shipping_delivery_hour" placeholder="<?php echo esc_html__( 'Example: 16:11', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_shipping_delivery_hour ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Qty', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="text" name="cargo_tracking_pcs" placeholder="<?php echo esc_html__( 'Example: 99 Box', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_pcs ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Weight', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="text" name="cargo_tracking_weight" placeholder="<?php echo esc_html__( 'Example: 100 kg', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_weight ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Height', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="text" name="cargo_tracking_height" placeholder="<?php echo esc_html__( 'Example: 50 m', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_height ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Shipping Status', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="text" name="cargo_tracking_shipping_status" placeholder="<?php echo esc_html__( 'Example: Delivered', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_shipping_status ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Product(s)', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="text" name="cargo_tracking_product" placeholder="<?php echo esc_html__( 'Example: Laptop', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_product ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Carrier', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="text" name="cargo_tracking_carrier" placeholder="<?php echo esc_html__( 'Example: John Doe', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_carrier ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Total Freight', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="text" name="cargo_tracking_freight" placeholder="<?php echo esc_html__( 'Example: 9', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_freight ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Payment Mode', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="text" name="cargo_tracking_cc" placeholder="<?php echo esc_html__( 'Example: Credit Card', 'translogistic' ); ?>" value="<?php echo esc_attr( $cargo_tracking_cc ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Barcode Image URL', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<input type="text" name="barcode_image_url" placeholder="<?php echo esc_html__( 'http://gloriathemes.com/image.jpg', 'translogistic' ); ?>" value="<?php echo esc_attr( $barcode_image_url ); ?>" />
			</div>
		</div>
		<div class="gloria-metabox-list-count">
			<div class="gloria-metabox-list-name"><?php echo esc_html__( 'Additional Note', 'translogistic' ); ?></div>
			<div class="gloria-metabox-list-content">
				<textarea name="cargo_tracking_additional_note" rows="10" placeholder="<?php echo esc_html__( 'Additional note area.', 'translogistic' ); ?>"><?php echo esc_attr( $cargo_tracking_additional_note ); ?></textarea>
			</div>
		</div>
    </div>
    <?php 
	wp_enqueue_script( 'jquery-ui-datepicker', 'script.js', array( 'jquery', 'jquery-ui-datepicker' ), '1.0', true );
}

function translogistic_cargo_tracking_meta_box_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
     
    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['meta_box_nonce'] ) ) return;
     
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post', $post_id ) ) return;
     
    // now we can actually save the data
    $allowed = array( 
        'a' => array( // on allow a tags
            'href' => array() // and those anchors can only have href attribute
        )
    );
     
    // Make sure your data is set before trying to save it
    if( isset( $_POST['cargo_tracking_code'] ) )
        update_post_meta( $post_id, 'cargo_tracking_code', strip_tags( wp_kses( $_POST['cargo_tracking_code'], $allowed ) ) );
	
    if( isset( $_POST['cargo_tracking_shipper_name'] ) )
        update_post_meta( $post_id, 'cargo_tracking_shipper_name', strip_tags( wp_kses( $_POST['cargo_tracking_shipper_name'], $allowed ) ) );

    if( isset( $_POST['cargo_tracking_shipper_tel'] ) )
        update_post_meta( $post_id, 'cargo_tracking_shipper_tel', strip_tags( wp_kses( $_POST['cargo_tracking_shipper_tel'], $allowed ) ) );

    if( isset( $_POST['cargo_tracking_shipper_email'] ) )
        update_post_meta( $post_id, 'cargo_tracking_shipper_email', strip_tags( wp_kses( $_POST['cargo_tracking_shipper_email'], $allowed ) ) );

    if( isset( $_POST['cargo_tracking_shipper_address'] ) )
        update_post_meta( $post_id, 'cargo_tracking_shipper_address', strip_tags( wp_kses( $_POST['cargo_tracking_shipper_address'], $allowed ) ) );
	
    if( isset( $_POST['cargo_tracking_shipping_from'] ) )
        update_post_meta( $post_id, 'cargo_tracking_shipping_from', strip_tags( wp_kses( $_POST['cargo_tracking_shipping_from'], $allowed ) ) );
	
    if( isset( $_POST['cargo_tracking_receiver_name'] ) )
        update_post_meta( $post_id, 'cargo_tracking_receiver_name', strip_tags( wp_kses( $_POST['cargo_tracking_receiver_name'], $allowed ) ) );

    if( isset( $_POST['cargo_tracking_receiver_tel'] ) )
        update_post_meta( $post_id, 'cargo_tracking_receiver_tel', strip_tags( wp_kses( $_POST['cargo_tracking_receiver_tel'], $allowed ) ) );

    if( isset( $_POST['cargo_tracking_receiver_email'] ) )
        update_post_meta( $post_id, 'cargo_tracking_receiver_email', strip_tags( wp_kses( $_POST['cargo_tracking_receiver_email'], $allowed ) ) );

    if( isset( $_POST['cargo_tracking_receiver_address'] ) )
        update_post_meta( $post_id, 'cargo_tracking_receiver_address', strip_tags( wp_kses( $_POST['cargo_tracking_receiver_address'], $allowed ) ) );
         
    if( isset( $_POST['cargo_tracking_shipping_delivery_place'] ) )
        update_post_meta( $post_id, 'cargo_tracking_shipping_delivery_place', strip_tags( esc_attr( $_POST['cargo_tracking_shipping_delivery_place'] ) ) );
         
    if( isset( $_POST['cargo_tracking_meta_box_select'] ) )
        update_post_meta( $post_id, 'cargo_tracking_meta_box_select', strip_tags( esc_attr( $_POST['cargo_tracking_meta_box_select'] ) ) );
         
    if( isset( $_POST['cargo_tracking_shipping_release_date'] ) )
        update_post_meta( $post_id, 'cargo_tracking_shipping_release_date', strip_tags( esc_attr( $_POST['cargo_tracking_shipping_release_date'] ) ) );
         
    if( isset( $_POST['cargo_tracking_shipping_release_hour'] ) )
        update_post_meta( $post_id, 'cargo_tracking_shipping_release_hour', strip_tags( esc_attr( $_POST['cargo_tracking_shipping_release_hour'] ) ) );
         
    if( isset( $_POST['cargo_tracking_shipping_delivery_date'] ) )
        update_post_meta( $post_id, 'cargo_tracking_shipping_delivery_date', strip_tags( esc_attr( $_POST['cargo_tracking_shipping_delivery_date'] ) ) );
         
    if( isset( $_POST['cargo_tracking_shipping_delivery_hour'] ) )
        update_post_meta( $post_id, 'cargo_tracking_shipping_delivery_hour', strip_tags( esc_attr( $_POST['cargo_tracking_shipping_delivery_hour'] ) ) );
         
    if( isset( $_POST['cargo_tracking_shipping_status'] ) )
        update_post_meta( $post_id, 'cargo_tracking_shipping_status', strip_tags( esc_attr( $_POST['cargo_tracking_shipping_status'] ) ) );
         
    if( isset( $_POST['cargo_tracking_product'] ) )
        update_post_meta( $post_id, 'cargo_tracking_product', strip_tags( esc_attr( $_POST['cargo_tracking_product'] ) ) );
         
    if( isset( $_POST['cargo_tracking_carrier'] ) )
        update_post_meta( $post_id, 'cargo_tracking_carrier', strip_tags( esc_attr( $_POST['cargo_tracking_carrier'] ) ) );
         
    if( isset( $_POST['cargo_tracking_freight'] ) )
        update_post_meta( $post_id, 'cargo_tracking_freight', strip_tags( esc_attr( $_POST['cargo_tracking_freight'] ) ) );
         
    if( isset( $_POST['cargo_tracking_cc'] ) )
        update_post_meta( $post_id, 'cargo_tracking_cc', strip_tags( esc_attr( $_POST['cargo_tracking_cc'] ) ) );
         
    if( isset( $_POST['barcode_image_url'] ) )
        update_post_meta( $post_id, 'barcode_image_url', strip_tags( esc_attr( $_POST['barcode_image_url'] ) ) );
         
    if( isset( $_POST['cargo_tracking_pcs'] ) )
        update_post_meta( $post_id, 'cargo_tracking_pcs', strip_tags( esc_attr( $_POST['cargo_tracking_pcs'] ) ) );
         
    if( isset( $_POST['cargo_tracking_weight'] ) )
        update_post_meta( $post_id, 'cargo_tracking_weight', strip_tags( esc_attr( $_POST['cargo_tracking_weight'] ) ) );
         
    if( isset( $_POST['cargo_tracking_height'] ) )
        update_post_meta( $post_id, 'cargo_tracking_height', strip_tags( esc_attr( $_POST['cargo_tracking_height'] ) ) );
         
    if( isset( $_POST['cargo_tracking_additional_note'] ) )
        update_post_meta( $post_id, 'cargo_tracking_additional_note', strip_tags( esc_attr( $_POST['cargo_tracking_additional_note'] ) ) );

}
add_action( 'save_post', 'translogistic_cargo_tracking_meta_box_save' );
/*------------- CARGO TRACKING END -------------*/