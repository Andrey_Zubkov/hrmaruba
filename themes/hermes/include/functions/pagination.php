<?php
/*------------- PAGINATION START -------------*/
function translogistic_pagination() {
	if( is_singular() )
		return;

	global $wp_query;

	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	if( $paged >= 1 )
		$links[] = $paged;

	if( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<nav class="post-pagination"><ul>' . "\n";

	if( get_previous_posts_link() )
		printf( '<li>' . get_previous_posts_link( '&lt;' ) . '</li>' );

	?>
		<li><span><?php echo esc_html__( 'Page', 'translogistic' ) . ' ' . $paged . ' ' . esc_html__( 'of', 'translogistic' ) . ' ' . $max; ?></span></li>
	<?php
	if( get_next_posts_link() )
		printf( '<li>' . get_next_posts_link( '&gt;' ) . '</li>' );

	echo '</ul></nav>' . "\n";
}

add_filter('redirect_canonical','translogistic_disable_redirect_canonical');
function translogistic_disable_redirect_canonical($redirect_url) {if (is_paged() && is_singular()) $redirect_url = false; return $redirect_url; }
/*------------- PAGINATION END -------------*/

/*------------- SINGLE NAVIGATION START -------------*/
function translogistic_single_nav() {
	$post_navigation_hide = get_theme_mod( 'hide_post_navigation' );
	if( !$post_navigation_hide == '1' ):
	$translogistic_single_nav_prev = esc_html__( 'Previous Post', 'translogistic' );
	$translogistic_single_nav_next = esc_html__( 'Next Post', 'translogistic' );
	$prevPost = get_previous_post( false );
	$nextPost = get_next_post( false );
?>
	<div class="post-bottom-element">
		<div class="post-navigation">
			<nav>
				<ul>
					<li class="previous">
						<?php if( !empty( $prevPost ) ) { ?>
							<h4><a href="<?php echo get_permalink( $prevPost->ID ); ?>" title="<?php the_title_attribute( array( 'post' => $prevPost->ID ) ); ?>"><?php echo esc_attr( $translogistic_single_nav_prev ); ?></a></h4>
							<h3><?php previous_post_link( '%link', '%title' ); ?></h3>
						<?php } ?>
					</li>
					<li class="next">
						<?php if( !empty( $nextPost ) ) { ?>
							<h4><a href="<?php echo get_permalink( $nextPost->ID ); ?>" title="<?php the_title_attribute( array( 'post' => $nextPost->ID ) ); ?>"><?php echo esc_attr( $translogistic_single_nav_next ); ?></a></h4>
							<h3><?php next_post_link( '%link', '%title' ); ?></h3>
						<?php } ?>
					</li>
				</ul>
			</nav>
		</div>
	</div>
<?php
	endif;
}
/*------------- SINGLE NAVIGATION END -------------*/