<?php
/*------------- LATEST POST WIDGET START -------------*/
function translogistic_latest_posts_register_widgets() {
	register_widget( 'translogistic_latest_Latest_Posts_Widget' );
}
add_action( 'widgets_init', 'translogistic_latest_posts_register_widgets' );

class translogistic_latest_Latest_Posts_Widget extends WP_Widget {
	function __construct() {
		parent::__construct(
	            'translogistic_latest_Latest_Posts_Widget',
        	    esc_html__( 'Translogistic Theme: Sidebar Latest Posts', 'translogistic' ),
 	           array( 'description' => esc_html__( 'Latest posts widget.', 'translogistic' ), )
		);
	}
	
	function widget( $args, $instance ) {
		
		echo $args['before_widget'];
		$latest_posts_widget_title_control = esc_attr( $instance['latest_posts_widget_title'] );
		if ( !empty( $instance['latest_posts_widget_title'] ) ) {
			echo '<div class="widget-title"><h4>'. esc_attr( $latest_posts_widget_title_control ) .'</h4></div>';
		}
		
		if( $instance) {
			$latest_posts_widget_title = strip_tags( esc_attr( $instance['latest_posts_widget_title'] ) );
			$postcount = strip_tags( esc_attr( $instance['postcount'] ) );
			$postfeaturedimage = strip_tags( esc_attr( $instance['postfeaturedimage'] ) );
			$postdate = strip_tags( esc_attr( $instance['postdate'] ) );
			$postcategory = strip_tags( esc_attr( $instance['postcategory'] ) );
			$categorylist = strip_tags( esc_attr( $instance['categorylist'] ) );
			$offset = strip_tags( esc_attr( $instance['offset'] ) );
			$exclude = strip_tags( esc_attr( $instance['exclude'] ) );
		} 
		?>
		<?php translogistic_widget_content_before(); ?>
			<div class="translogistic-latest-posts-widget">
				<ul>
					<?php
						$args_latest_posts = array(
								'posts_per_page' => $postcount,
								'post_status' => 'publish',
								'ignore_sticky_posts'    => true,
								'post_type' => 'post',
								'cat' => $categorylist
						); 
						$wp_query = new WP_Query($args_latest_posts);
						while ( $wp_query->have_posts() ) :
						$wp_query->the_post();
					?>
					<li>
						<?php if( !empty( $postfeaturedimage ) ): ?>
							<div class="image">
								<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
									<?php if ( has_post_thumbnail() ) {
										the_post_thumbnail( 'translogistic-latest-posts-widget-image' );
									}
									?>
								</a>
							</div>
						<?php endif; ?>
						<div class="desc">
							<?php if( !empty( $postcategory ) ): ?>
								<div class="category"><?php the_category( ', ', '' ); ?></div>
							<?php endif; ?>
							<?php if( !empty( $postdate ) ): ?>
								<div class="date"><?php the_time( get_option( 'date_format' ) ); ?></div>
							<?php endif; ?>
						</div>
						<h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
					</li>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>
				</ul>
			</div>
		<?php translogistic_widget_content_after(); ?>
		<?php
		echo $args['after_widget'];
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['latest_posts_widget_title'] = strip_tags( esc_attr( $new_instance['latest_posts_widget_title'] ) );
		$instance['postcount'] = strip_tags( esc_attr( $new_instance['postcount'] ) );
		$instance['postfeaturedimage'] = strip_tags( esc_attr( $new_instance['postfeaturedimage'] ) );
		$instance['postdate'] = strip_tags( esc_attr( $new_instance['postdate'] ) );
		$instance['categorylist'] = strip_tags( esc_attr( $new_instance['categorylist'] ) );
		$instance['postcategory'] = strip_tags( esc_attr( $new_instance['postcategory'] ) );
		$instance['offset'] = strip_tags( esc_attr( $new_instance['offset'] ) );
		$instance['exclude'] = strip_tags( esc_attr( $new_instance['exclude'] ) );
		return $instance;
	}

	function form($instance) {
	 	$latest_posts_widget_title = '';
	 	$postcount = '';
		$postfeaturedimage = '';
		$postdate = '';
		$postcategory = '';
		$categorylist = '';
		$offset = '';
		$exclude = '';

		if( $instance) {
			$latest_posts_widget_title = strip_tags( esc_attr( $instance['latest_posts_widget_title'] ) );
			$postcount = strip_tags( esc_attr( $instance['postcount'] ) );
			$postfeaturedimage = strip_tags( esc_attr( esc_textarea( $instance['postfeaturedimage'] ) ) );
			$postdate = strip_tags( esc_attr( esc_attr( $instance['postdate'] ) ) );
			$postcategory = strip_tags( esc_attr( esc_attr( $instance['postcategory'] ) ) );
			$categorylist = strip_tags( esc_attr( esc_attr( $instance['categorylist'] ) ) );
			$offset = strip_tags( esc_attr( esc_attr( $instance['offset'] ) ) );
			$exclude = strip_tags( esc_attr( esc_attr( $instance['exclude'] ) ) );
		} ?>
		 
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'latest_posts_widget_title' ) ); ?>"><?php esc_html_e( 'Widget Title:', 'translogistic' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'latest_posts_widget_title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'latest_posts_widget_title' ) ); ?>" type="text" value="<?php echo esc_attr( $latest_posts_widget_title ); ?>" />
		</p>
		 
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'postcount' ) ); ?>"><?php esc_html_e( 'Post Count:', 'translogistic' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'postcount' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'postcount' ) ); ?>" type="text" value="<?php echo esc_attr( $postcount ); ?>" />
		</p>
		 
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'categorylist' ) ); ?>"><?php esc_html_e( 'Category:', 'translogistic' ); ?></label>
			<select name="<?php echo esc_attr( $this->get_field_name('categorylist') ); ?>" id="<?php echo esc_attr( $this->get_field_id('categorylist') ); ?>" class="widefat"> 
				<option value=""><?php echo esc_html__( 'All Categories', 'translogistic' ); ?></option>
				<?php
				 $categories =  get_categories('child_of=0'); 
				 foreach ($categories as $category) {
					$category_select_control = '';
					if ( $categorylist == $category->cat_ID )
					{
						$category_select_control = "selected";
					}
					$option = '<option value="' . esc_attr( $category->cat_ID ) . '"' . $category_select_control . '>';
					$option .= $category->cat_name;
					$option .= '</option>';
					echo balanceTags( $option );
				 }
				?>
			</select>
		</p>
		 
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'offset' ) ); ?>"><?php esc_html_e( 'Offset:', 'translogistic' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'offset' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'offset' ) ); ?>" type="text" value="<?php echo esc_attr( $offset ); ?>" />
		</p>
		 
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'exclude' ) ); ?>"><?php esc_html_e( 'Exclude Posts:', 'translogistic' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'exclude' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'exclude' ) ); ?>" type="text" value="<?php echo esc_attr( $exclude ); ?>" />
		</p>
		 
		<p>
			<input type="checkbox" class="widefat" <?php checked($postfeaturedimage, 'on'); ?> id="<?php echo esc_attr( $this->get_field_id( 'postfeaturedimage' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'postfeaturedimage' ) ); ?>" />
			<label for="<?php echo esc_attr( $this->get_field_id( 'postfeaturedimage' ) ); ?>"><?php esc_html_e( 'Post Featured Image', 'translogistic' ); ?></label>
		</p>
		 
		<p>
			<input type="checkbox" class="checkbox" <?php checked($postdate, 'on'); ?> id="<?php echo esc_attr( $this->get_field_id( 'postdate' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'postdate' ) ); ?>" />
			<label for="<?php echo esc_attr( $this->get_field_id( 'postdate' ) ); ?>"><?php esc_html_e( 'Post Date', 'translogistic' ); ?></label>
		</p>
		 
		<p>
			<input type="checkbox" class="checkbox" <?php checked($postcategory, 'on'); ?> id="<?php echo esc_attr( $this->get_field_id( 'postcategory' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'postcategory' ) ); ?>" />
			<label for="<?php echo esc_attr( $this->get_field_id( 'postcategory' ) ); ?>"><?php esc_html_e( 'Post Category', 'translogistic' ); ?></label>
		</p>
		
	<?php }
	
}
/*------------- LATEST POST WIDGET END -------------*/