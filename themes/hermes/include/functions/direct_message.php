<?php

function message_init() {
    $labels = array(
        'name'                  => 'Messages',
        'singular_name'         => 'Message',
        'menu_name'             => 'Messages',
        'name_admin_bar'        => 'Message',
        'add_new'               => 'Add Message',
        'add_new_item'          => 'Add new message',
        'new_item'              => 'New message',
        'edit_item'             => 'Update message',
        'all_items'             => 'All messages',
        'search_items'          => 'Find message',
        'not_found'             => 'Message not found',
        'not_found_in_trash'    => 'Message is not in trash',
    );

    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'publicly_queryable'    => true,
        'show_ui'               => true,
        'has_archive'           => false,
        'menu_icon'             => 'dashicons-admin-multisite',
        'supports'              => array('title', 'editor', 'autor'),
    );

    register_post_type( 'message' , $args);
}
add_action( 'init', 'message_init' );


function message_custom_column($defaults) {
    
    $columns = array(
    	'cb' 					=> '<input type="checkbox" />',
        'img'           		=> '<span class="dashicons dashicons-format-image"></span>',        
        'username'         		=> 'Username',
        'first_name'      		=> 'First name',
        'last_name'          	=> 'Last name',
        'user_email'			=> 'Email',
        'date'          		=> 'Дата создания <span class="dashicons dashicons-calendar"></span>',
    );

    return $columns;
}
add_filter( 'manage_message_posts_columns', 'message_custom_column' );

function message_get_column_value($column, $post_id) {
   $post_info = get_post($post_id);
   $user_info = get_userdata($post_info->post_author);

    if ( $column == 'img' ) {
        echo  '<img style="height: 60px; width:60px;" src="'.get_avatar_url($post_id).'" alt="" />';             
    }

    if ( $column == 'username') {
    	if (isset($user_info->user_login) && !empty($user_info->user_login)) {
    		echo $user_info->user_login;
    	} else {
    		echo 'not set';
    	}
    }       
        
    if ( $column == 'first_name') {
    	if (isset($user_info->first_name) && !empty($user_info->first_name)) {
    		echo $user_info->first_name;
    	} else {
    		echo 'not set';
    	}
    }
    if ( $column == 'last_name') {
    	if (isset($user_info->last_name) && !empty($user_info->last_name)) {
    		echo $user_info->last_name;
    	} else {
    		echo 'not set';
    	}
    }
     if ( $column == 'user_email') {
    	if (isset($user_info->user_email) && !empty($user_info->user_email)) {
    		echo $user_info->user_email;
    	} else {
    		echo 'not set';
    	}
    }    
}
add_action( 'manage_message_posts_custom_column', 'message_get_column_value', 10, 2 );