<?php
/*------------- THEME OPTIONS START -------------*/
function translogistic_santanize( $input ) {
		return $input;
}

/*-- FONT CUSTOMIZE START --*/
new theme_customizer();
class theme_customizer
{
    public function __construct()
    {
        add_action( 'customize_register', array(&$this, 'customize_manager_demo' ));
    }

    public function customizer_admin() {
        add_theme_page( esc_html__( 'Customize', 'translogistic' ), esc_html__( 'Customize', 'translogistic' ), 'edit_theme_options', 'customize.php' );
    }

    public function customize_manager_demo( $wp_manager )
    {
        $this->demo_section( $wp_manager );
        $this->custom_sections( $wp_manager );
    }

    private function demo_section( $wp_manager )
    {
        $wp_manager->add_section( 'customiser_demo_section', array(
            'title'          => esc_html__( 'Default Demo Controls', 'translogistic' ),
        ) );
    }

    private function custom_sections( $wp_manager )
    {
        $wp_manager->add_section( 'customiser_demo_custom_section', array(
            'title'          => esc_html__( 'Custom Controls Demo', 'translogistic' ),
        ) );

        require_once get_template_directory() . '/include/functions/google-font-dropdown-custom-control.php';
        $wp_manager->add_setting( 'heading-font', array(
            'default'        => '',
			'sanitize_callback' => 'translogistic_santanize'
        ) );
		
        $wp_manager->add_control( new translogistic_Google_Font_Dropdown_Custom_Control( $wp_manager, 'heading-font', array(
            'label'   => esc_html__( 'Heading Font', 'translogistic' ),
            'section' => 'font',
            'settings'   => 'heading-font',
        ) ) );
		
        $wp_manager->add_setting( 'text-font', array(
            'default'        => '',
			'sanitize_callback' => 'translogistic_santanize'
        ) );
		
        $wp_manager->add_control( new translogistic_Google_Font_Dropdown_Custom_Control( $wp_manager, 'text-font', array(
            'label'   => esc_html__( 'Text Font', 'translogistic' ),
            'section' => 'font',
            'settings'   => 'text-font',
        ) ) );
		
        $wp_manager->add_setting( 'menu-font', array(
            'default'        => '',
			'sanitize_callback' => 'translogistic_santanize'
        ) );
		
        $wp_manager->add_control( new translogistic_Google_Font_Dropdown_Custom_Control( $wp_manager, 'menu-font', array(
            'label'   => esc_html__('Menu Font', 'translogistic' ),
            'section' => 'font',
            'settings'   => 'menu-font',
        ) ) );
		
        $wp_manager->add_setting( 'theme-one-font', array(
            'default'        => 'Source Sans Pro',
			'sanitize_callback' => 'translogistic_santanize'
        ) );
		
        $wp_manager->add_control( new translogistic_Google_Font_Dropdown_Custom_Control( $wp_manager, 'theme-one-font', array(
            'label'   => esc_html__('Theme One Font', 'translogistic' ),
            'section' => 'font',
            'settings'   => 'theme-one-font',
        ) ) );
		
        $wp_manager->add_setting( 'theme-two-font', array(
            'default'        => 'Roboto',
			'sanitize_callback' => 'translogistic_santanize'
        ) );
		
        $wp_manager->add_control( new translogistic_Google_Font_Dropdown_Custom_Control( $wp_manager, 'theme-two-font', array(
            'label'   => esc_html__('Theme Two Font', 'translogistic' ),
            'section' => 'font',
            'settings'   => 'theme-two-font',
        ) ) );
    }

}
/*-- FONT CUSTOMIZE END --*/

function translogistic_customizer( $wp_customize ) {

/*-- FONTS START --*/
	$wp_customize->add_section( 'font', array(
        'title' => esc_html__( 'Fonts', 'translogistic' ),
    ) );
	
/*-- FONT FAMILY STYLE --*/
	$wp_customize->add_setting( 'font_family_style', array(
		'default' => '900italic,900,800italic,800,700,700italic,600,600italic,500,500italic,400,400italic,300,300italic,200,200italic,100,100italic',
		'sanitize_callback' => 'jupios_santanize'
	) );

	$wp_customize->add_control( 'font_family_style', array(
		'label' => esc_html__( 'Font Styles', 'translogistic' ),
		'section' => 'font',
	) );

/*-- CYRILLIC EXT FONT --*/
	$wp_customize->add_setting( 'font_cyrillic_ext', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'font_cyrillic_ext', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Include Cyrillic Extended (cyrillic-ext)', 'translogistic' ),
        'section' => 'font',
    ) );

/*-- GREEK EXT FONT --*/
	$wp_customize->add_setting( 'font_greek_ext', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'font_greek_ext', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Include Greek Extended (greek-ext)', 'translogistic' ),
        'section' => 'font',
    ) );

/*-- GREEK FONT --*/
	$wp_customize->add_setting( 'font_greek', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'font_greek', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Include Greek (greek)', 'translogistic' ),
        'section' => 'font',
    ) );

/*-- VIETNAMESE FONT --*/
	$wp_customize->add_setting( 'font_vietnamese', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'font_vietnamese', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Include Vietnamese (vietnamese)', 'translogistic' ),
        'section' => 'font',
    ) );

/*-- CYRILLIC FONT --*/
	$wp_customize->add_setting( 'font_cyrillic', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'font_cyrillic', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Include Cyrillic (cyrillic)', 'translogistic' ),
        'section' => 'font',
    ) );
	
/*-- H1 --*/
	$wp_customize->add_setting( 'h1_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'h1_font_size', array(
		'label' => esc_html__( 'h1', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- H2 --*/
	$wp_customize->add_setting( 'h2_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'h2_font_size', array(
		'label' => esc_html__( 'h2', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- H3 --*/
	$wp_customize->add_setting( 'h3_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'h3_font_size', array(
		'label' => esc_html__( 'h3', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- H4 --*/
	$wp_customize->add_setting( 'h4_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'h4_font_size', array(
		'label' => esc_html__( 'h4', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- H5 --*/
	$wp_customize->add_setting( 'h5_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'h5_font_size', array(
		'label' => esc_html__( 'h5', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- H6 --*/
	$wp_customize->add_setting( 'h6_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'h6_font_size', array(
		'label' => esc_html__( 'h6', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- BODY FONT SIZE --*/
	$wp_customize->add_setting( 'body_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'body_font_size', array(
		'label' => esc_html__( 'Body Font Size', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- BODY LINE HEIGHT  --*/
	$wp_customize->add_setting( 'body_line_height', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'body_line_height', array(
		'label' => esc_html__( 'Body Font Line Height (Exm. 1.8)', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- MENU FONT SIZE --*/
	$wp_customize->add_setting( 'menu_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'menu_font_size', array(
		'label' => esc_html__( 'Menu Font Size', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- SUB MENU FONT SIZE --*/
	$wp_customize->add_setting( 'sub_menu_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'sub_menu_font_size', array(
		'label' => esc_html__( 'Submenu Font Size', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- POST CONTENT FONT SIZE --*/
	$wp_customize->add_setting( 'post_content_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'post_content_font_size', array(
		'label' => esc_html__( 'Post Content Font Size', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- POST TITLE FONT SIZE --*/
	$wp_customize->add_setting( 'post_title_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'post_title_font_size', array(
		'label' => esc_html__( 'Post Title Font Size', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- PAGE CONTENT FONT SIZE --*/
	$wp_customize->add_setting( 'page_content_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'page_content_font_size', array(
		'label' => esc_html__( 'Page Content Font Size', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- PAGE TITLE FONT SIZE --*/
	$wp_customize->add_setting( 'page_title_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'page_title_font_size', array(
		'label' => esc_html__( 'Page Title Font Size', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- PAGE EXCERPT FONT SIZE --*/
	$wp_customize->add_setting( 'page_excerpt_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'page_excerpt_font_size', array(
		'label' => esc_html__( 'Page Excerpt Font Size', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- FOOTER CONTENT FONT SIZE --*/
	$wp_customize->add_setting( 'footer_content_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'footer_content_font_size', array(
		'label' => esc_html__( 'Footer Content Font Size', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- 404 TITLE FONT ONE SIZE --*/
	$wp_customize->add_setting( 'error_text_one_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'error_text_one_font_size', array(
		'label' => esc_html__( '404 Text One Font Size', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- 404 TEXT FONT TWO SIZE --*/
	$wp_customize->add_setting( 'error_text_two_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'error_text_two_font_size', array(
		'label' => esc_html__( '404 Text Two Font Size', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- INPUT FONT SIZE --*/
	$wp_customize->add_setting( 'input_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'input_font_size', array(
		'label' => esc_html__( 'Input Font Size', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
	
/*-- BUTTON FONT SIZE --*/
	$wp_customize->add_setting( 'button_font_size', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'button_font_size', array(
		'label' => esc_html__( 'Button Font Size', 'translogistic' ),
        'type' => 'number',
		'section' => 'font',
	) );
/*-- FONTS END --*/

/*-- GENERAL START --*/
	$wp_customize->add_section( 'generalsettings', array(
        'title' => esc_html__( 'General', 'translogistic' ),
        'description' => '',
    ) );
	
/*-- SIDEBAR POSITION --*/
	$wp_customize->add_setting( 'sidebar_position', array(
		'default' => 'right',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	 
	$wp_customize->add_control( 'sidebar_position', array(
		'type' => 'radio',
		'label' => esc_html__( 'General Sidebar Position', 'translogistic' ),
		'section' => 'generalsettings',
		'choices' => array(
			'left' => esc_html__( 'Left', 'translogistic' ),
			'right' => esc_html__( 'Right', 'translogistic' ),
			'nosidebar' => esc_html__( 'Sidebar None', 'translogistic' ),
		),
	) );
	
/*-- LOADER --*/
	$wp_customize->add_setting( 'translogistic_loader', array(
		'default' => 'active',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	 
	$wp_customize->add_control( 'translogistic_loader', array(
		'type' => 'radio',
		'label' => esc_html__( 'Loader', 'translogistic' ),
		'section' => 'generalsettings',
		'choices' => array(
			'active' => esc_html__( 'Active', 'translogistic' ),
			'inactive' => esc_html__( 'Inactive', 'translogistic' ),
		),
	) );

/*-- CARGO QUERY RESULTS PAGE --*/
	$wp_customize->add_setting( 'cargo_query_results_page', array(
		'default' => esc_html__( '', 'translogistic' ),
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'cargo_query_results_page', array(
		'label' => esc_html__( 'Cargo Query Results Page', 'translogistic' ),
        'section' => 'generalsettings',
		'type'           => 'dropdown-pages'
	) );
/*-- GENERAL END --*/

/*-- HEADER START --*/
	$wp_customize->add_section( 'headerelement', array(
        'title' => esc_html__( 'Header', 'translogistic' ),
        'description' => '',
    ) );
	
/*-- HEADER LOGO --*/
	$wp_customize->add_setting( 'translogistic_logo', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
 
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'translogistic_logo', array(
        'label'    => esc_html__( 'Header Logo Upload', 'translogistic' ),
		'section' => 'headerelement',
    ) ) );
	
/*-- HEADER FIXED --*/
	$wp_customize->add_setting( 'header_fixed', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'header_fixed', array(
		'label' => esc_html__( 'Header Fixed', 'translogistic' ),
        'type' => 'checkbox',
        'section' => 'headerelement',
	) );
	
/*-- HIDE HEADER SOCIAL MEDIA --*/
	$wp_customize->add_setting( 'translogistic_hide_header_social_media', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_hide_header_social_media', array(
		'label' => esc_html__( 'Hide Header Social Media', 'translogistic' ),
        'type' => 'checkbox',
        'section' => 'headerelement',
	) );
	
/*-- HIDE HEADER SEARCH --*/
	$wp_customize->add_setting( 'translogistic_hide_header_search', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_hide_header_search', array(
		'label' => esc_html__( 'Hide Header Search', 'translogistic' ),
        'type' => 'checkbox',
        'section' => 'headerelement',
	) );
	
/*-- HIDE HEADER CONTACT --*/
	$wp_customize->add_setting( 'translogistic_hide_header_contact', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_hide_header_contact', array(
		'label' => esc_html__( 'Hide Header Contact', 'translogistic' ),
        'type' => 'checkbox',
        'section' => 'headerelement',
	) );
	
/*-- HEADER EMAIL --*/
	$wp_customize->add_setting( 'header_email', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'header_email', array(
		'label' => esc_html__( 'Header Email', 'translogistic' ),
        'type' => 'text',
        'section' => 'headerelement',
	) );
	
/*-- HEADER PHONE --*/
	$wp_customize->add_setting( 'header_phone', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'header_phone', array(
		'label' => esc_html__( 'Header Phone', 'translogistic' ),
        'type' => 'text',
        'section' => 'headerelement',
	) );
	
/*-- HEADER LOGO HEIGHT --*/
	$wp_customize->add_setting( 'logo_height', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'logo_height', array(
		'label' => esc_html__( 'Logo Height (px)', 'translogistic' ),
        'type' => 'number',
        'section' => 'headerelement',
	) );
	
/*-- HEADER LOGO WIDTH --*/
	$wp_customize->add_setting( 'logo_width', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'logo_width', array(
		'label' => esc_html__( 'Logo Width (px)', 'translogistic' ),
        'type' => 'number',
        'section' => 'headerelement',
	) );
/*-- HEADER END --*/

/*-- CATEGORY & ARCHIVE START --*/
	$wp_customize->add_section( 'categoryarchive', array(
        'title' => esc_html__( 'Category & Archive', 'translogistic' ),
        'description' => '',
    ) );
	
/*-- BLOG & SINGLE BANNER --*/
	$wp_customize->add_setting( 'blog_single_banner', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
 
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'blog_single_banner', array(
        'label'    => esc_html__( 'Blog & Single Banner', 'translogistic' ),
		'section' => 'categoryarchive',
    ) ) );

/*-- CATEGORY & ARCHIVE EXCERPT HIDE --*/
	$wp_customize->add_setting( 'hide_categoryarchive_excerpt', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'hide_categoryarchive_excerpt', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Hide Excerpt', 'translogistic' ),
		'section' => 'categoryarchive',
    ) );

/*-- CATEGORY & ARCHIVE CATEGORY NAME HIDE --*/
	$wp_customize->add_setting( 'hide_categoryarchive_name', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'hide_categoryarchive_name', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Hide Category Name', 'translogistic' ),
		'section' => 'categoryarchive',
    ) );

/*-- CATEGORY & ARCHIVE POST INFORMATION HIDE --*/
	$wp_customize->add_setting( 'hide_categoryarchive_post_information', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'hide_categoryarchive_post_information', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Hide Post Information', 'translogistic' ),
		'section' => 'categoryarchive',
    ) );

/*-- CATEGORY & ARCHIVE POST READ MORE HIDE --*/
	$wp_customize->add_setting( 'hide_categoryarchive_post_read_more', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'hide_categoryarchive_post_read_more', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Hide Post Read More', 'translogistic' ),
		'section' => 'categoryarchive',
    ) );

/*-- CATEGORY & ARCHIVE POST SOCIAL SHARE HIDE --*/
	$wp_customize->add_setting( 'hide_categoryarchive_post_social_share_more', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'hide_categoryarchive_post_social_share_more', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Hide Post Social Share', 'translogistic' ),
		'section' => 'categoryarchive',
    ) );

/*-- CATEGORY & ARCHIVE POST FEATURED IMAGE HIDE --*/
	$wp_customize->add_setting( 'hide_categoryarchive_featured_image', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'hide_categoryarchive_featured_image', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Hide Post Featured Image', 'translogistic' ),
		'section' => 'categoryarchive',
    ) );
/*-- CATEGORY & ARCHIVE END --*/

/*-- PAGES & POSTS START --*/
	$wp_customize->add_section( 'pagepost', array(
        'title' => esc_html__( 'Posts & Pages', 'translogistic' ),
        'description' => '',
    ) );

/*-- RELATED POSTS HIDE --*/
	$wp_customize->add_setting( 'hide_related_posts', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'hide_related_posts', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Hide Related Posts', 'translogistic' ),
        'section' => 'pagepost',
    ) );
	
/*-- POST NAVIGATION HIDE --*/
	$wp_customize->add_setting( 'hide_post_navigation', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'hide_post_navigation', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Hide Post Navigation', 'translogistic' ),
        'section' => 'pagepost',
    ) );
	
/*-- POST FEATURED IMAGE HIDE --*/
	$wp_customize->add_setting( 'hide_post_featured_image', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'hide_post_featured_image', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Hide Post Featured Image', 'translogistic' ),
        'section' => 'pagepost',
    ) );
	
/*-- POST COMMENT AREA HIDE --*/
	$wp_customize->add_setting( 'hide_post_comment', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'hide_post_comment', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Hide Post Comment Area', 'translogistic' ),
        'section' => 'pagepost',
    ) );
	
/*-- POST INFORMATION HIDE --*/
	$wp_customize->add_setting( 'hide_post_information', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'hide_post_information', array(
		'type' => 'checkbox',
		'label' => esc_html__( 'Hide Post Information', 'translogistic' ),
		'section' => 'pagepost',
	) );
	
/*-- POST SHARE HIDE --*/
	$wp_customize->add_setting( 'hide_post_share', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'hide_post_share', array(
		'type' => 'checkbox',
		'label' => esc_html__( 'Hide Post Share', 'translogistic' ),
		'section' => 'pagepost',
	) );
	
/*-- POST TAGS HIDE --*/
	$wp_customize->add_setting( 'hide_post_tags', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'hide_post_tags', array(
		'type' => 'checkbox',
		'label' => esc_html__( 'Hide Post Tags', 'translogistic' ),
		'section' => 'pagepost',
	) );
	
/*-- POST RELATED LIMIT --*/
	$wp_customize->add_setting( 'post_related_limit', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'post_related_limit', array(
		'label' => esc_html__( 'Post Related Count', 'translogistic' ),
        'type' => 'number',
		'section' => 'pagepost',
	) );	
/*-- PAGES & POSTS SETINGS END --*/

/*-- SOCIAL MEDIA START --*/
	$wp_customize->add_section( 'socialmedia', array(
		'title' => esc_html__( 'Social Media', 'translogistic' ),
	) );
	
/*-- FACEBOOK --*/
	$wp_customize->add_setting( 'translogistic_facebook', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_facebook', array(
		'label' => esc_html__( 'Facebook URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );

/*-- GOOGLE PLUS --*/
	$wp_customize->add_setting( 'translogistic_googleplus', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_googleplus', array(
		'label' => esc_html__( 'Google+ URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
	
/*-- INSTAGRAM --*/
	$wp_customize->add_setting( 'translogistic_instagram', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_instagram', array(
		'label' => esc_html__( 'Instagram URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
	
/*-- LINKEDIN --*/
	$wp_customize->add_setting( 'translogistic_linkedin', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_linkedin', array(
		'label' => esc_html__( 'Linkedin URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
	
/*-- VINE --*/
	$wp_customize->add_setting( 'translogistic_vine', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_vine', array(
		'label' => esc_html__( 'Vine URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
		
/*-- TWITTER --*/
	$wp_customize->add_setting( 'translogistic_twitter', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_twitter', array(
		'label' => esc_html__( 'Twitter URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
		
/*-- PINTEREST --*/
	$wp_customize->add_setting( 'translogistic_pinterest', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_pinterest', array(
		'label' => esc_html__( 'Pinterest URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
		
/*-- RSS --*/
	$wp_customize->add_setting( 'translogistic_rss', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_rss', array(
		'label' => esc_html__( 'RSS URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
	
/*-- YOUTUBE --*/
	$wp_customize->add_setting( 'translogistic_youtube', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_youtube', array(
		'label' => esc_html__( 'YouTube URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
	
/*-- BEHANCE --*/
	$wp_customize->add_setting( 'translogistic_behance', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_behance', array(
		'label' => esc_html__( 'Behance URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
	
/*-- DEVIANTART --*/
	$wp_customize->add_setting( 'translogistic_deviantart', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_deviantart', array(
		'label' => esc_html__( 'DeviantArt URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
	
/*-- DIGG --*/
	$wp_customize->add_setting( 'translogistic_digg', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_digg', array(
		'label' => esc_html__( 'Digg URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
	
/*-- DRIBBBLE --*/
	$wp_customize->add_setting( 'translogistic_dribbble', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_dribbble', array(
		'label' => esc_html__( 'Dribbble URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
	
/*-- FLICKR --*/
	$wp_customize->add_setting( 'translogistic_flickr', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_flickr', array(
		'label' => esc_html__( 'Flickr URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
	
/*-- GITHUB --*/
	$wp_customize->add_setting( 'translogistic_github', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_github', array(
		'label' => esc_html__( 'GitHub URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
	
/*-- LAST.FM --*/
	$wp_customize->add_setting( 'translogistic_lastfm', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_lastfm', array(
		'label' => esc_html__( 'Last.fm URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
	
/*-- REDDIT --*/
	$wp_customize->add_setting( 'translogistic_reddit', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_reddit', array(
		'label' => esc_html__( 'Reddit URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
	
/*-- SOUNDCLOUD --*/
	$wp_customize->add_setting( 'translogistic_soundcloud', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_soundcloud', array(
		'label' => esc_html__( 'SoundCloud URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
	
/*-- TUMBLR --*/
	$wp_customize->add_setting( 'translogistic_tumblr', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_tumblr', array(
		'label' => esc_html__( 'Tumblr URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
	
/*-- VIMEO --*/
	$wp_customize->add_setting( 'translogistic_vimeo', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_vimeo', array(
		'label' => esc_html__( 'Vimeo URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
	
/*-- VK --*/
	$wp_customize->add_setting( 'translogistic_vk', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_vk', array(
		'label' => esc_html__( 'VK URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );
	
/*-- MEDIUM --*/
	$wp_customize->add_setting( 'translogistic_medium', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'translogistic_medium', array(
		'label' => esc_html__( 'Medium URL', 'translogistic' ),
		'section' => 'socialmedia',
	) );

/*-- SOCIAL SHARE HIDE --*/
	$wp_customize->add_setting( 'social_share_hide', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'social_share_hide', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Post Social Share Hide', 'translogistic' ),
        'section' => 'socialmedia',
    ) );

/*-- SOCIAL SHARE ICONS --*/
	$wp_customize->add_setting( 'social_share_facebook', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'social_share_facebook', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Facebook Share Hide', 'translogistic' ),
        'section' => 'socialmedia',
    ) );

	$wp_customize->add_setting( 'social_share_twitter', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'social_share_twitter', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Twitter Share Hide', 'translogistic' ),
        'section' => 'socialmedia',
    ) );

	$wp_customize->add_setting( 'social_share_googleplus', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'social_share_googleplus', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Google+ Share Hide', 'translogistic' ),
        'section' => 'socialmedia',
    ) );

	$wp_customize->add_setting( 'social_share_linkedin', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'social_share_linkedin', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Linkedin Share Hide', 'translogistic' ),
        'section' => 'socialmedia',
    ) );

	$wp_customize->add_setting( 'social_share_pinterest', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'social_share_pinterest', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Pinterest Share Hide', 'translogistic' ),
        'section' => 'socialmedia',
    ) );

	$wp_customize->add_setting( 'social_share_reddit', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'social_share_reddit', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Reddit Share Hide', 'translogistic' ),
        'section' => 'socialmedia',
    ) );

	$wp_customize->add_setting( 'social_share_delicious', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'social_share_delicious', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Delicious Share Hide', 'translogistic' ),
        'section' => 'socialmedia',
    ) );

	$wp_customize->add_setting( 'social_share_stumbleupon', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'social_share_stumbleupon', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Stumbleupon Share Hide', 'translogistic' ),
        'section' => 'socialmedia',
    ) );

	$wp_customize->add_setting( 'social_share_tumblr', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'social_share_tumblr', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Tumblr Share Hide', 'translogistic' ),
        'section' => 'socialmedia',
    ) );
/*-- SOCIAL MEDIA END  --*/

/*-- FOOTER START --*/	
	$wp_customize->add_section( 'footer', array(
        'title' => esc_html__( 'Footer', 'translogistic' ),
    ) );
	
/*-- FOOTER HIDE --*/
	$wp_customize->add_setting( 'hide_footer', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'hide_footer', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Hide Footer', 'translogistic' ),
        'section' => 'footer',
    ) );
	
/*-- GO TOP HIDE --*/
	$wp_customize->add_setting( 'translogistic_go_top', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'translogistic_go_top', array(
        'type' => 'checkbox',
        'label' => esc_html__( 'Hide Go Top Icon', 'translogistic' ),
        'section' => 'footer',
    ) );
	
/*-- FOOTER TOP WIDGET AREA PAGE --*/
	$wp_customize->add_setting( 'footer_top_widget_page', array(
		'default' => esc_html__( '', 'translogistic' ),
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'footer_top_widget_page', array(
		'label' => esc_html__( 'Footer Top Widget Area Page', 'translogistic' ),
        'section' => 'footer',
		'type'           => 'dropdown-pages'
	) );
	
/*-- FOOTER BOTTOM WIDGET AREA PAGE --*/
	$wp_customize->add_setting( 'footer_bottom_widget_page', array(
		'default' => esc_html__( '', 'translogistic' ),
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( 'footer_bottom_widget_page', array(
		'label' => esc_html__( 'Footer Bottom Widget Area Page', 'translogistic' ),
        'section' => 'footer',
		'type'           => 'dropdown-pages'
	) );
/*-- FOOTER END --*/	

/*-- CUSTOM COLOR START --*/
/*-- WRAPPER BACKGROUND --*/	
	$wp_customize->add_setting( 'wrapper_background', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'wrapper_background', array(
		'label' => esc_html__( 'Wrapper Background', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'wrapper_background',
	) ) );
	
/*-- THEME COLOR ONE --*/	
	$wp_customize->add_setting( 'theme_color_one', array(
		'default' => '#01BA7C',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'theme_color_one', array(
		'label' => esc_html__( 'Theme Color One', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'theme_color_one',
	) ) );
	
/*-- THEME COLOR TWO --*/	
	$wp_customize->add_setting( 'theme_color_two', array(
		'default' => '#0CC688',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'theme_color_two', array(
		'label' => esc_html__( 'Theme Color Two', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'theme_color_two',
	) ) );
	
/*-- THEME COLOR THREE --*/	
	$wp_customize->add_setting( 'theme_color_three', array(
		'default' => '#0BA6DD',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'theme_color_three', array(
		'label' => esc_html__( 'Theme Color Three', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'theme_color_three',
	) ) );
	
/*-- THEME COLOR FOUR --*/	
	$wp_customize->add_setting( 'theme_color_four', array(
		'default' => '#3c5e74',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'theme_color_four', array(
		'label' => esc_html__( 'Theme Color Four', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'theme_color_four',
	) ) );
	
/*-- BODY TEXT COLOR --*/	
	$wp_customize->add_setting( 'body_text_color', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'body_text_color', array(
		'label' => esc_html__( 'Body Text Color', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'body_text_color',
	) ) );
	
/*-- HEADER MENU AREA BACKGROUND --*/	
	$wp_customize->add_setting( 'header_menu_area_background', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header_menu_area_background', array(
		'label' => esc_html__( 'Header Menu Area Background', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'header_menu_area_background',
	) ) );
	
/*-- HEADER LOGO AREA BACKGROUND --*/	
	$wp_customize->add_setting( 'header_logo_area_background', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header_logo_area_background', array(
		'label' => esc_html__( 'Header Logo Area Background', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'header_logo_area_background',
	) ) );
	
/*-- HEADER LOGO AREA FIXED BACKGROUND --*/	
	$wp_customize->add_setting( 'header_logo_area_background_fixed', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header_logo_area_background_fixed', array(
		'label' => esc_html__( 'Header Logo Area Fixed Background', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'header_logo_area_background_fixed',
	) ) );
	
/*-- HEADER MENU LINK COLOR --*/	
	$wp_customize->add_setting( 'header_menu_link_color', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header_menu_link_color', array(
		'label' => esc_html__( 'Header Menu Link Color', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'header_menu_link_color',
	) ) );
	
/*-- HEADER MENU LINK HOVER COLOR --*/	
	$wp_customize->add_setting( 'header_menu_link_hover_color', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header_menu_link_hover_color', array(
		'label' => esc_html__( 'Header Menu Link Hover Color', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'header_menu_link_hover_color',
	) ) );
	
/*-- HEADER MENU LINK HOVER BACKGROUND --*/	
	$wp_customize->add_setting( 'header_menu_link_hover_background_color', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header_menu_link_hover_background_color', array(
		'label' => esc_html__( 'Header Menu Link Hover Background Color', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'header_menu_link_hover_background_color',
	) ) );
	
/*-- HEADER SUBMENU BACKGROUND --*/	
	$wp_customize->add_setting( 'header_sub_menu_background', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header_sub_menu_background', array(
		'label' => esc_html__( 'Header Submenu Background', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'header_sub_menu_background',
	) ) );
	
/*-- HEADER SUBMENU BORDER COLOR --*/	
	$wp_customize->add_setting( 'header_sub_menu_border_color', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header_sub_menu_border_color', array(
		'label' => esc_html__( 'Header Submenu Border Color', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'header_sub_menu_border_color',
	) ) );
	
/*-- HEADER SUBMENU LINK COLOR --*/	
	$wp_customize->add_setting( 'header_sub_menu_link_color', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header_sub_menu_link_color', array(
		'label' => esc_html__( 'Header Submenu Link Color', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'header_sub_menu_link_color',
	) ) );
	
/*-- HEADER SUBMENU LINK HOVER COLOR --*/	
	$wp_customize->add_setting( 'header_sub_menu_link_hover_color', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header_sub_menu_link_hover_color', array(
		'label' => esc_html__( 'Header Submenu Link Hover Color', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'header_sub_menu_link_hover_color',
	) ) );
	
/*-- HEADING COLOR --*/	
	$wp_customize->add_setting( 'heading_color', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'heading_color', array(
		'label' => esc_html__( 'Heading Color', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'heading_color',
	) ) );

/*-- LINK COLOR --*/	
	$wp_customize->add_setting( 'link_color', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'link_color', array(
		'label' => esc_html__( 'Link Color', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'link_color',
	) ) );

/*-- LINK HOVER COLOR --*/	
	$wp_customize->add_setting( 'link_hover_color', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'link_hover_color', array(
		'label' => esc_html__( 'Link Hover Color', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'link_hover_color',
	) ) );

/*-- FOOTER TOP BACKGROUND --*/	
	$wp_customize->add_setting( 'footer_top_background', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_top_background', array(
		'label' => esc_html__( 'Footer Top Background', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'footer_top_background',
	) ) );

/*-- FOOTER TOP BORDER COLOR --*/	
	$wp_customize->add_setting( 'footer_top_border', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_top_border', array(
		'label' => esc_html__( 'Footer Top Border Color', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'footer_top_border',
	) ) );

/*-- FOOTER BOTTOM BACKGROUND --*/	
	$wp_customize->add_setting( 'footer_bottom_background', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_bottom_background', array(
		'label' => esc_html__( 'Footer Bottom Background', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'footer_bottom_background',
	) ) );

/*-- FOOTER TEXT COLOR --*/	
	$wp_customize->add_setting( 'footer_text_color', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_text_color', array(
		'label' => esc_html__( 'Footer Text Color', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'footer_text_color',
	) ) );

/*-- INPUT BORDER COLOR --*/	
	$wp_customize->add_setting( 'input_border_color', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'input_border_color', array(
		'label' => esc_html__( 'Input Border Color', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'input_border_color',
	) ) );

/*-- INPUT TEXT COLOR --*/	
	$wp_customize->add_setting( 'input_text_color', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'input_text_color', array(
		'label' => esc_html__( 'Input Text Color', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'input_text_color',
	) ) );

/*-- INPUT PLACEHOLDER COLOR --*/	
	$wp_customize->add_setting( 'input_placeholder_color', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'input_placeholder_color', array(
		'label' => esc_html__( 'Input Placeholder Color', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'input_placeholder_color',
	) ) );

/*-- INPUT BACKGROUND --*/	
	$wp_customize->add_setting( 'input_background', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'input_background', array(
		'label' => esc_html__( 'Input Background', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'input_background',
	) ) );

/*-- BUTTON BACKGROUND --*/	
	$wp_customize->add_setting( 'button_background', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'button_background', array(
		'label' => esc_html__( 'Button Background', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'button_background',
	) ) );

/*-- BUTTON BACKGROUND --*/	
	$wp_customize->add_setting( 'button_hover_background', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'button_hover_background', array(
		'label' => esc_html__( 'Button Hover Background', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'button_hover_background',
	) ) );

/*-- BUTTON COLOR --*/	
	$wp_customize->add_setting( 'button_color', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'button_color', array(
		'label' => esc_html__( 'Button Color', 'translogistic' ),
		'section' => 'colors',
		'settings' => 'button_color',
	) ) );
/*-- CUSTOM COLOR END --*/	

/*-- CUSTOM CODE START --*/	
	$wp_customize->add_section( 'customcode', array(
        'title' => esc_html__( 'Custom Code', 'translogistic' ),
        'description' => esc_html__( 'Your custom code..', 'translogistic' ),
    ) );
	
/*-- CUSTOM CSS  --*/
	$wp_customize->add_setting( 'custom_css', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'custom_css', array(
		'label' => esc_html__( 'Custom CSS', 'translogistic' ),
        'type'       => 'textarea',
		'section' => 'customcode',
	) );
	
/*-- CUSTOM JS  --*/
	$wp_customize->add_setting( 'custom_js', array(
		'default' => '',
		'sanitize_callback' => 'translogistic_santanize'
	) );

	$wp_customize->add_control( 'custom_js', array(
		'label' => esc_html__( 'Custom JS', 'translogistic' ),
        'type'       => 'textarea',
		'section' => 'customcode',
	) );
/*-- CUSTOM CODE END --*/

}
add_action( 'customize_register', 'translogistic_customizer', 11 );

/*-- THEME CUSTOM STYLE FUNCTION START --*/
function translogistic_custom_style() {
	?>
		<style type="text/css">
			<?php
			$heading_font = get_theme_mod( 'heading-font' );
			$text_font = get_theme_mod( 'text-font' );
			$menu_font = get_theme_mod( 'menu-font' );
			$theme_one_font = get_theme_mod( 'theme-one-font' );
			$theme_two_font = get_theme_mod( 'theme-two-font' );
			
			$font_family_style = get_theme_mod( 'font_family_style' );
			if( !$font_family_style == '' ):
				$font_family_style = get_theme_mod( 'font_family_style' );
			else:
				$font_family_style = '';
			endif;
			
			$font_cyrillic_ext = get_theme_mod( 'font_cyrillic_ext' );
			if( $font_cyrillic_ext == '1' ):
				$font_cyrillic_ext = ',cyrillic-ext';
			else:
				$font_cyrillic_ext = '';
			endif;
			
			$font_greek_ext = get_theme_mod( 'font_greek_ext' );
			if( $font_greek_ext == '1' ):
				$font_greek_ext = ',greek-ext';
			else:
				$font_greek_ext = '';
			endif;
			
			$font_greek = get_theme_mod( 'font_greek' );
			if( $font_greek == '1' ):
				$font_greek = ',greek';
			else:
				$font_greek = '';
			endif;
			
			$font_vietnamese = get_theme_mod( 'font_vietnamese' );
			if( $font_vietnamese == '1' ):
				$font_vietnamese = ',vietnamese';
			else:
				$font_vietnamese = '';
			endif;
			
			$font_cyrillic = get_theme_mod( 'font_cyrillic' );
			if( $font_cyrillic == '1' ):
				$font_cyrillic = ',cyrillic';
			else:
				$font_cyrillic = '';
			endif;
			
			$font_character_set_select = $font_cyrillic_ext . $font_greek_ext . $font_greek . $font_vietnamese . $font_cyrillic;
			
			if( !$heading_font == '' and $heading_font != 'Select' ):
				$google_heading_font = str_replace(' ', '+', $heading_font);
			?>
				@import url(https://fonts.googleapis.com/css?family=<?php echo esc_attr( $google_heading_font ); ?>:<?php echo esc_attr( $font_family_style ); ?>&subset=latin,latin-ext<?php echo esc_attr( $font_character_set_select ); ?>);
			<?php endif;
			if( !$text_font == '' and $text_font != 'Select' ):
				$google_text_font = str_replace(' ', '+', $text_font);
			?>
				@import url(https://fonts.googleapis.com/css?family=<?php echo esc_attr( $google_text_font ); ?>:<?php echo esc_attr( $font_family_style ); ?>&subset=latin,latin-ext<?php echo esc_attr( $font_character_set_select ); ?>);
			<?php endif;
			if( !$menu_font == '' and $menu_font != 'Select' ):
				$google_menu_font = str_replace(' ', '+', $menu_font);
			?>
				@import url(https://fonts.googleapis.com/css?family=<?php echo esc_attr( $google_menu_font ); ?>:<?php echo esc_attr( $font_family_style ); ?>&subset=latin,latin-ext<?php echo esc_attr( $font_character_set_select ); ?>);
			<?php endif;
			if( !$theme_one_font == '' and $theme_one_font != 'Select' ):
				$google_theme_one_font = str_replace(' ', '+', $theme_one_font);
			?>
				@import url(https://fonts.googleapis.com/css?family=<?php echo esc_attr( $google_theme_one_font ); ?>:<?php echo esc_attr( $font_family_style ); ?>&subset=latin,latin-ext<?php echo esc_attr( $font_character_set_select ); ?>);
			<?php else: ?>
				@import url(https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,900,700,600,300,200);
				@import url(https://fonts.googleapis.com/css?family=Roboto:400,900,700,500,300,100);
			<?php endif;
			if( !$theme_two_font == '' and $theme_two_font != 'Select' ):
				$google_theme_two_font = str_replace(' ', '+', $theme_two_font);
			?>
				@import url(https://fonts.googleapis.com/css?family=<?php echo esc_attr( $google_theme_two_font ); ?>:<?php echo esc_attr( $font_family_style ); ?>&subset=latin,latin-ext<?php echo esc_attr( $font_character_set_select ); ?>);
			<?php endif;
			if( !$theme_one_font == '' and $theme_one_font != 'Select' ):
			?>
				body, .list-box h3, .icon-list h3, .vc_tta.vc_general .vc_tta-panel-title {
					font-family: '<?php echo esc_attr( $theme_one_font ); ?>';
				}
			<?php endif;
			if( !$theme_two_font == '' and $theme_two_font != 'Select' ):
			?>
				h1, h2, h3, h4, h5, h6, .post-pagination ul, .departments-content .department-button a, .departments-content .department-button a:visited, .translogistic-button.bluetransparent a, .translogistic-button.bluetransparent a:visited, .translogistic-button.greentransparent a, .translogistic-button.greentransparent a:visited, .feature-box .feature-box-content .button a, .feature-box .feature-box-content .button a:visited, .cargo-tracking-form.white input[type="submit"], .cargo-tracking-list-form .cargo-tracking-list-form-header .cargo-tracking-list-form-col, .contact-information .contact-information-row.contact-information-custom-row, .getaquote input[type="submit"], .translogistic-latest-posts-widget, .translogistic-latest-posts-widget, .footer-newsletter input[type="email"], .footer-newsletter input[type="submit"], .page404 .content404 .top-desc, .post-bottom-element .post-related .desc {
					font-family: '<?php echo esc_attr( $theme_two_font ); ?>';
				}
			<?php endif;
			if( !$heading_font == '' and $heading_font != 'Select' ):
			?>
				h1, h2, h3, h4, h5, h6 {
					font-family: '<?php echo esc_attr( $heading_font ); ?>';
				}
			<?php endif;
			if( !$text_font == '' and $text_font != 'Select' ):
			?>
				body {
					font-family: '<?php echo esc_attr( $text_font ); ?>';
				}
			<?php endif;
			if( !$menu_font == '' and $menu_font != 'Select' ):
			?>
				.header-wrapper .menu-area{
					font-family: '<?php echo esc_attr( $menu_font ); ?>';
				}
			<?php endif;		
			$h1_font_size = get_theme_mod( 'h1_font_size' );
			if( !$h1_font_size == '' ):
			?>
				h1 {
					font-size: <?php echo esc_attr( $h1_font_size ); ?>px;
				}
			<?php endif;
			$h2_font_size = get_theme_mod( 'h2_font_size' );
			if( !$h2_font_size == '' ):
			?>
				h2 {
					font-size: <?php echo esc_attr( $h2_font_size ); ?>px;
				}
			<?php endif;
			$h3_font_size = get_theme_mod( 'h3_font_size' );
			if( !$h3_font_size == '' ):
			?>
				h3 {
					font-size: <?php echo esc_attr( $h3_font_size ); ?>px;
				}
			<?php endif;
			$h4_font_size = get_theme_mod( 'h4_font_size' );
			if( !$h4_font_size == '' ):
			?>
				h4 {
					font-size: <?php echo esc_attr( $h4_font_size ); ?>px;
				}
			<?php endif;
			$h5_font_size = get_theme_mod( 'h5_font_size' );
			if( !$h5_font_size == '' ):
			?>
				h5 {
					font-size: <?php echo esc_attr( $h5_font_size ); ?>px;
				}
			<?php endif;
			$h6_font_size = get_theme_mod( 'h6_font_size' );
			if( !$h6_font_size == '' ):
			?>
				h6 {
					font-size: <?php echo esc_attr( $h6_font_size ); ?>px;
				}
			<?php endif;
			$body_font_size = get_theme_mod( 'body_font_size' );
			if( !$body_font_size == '' ):
			?>
				body {
					font-size: <?php echo esc_attr( $body_font_size ); ?>px;
				}
			<?php endif;
			$body_line_height = get_theme_mod( 'body_line_height' );
			if( !$body_line_height == '' ):
			?>
				body {
					line-height: <?php echo esc_attr( $body_line_height ); ?>;
				}
			<?php endif;
			$menu_font_size = get_theme_mod( 'menu_font_size' );
			if( !$menu_font_size == '' ):
			?>
				@media (min-width: 992px) {
					.header-wrapper .menu-area .navbar-nav li a, .header-wrapper .menu-area .navbar-nav li a:visited {
						font-size: <?php echo esc_attr( $menu_font_size ); ?>px;
					}
				}
			<?php endif;
			$sub_menu_font_size = get_theme_mod( 'sub_menu_font_size' );
			if( !$sub_menu_font_size == '' ):
			?>
				@media (min-width: 768px) {
					.header-wrapper .menu-area .navbar-nav li ul li a, .header-wrapper .menu-area .navbar-nav li ul li a:visited {
						font-size: <?php echo esc_attr( $sub_menu_font_size ); ?>px;
					}
				}
			<?php endif;
			$post_content_font_size = get_theme_mod( 'post_content_font_size' );
			if( !$post_content_font_size == '' ):
			?>
				.category-post-list .post-content {
					font-size: <?php echo esc_attr( $post_content_font_size ); ?>px;
				}
			<?php endif;
			$post_title_font_size = get_theme_mod( 'post_title_font_size' );
			if( !$post_title_font_size == '' ):
			?>
				.category-post-list .post-wrapper .post-header h2 {
					font-size: <?php echo esc_attr( $post_title_font_size ); ?>px;
				}
			<?php endif;
			$page_content_font_size = get_theme_mod( 'page_content_font_size' );
			if( !$page_content_font_size == '' ):
			?>
				.page-content .page-content-bottom {
					font-size: <?php echo esc_attr( $page_content_font_size ); ?>px;
				}
			<?php endif;
			$page_title_font_size = get_theme_mod( 'page_title_font_size' );
			if( !$page_title_font_size == '' ):
			?>
				.page-title-wrapper h1 {
					font-size: <?php echo esc_attr( $page_title_font_size ); ?>px;
				}
			<?php endif;
			$page_excerpt_font_size = get_theme_mod( 'page_excerpt_font_size' );
			if( !$page_excerpt_font_size == '' ):
			?>
				.page-title-wrapper p {
					font-size: <?php echo esc_attr( $page_excerpt_font_size ); ?>px;
				}
			<?php endif;
			$footer_content_font_size = get_theme_mod( 'footer_content_font_size' );
			if( !$footer_content_font_size == '' ):
			?>
				.footer .footer-bottom {
					font-size: <?php echo esc_attr( $footer_content_font_size ); ?>px;
				}
			<?php endif;
			$error_text_one_font_size = get_theme_mod( 'error_text_one_font_size' );
			if( !$error_text_one_font_size == '' ):
			?>
				.page404 .content404 .top-desc {
					font-size: <?php echo esc_attr( $error_text_one_font_size ); ?>px;
				}
			<?php endif;
			$error_text_two_font_size = get_theme_mod( 'error_text_two_font_size' );
			if( !$error_text_two_font_size == '' ):
			?>
				.page404 .content404 .bottom-desc {
					font-size: <?php echo esc_attr( $error_text_two_font_size ); ?>px;
				}
			<?php endif;
			$input_font_size = get_theme_mod( 'input_font_size' );
			if( !$input_font_size == '' ):
			?>
				input[type="email"], input[type="number"], input[type="password"], input[type="tel"], input[type="url"], input[type="text"], input[type="time"], input[type="week"], input[type="search"], input[type="month"], input[type="datetime"], input[type="date"], textarea, textarea.form-control, select, .select2-container .select2-choice, .form-control {
					font-size: <?php echo esc_attr( $input_font_size ); ?>px;
				}
			<?php endif;
			$button_font_size = get_theme_mod( 'button_font_size' );
			if( !$button_font_size == '' ):
			?>
				button.alternative-button, input[type="submit"].alternative-button, button, input[type="submit"], .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button {
					font-size: <?php echo esc_attr( $button_font_size ); ?>px;
				}
			<?php endif;
			
			$wrapper_background = get_theme_mod( 'wrapper_background' ); 
			if( !$wrapper_background == '' ) :
			?>
				.translogistic-wrapper {
					background:<?php echo esc_attr( $wrapper_background ); ?>;
				}
			<?php endif;

			$theme_color_one = get_theme_mod( 'theme_color_one' ); 
			if( !$theme_color_one == '' ) :
			?>
				.footer .go-top-icon i, .footer .footer-bottom, .header-wrapper .menu-area, .cargo-tracking-list-form .cargo-tracking-list-form-header .cargo-tracking-list-form-col, .cargo-tracking-form.themeonecolor .input-group-addon, .cargo-tracking-form.themeonecolor input[type="submit"], .testimonial-block-widget .testimonial-block-widget-content-area-columns.themeonecolor .testimonial-block-widget-content-area .testimonial-block-item-name:before, .feature-box .feature-box-content, .translogistic-button.themeonecolor a:hover, .translogistic-button.themeonecolor a:focus, .departments-content .department-button a:hover, .departments-content .department-button a:focus, .edit-link a, .edit-link a:visited, .sk-folding-cube .sk-cube:before {
					background:<?php echo esc_attr( $theme_color_one ); ?>;
				}
				
				.page404 .content404 .bottom-desc a:hover, .page404 .content404 .bottom-desc a:focus, .page404 .content404 .top-desc span, .footer .go-top-icon i:hover, .footer .go-top-icon i:focus, .header-wrapper .logo-area .header-search button, .header-wrapper .header-social-media li a:hover, .header-wrapper .header-social-media li a:focus, .contact-information .contact-information-row .contact-information-icon, .chronologyyears-content.themeonecolor, .feature-box.themetwocolor .feature-box-content .feature-box-icon, .feature-box .feature-box-content .feature-box-icon, .feature-box .feature-box-content .button a:hover, .feature-box .feature-box-content .button a:focus, .translogistic-button.themeonecolor a, .translogistic-button.themeonecolor a:visited, .departments-content .department-button a, .departments-content .department-button a:visited {
					color:<?php echo esc_attr( $theme_color_one ); ?>;
				}
				
				.contact-information .contact-information-row .contact-information-icon, .translogistic-button.themeonecolor a:hover, .translogistic-button.themeonecolor a:focus, .translogistic-button.themeonecolor a, .translogistic-button.themeonecolor a:visited, .departments-content .department-button a:hover, .departments-content .department-button a:focus, .departments-content .image .department-icon, .departments-content .department-button a, .departments-content .department-button a:visited {
					border-color:<?php echo esc_attr( $theme_color_one ); ?>;
				}
				
				.testimonial-block-widget .testimonial-block-widget-content-area-columns.themeonecolor .testimonial-block-widget-content-area .testimonial-block-item-content {
					border-left-color:<?php echo esc_attr( $theme_color_one ); ?>;
				}
				
				.chronologyyears-content.themeonecolor .separate {
					border-top-color:<?php echo esc_attr( $theme_color_one ); ?>;
				}
				
				@media (min-width: 768px) {
					.header-wrapper .menu-area .navbar-nav li ul li a:hover, .header-wrapper .menu-area .navbar-nav li ul li a:focus, .header-wrapper .menu-area .navbar-nav>li:hover>a, .header-wrapper .menu-area .navbar-nav>li:focus>a, .header-wrapper .menu-area .navbar-nav>li>a:hover, .header-wrapper .menu-area .navbar-nav>li>a:focus {
						color:<?php echo esc_attr( $theme_color_one ); ?>;
					}
				}
			<?php endif;

			$theme_color_two = get_theme_mod( 'theme_color_two' ); 
			if( !$theme_color_two == '' ) :
			?>
				.cargo-tracking-list-form .cargo-tracking-list-form-header.themetwocolor .cargo-tracking-list-form-col, .cargo-tracking-form.themetwocolor .input-group-addon, .cargo-tracking-form.themetwocolor input[type="submit"], .testimonial-block-widget .testimonial-block-widget-content-area-columns.themetwocolor .testimonial-block-widget-content-area .testimonial-block-item-name:before, .feature-box.themetwocolor .feature-box-content, .translogistic-button.themetwocolor a:hover, .translogistic-button.themetwocolor a:focus, .departments-content .department-button.themetwocolor a:hover, .departments-content .department-button.themetwocolor a:focus {
					background:<?php echo esc_attr( $theme_color_two ); ?>;
				}
				
				.header-wrapper .logo-area .header-contact .header-contact-phone .content span, .header-wrapper .logo-area .header-contact .header-contact-phone i, .header-wrapper .logo-area .header-contact .header-contact-email .content i, .contact-information.themetwocolor .contact-information-row .contact-information-icon , .chronologyyears-content.themetwocolor, .feature-box.themetwocolor .feature-box-content .button a:hover, .feature-box.themetwocolor .feature-box-content .button a:focus, .translogistic-button.themetwocolor a, .translogistic-button.themetwocolor a:visited, .departments-content .department-button.themetwocolor a, .departments-content .department-button.themetwocolor a:visited {
					color:<?php echo esc_attr( $theme_color_two ); ?>;
				}
				
				.contact-information.themetwocolor .contact-information-row .contact-information-icon, .translogistic-button.themetwocolor a:hover, .translogistic-button.themetwocolor a:focus, .translogistic-button.themetwocolor a, .translogistic-button.themetwocolor a:visited, .departments-content .department-button.themetwocolor a:hover, .departments-content .department-button.themetwocolor a:focus, .departments-content .department-button.themetwocolor a, .departments-content .department-button.themetwocolor a:visited {
					border-color:<?php echo esc_attr( $theme_color_two ); ?>;
				}
				
				.testimonial-block-widget .testimonial-block-widget-content-area-columns.themetwocolor .testimonial-block-widget-content-area .testimonial-block-item-content {
					border-left-color:<?php echo esc_attr( $theme_color_two ); ?>;
				}
				
				.chronologyyears-content.themetwocolor .separate {
					border-top-color:<?php echo esc_attr( $theme_color_two ); ?>;
				}
			<?php endif;

			$theme_color_three = get_theme_mod( 'theme_color_three' ); 
			if( !$theme_color_three == '' ) :
			?>
				.post-bottom-element .post-related .desc .category, .category-post-list .post-wrapper .single-tag-list span.single-tag-list-title, .category-post-list .post-wrapper .page-links span.page-links-title, .category-post-list .post-wrapper .post-bottom .post-share ul li a:hover, .category-post-list .post-wrapper .post-bottom .post-share ul li a:focus, .category-post-list .post-wrapper .post-bottom .more:hover, .category-post-list .post-wrapper .post-bottom .more:focus, .category-post-list .post-wrapper .post-header .category, .comments-area .comment-form .form-submit input[type="submit"]:hover, .comments-area .comment-form .form-submit input[type="submit"]:hover:visited, .comments-area .comment-form .form-submit input[type="submit"]:hover:focus, .comments-area .comment-form .form-submit input[type="submit"]:focus, .comments-area .comment-form .form-submit input[type="submit"]:active, .comments-area .comment-form .form-submit input[type="submit"]:active:hover, .comments-area .comment-form .form-submit input[type="submit"]:active:visited, .translogistic-latest-posts-widget .desc .category, .tagcloud a:hover, .tagcloud a:focus, .widget-box:hover .widget-title:before, .page-title-wrapper p:after, .cargo-tracking-list-form .cargo-tracking-list-form-header.themethreecolor .cargo-tracking-list-form-col, .cargo-tracking-form.themethreecolor .input-group-addon, .cargo-tracking-form.themethreecolor input[type="submit"], .cargo-tracking-form.querypageversion .input-group-addon, .cargo-tracking-form.querypageversion input[type="submit"], .cargo-tracking-form.slideversionv2 .input-group-addon, .cargo-tracking-form.slideversionv2 input[type="submit"], .cargo-tracking-form .input-group-addon, .testimonial-block-widget .testimonial-block-widget-content-area-columns.themethreecolor .testimonial-block-widget-content-area .testimonial-block-item-name:before, .feature-box.themethreecolor .feature-box-content, .translogistic-button.themethreecolor a:hover, .translogistic-button.themethreecolor a:focus, .list-box:hover .list-box-icon, .departments-content .department-button.themethreecolor a:hover, .departments-content .department-button.themethreecolor a:focus, .post-pagination ul li:hover, .post-pagination ul li:focus, button.alternative-button:hover, input[type="submit"].alternative-button:hover, button.alternative-button:focus, input[type="submit"].alternative-button:focus, button.alternative-button:active, input[type="submit"].alternative-button:active, button.alternative-button:active:hover, input[type="submit"].alternative-button:active:hover, button.alternative-button:active:focus, input[type="submit"].alternative-button:active:focus, button.alternative-button:active:visited, input[type="submit"].alternative-button:active:visited, button, input[type="submit"], .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button {
					background:<?php echo esc_attr( $theme_color_three ); ?>;
				}
				
				.woocommerce div.product p.price, .woocommerce div.product span.price, .woocommerce ul.products li.product .price, .category-post-list .post-wrapper .post-bottom .post-share ul li a, .category-post-list .post-wrapper .post-bottom .post-share ul li a:visited, .category-post-list .post-wrapper .post-bottom .more, .category-post-list .post-wrapper .post-bottom .more:visited, .category-post-list .post-wrapper .post-header h2 a:hover, .category-post-list .post-wrapper .post-header h2 a:focus, .comments-area .comment-form .form-submit input[type="submit"], .footer-newsletter i, .translogistic-latest-posts-widget ul li h3 a:hover, .translogistic-latest-posts-widget ul li h3 a:focus, .translogistic-button.themethreecolor a, .translogistic-button.themethreecolor a:visited, .contact-information.themethreecolor .contact-information-row .contact-information-icon, .chronologyyears-content.themethreecolor, .feature-box.themethreecolor .feature-box-content .feature-box-icon, .feature-box.themethreecolor .feature-box-content .button a:hover, .feature-box.themethreecolor .feature-box-content .button a:focus, .list-box .list-box-icon, .departments-content .department-button.themethreecolor a, .departments-content .department-button.themethreecolor a:visited, .services-box .services-box-icon, .post-pagination ul li a, .post-pagination ul li a:visited, .post-pagination ul li span, .post-pagination ul li, button.alternative-button, input[type="submit"].alternative-button, a:hover, a:focus {
					color:<?php echo esc_attr( $theme_color_three ); ?>;
				}
				
				.post-bottom-element .post-related h4:before, .comments-area .comment-reply-title h2:before, .category-post-list .post-wrapper .post-bottom .post-share ul li a, .category-post-list .post-wrapper .post-bottom .post-share ul li a:visited, .category-post-list .post-wrapper .post-bottom .more, .category-post-list .post-wrapper .post-bottom .more:visited, .comments-area .comment-form .form-submit input[type="submit"], .tagcloud a:hover, .tagcloud a:focus, .widget-title:before, .vc_tta-accordion.faq-accordion .vc_tta-panel .vc_tta-panel-title>a:hover i.vc_tta-controls-icon::before, .vc_tta-accordion.faq-accordion .vc_tta-panel .vc_tta-panel-title>a:focus i.vc_tta-controls-icon::before, .vc_tta-accordion.faq-accordion .vc_tta-panel.vc_active .vc_tta-panel-title>a i.vc_tta-controls-icon::before, .page-title-wrapper h1:before,  .contact-information.themethreecolor .contact-information-row .contact-information-icon, .translogistic-button.themethreecolor a:hover, .translogistic-button.themethreecolor a:focus, .translogistic-button.themethreecolor a, .translogistic-button.themethreecolor a:visited, .list-box .list-box-icon, .departments-content .department-button.themethreecolor a:hover, .departments-content .department-button.themethreecolor a:focus, .departments-content .department-button.themethreecolor a, .departments-content .department-button.themethreecolor a:visited, .services-box .services-box-icon, .post-pagination ul li, button.alternative-button:hover, input[type="submit"].alternative-button:hover, button.alternative-button:focus, input[type="submit"].alternative-button:focus, button.alternative-button:active, input[type="submit"].alternative-button:active, button.alternative-button:active:hover, input[type="submit"].alternative-button:active:hover, button.alternative-button:active:focus, input[type="submit"].alternative-button:active:focus, button.alternative-button:active:visited, input[type="submit"].alternative-button:active:visited, button.alternative-button, input[type="submit"].alternative-button {
					border-color:<?php echo esc_attr( $theme_color_three ); ?>;
				}
				
				.list-box:hover .list-box-icon {
					border-color:<?php echo esc_attr( $theme_color_three ); ?> !important;
				}
				
				.vc_tta-accordion.faq-accordion .vc_tta-panel.vc_active .vc_tta-panel-title>a, .list-box:hover .list-box-icon.white i, .vc_tta-accordion.faq-accordion .vc_tta-panel .vc_tta-panel-title>a:hover, .vc_tta-accordion.faq-accordion .vc_tta-panel .vc_tta-panel-title>a:focus {
					color:<?php echo esc_attr( $theme_color_three ); ?> !important;
				}
				
				.testimonial-block-widget .testimonial-block-widget-content-area-columns.themethreecolor .testimonial-block-widget-content-area .testimonial-block-item-content {
					border-left-color:<?php echo esc_attr( $theme_color_three ); ?>;
				}
				
				.chronologyyears-content.themethreecolor .separate {
					border-top-color:<?php echo esc_attr( $theme_color_three ); ?>;
				}
				
				.woocommerce .widget_price_filter .ui-slider .ui-slider-range, .woocommerce .widget_price_filter .ui-slider .ui-slider-handle, .woocommerce #respond input#submit.alt.disabled, .woocommerce #respond input#submit.alt.disabled:hover, .woocommerce #respond input#submit.alt:disabled, .woocommerce #respond input#submit.alt:disabled:hover, .woocommerce #respond input#submit.alt:disabled[disabled], .woocommerce #respond input#submit.alt:disabled[disabled]:hover, .woocommerce a.button.alt.disabled, .woocommerce a.button.alt.disabled:hover, .woocommerce a.button.alt:disabled, .woocommerce a.button.alt:disabled:hover, .woocommerce a.button.alt:disabled[disabled], .woocommerce a.button.alt:disabled[disabled]:hover, .woocommerce button.button.alt.disabled, .woocommerce button.button.alt.disabled:hover, .woocommerce button.button.alt:disabled, .woocommerce button.button.alt:disabled:hover, .woocommerce button.button.alt:disabled[disabled], .woocommerce button.button.alt:disabled[disabled]:hover, .woocommerce input.button.alt.disabled, .woocommerce input.button.alt.disabled:hover, .woocommerce input.button.alt:disabled, .woocommerce input.button.alt:disabled:hover, .woocommerce input.button.alt:disabled[disabled], .woocommerce input.button.alt:disabled[disabled]:hover, .woocommerce span.onsale {
					background-color:<?php echo esc_attr( $theme_color_three ); ?>;
				}
			<?php endif;

			$theme_color_four = get_theme_mod( 'theme_color_four' ); 
			if( !$theme_color_four == '' ) :
			?>
				.chronologyyears-content, .getaquote input[type="submit"], .getaquote .input-group-addon, .cargo-tracking-list-form .cargo-tracking-list-form-header.themefourcolor .cargo-tracking-list-form-col, .cargo-tracking-form.themefourcolor .input-group-addon, .cargo-tracking-form.themefourcolor input[type="submit"], .testimonial-block-widget .testimonial-block-widget-content-area-columns.themefourcolor .testimonial-block-widget-content-area .testimonial-block-item-name:before, .feature-box.themefourcolor .feature-box-content, .translogistic-button.themefourcolor a:hover, .translogistic-button.themefourcolor a:focus, .departments-content .department-button.themefourcolor a:hover, .departments-content .department-button.themefourcolor a:focus {
					background:<?php echo esc_attr( $theme_color_four ); ?>;
				}
				
				.header-wrapper .logo-area .header-contact h2, .page404 .content404 .top-desc, .page404 .content404 h2, .getaquote input[type="submit"]:hover, .getaquote input[type="submit"]:focus, .contact-information.themefourcolor .contact-information-row .contact-information-icon, .chronologyyears-content.themefourcolor, .feature-box.themefourcolor .feature-box-content .feature-box-icon, .feature-box.themefourcolor .feature-box-content .button a:hover, .feature-box.themefourcolor .feature-box-content .button a:focus, .translogistic-button.themefourcolor a, .translogistic-button.themefourcolor a:visited, .departments-content .department-button.themefourcolor a, .departments-content .department-button.themefourcolor a:visited {
					color:<?php echo esc_attr( $theme_color_four ); ?>;
				}
				
				.contact-information.themefourcolor .contact-information-row .contact-information-icon, .translogistic-button.themefourcolor a:hover, .translogistic-button.themefourcolor a:focus, .translogistic-button.themefourcolor a, .translogistic-button.themefourcolor a:visited, .departments-content .department-button.themefourcolor a:hover, .departments-content .department-button.themefourcolor a:focus, .departments-content .department-button.themefourcolor a, .departments-content .department-button.themefourcolor a:visited {
					border-color:<?php echo esc_attr( $theme_color_four ); ?>;
				}
				
				{
					border-color:<?php echo esc_attr( $theme_color_four ); ?> !important;
				}
				
				{
					color:<?php echo esc_attr( $theme_color_four ); ?> !important;
				}
				
				.testimonial-block-widget .testimonial-block-widget-content-area-columns.themefourcolor .testimonial-block-widget-content-area .testimonial-block-item-content {
					border-left-color:<?php echo esc_attr( $theme_color_four ); ?>;
				}
				
				.chronologyyears-content.themefourcolor .separate {
					border-top-color:<?php echo esc_attr( $theme_color_four ); ?>;
				}
				
				{
					background-color:<?php echo esc_attr( $theme_color_four ); ?>;
				}
			<?php endif;

			$body_text_color = get_theme_mod( 'body_text_color' ); 
			if( !$body_text_color == '' ) :
			?>
				body {
					color:<?php echo esc_attr( $body_text_color ); ?>;
				}
			<?php endif;

			$header_menu_area_background = get_theme_mod( 'header_menu_area_background' ); 
			if( !$header_menu_area_background == '' ) :
			?>
				.header-wrapper .menu-area {
					background:<?php echo esc_attr( $header_menu_area_background ); ?>;
				}
			<?php endif;

			$header_logo_area_background = get_theme_mod( 'header_logo_area_background' ); 
			if( !$header_logo_area_background == '' ) :
			?>
				.header-wrapper .logo-area {
					background:<?php echo esc_attr( $header_logo_area_background ); ?>;
				}
			<?php endif;

			$header_logo_area_background_fixed = get_theme_mod( 'header_logo_area_background_fixed' ); 
			if( !$header_logo_area_background_fixed == '' ) :
			?>
				.header-wrapper.fixed-header-class .logo-area {
					background:<?php echo esc_attr( $header_logo_area_background_fixed ); ?>;
				}
			<?php endif;

			$header_menu_link_color = get_theme_mod( 'header_menu_link_color' ); 
			if( !$header_menu_link_color == '' ) :
			?>
				@media (min-width: 768px) {
					.header-wrapper .menu-area .navbar-nav li a, .header-wrapper .menu-area .navbar-nav li a:visited {
						color:<?php echo esc_attr( $header_menu_link_color ); ?>;
					}
				}
			<?php endif;

			$header_menu_link_hover_color = get_theme_mod( 'header_menu_link_hover_color' ); 
			if( !$header_menu_link_hover_color == '' ) :
			?>
				@media (min-width: 768px) {
					.header-wrapper .menu-area .navbar-nav>li:hover>a, .header-wrapper .menu-area .navbar-nav>li:focus>a, .header-wrapper .menu-area .navbar-nav>li>a:hover, .header-wrapper .menu-area .navbar-nav>li>a:focus {
						color:<?php echo esc_attr( $header_menu_link_hover_color ); ?>;
					}
				}
			<?php endif;

			$header_menu_link_hover_background_color = get_theme_mod( 'header_menu_link_hover_background_color' ); 
			if( !$header_menu_link_hover_background_color == '' ) :
			?>
				@media (min-width: 768px) {
					.header-wrapper .menu-area .navbar-nav>li:hover>a, .header-wrapper .menu-area .navbar-nav>li:focus>a, .header-wrapper .menu-area .navbar-nav>li>a:hover, .header-wrapper .menu-area .navbar-nav>li>a:focus {
						background:<?php echo esc_attr( $header_menu_link_hover_background_color ); ?>;
					}
				}
			<?php endif;

			$header_sub_menu_background = get_theme_mod( 'header_sub_menu_background' ); 
			if( !$header_sub_menu_background == '' ) :
			?>
				@media (min-width: 768px) {
					.header-wrapper .menu-area .navbar-nav li ul.dropdown-menu {
						background:<?php echo esc_attr( $header_sub_menu_background ); ?>;
					}
				}
			<?php endif;

			$header_sub_menu_border_color = get_theme_mod( 'header_sub_menu_border_color' ); 
			if( !$header_sub_menu_border_color == '' ) :
			?>
				@media (min-width: 768px) {
					.header-wrapper .menu-area .navbar-nav li ul.dropdown-menu {
						border-color:<?php echo esc_attr( $header_sub_menu_border_color ); ?>;
					}
				}
			<?php endif;

			$header_sub_menu_link_color = get_theme_mod( 'header_sub_menu_link_color' ); 
			if( !$header_sub_menu_link_color == '' ) :
			?>
				@media (min-width: 768px) {
					.header-wrapper .menu-area .navbar-nav li ul li a, .header-wrapper .menu-area .navbar-nav li ul li a:visited {
						color:<?php echo esc_attr( $header_sub_menu_link_color ); ?>;
					}
				}
			<?php endif;

			$header_sub_menu_link_hover_color = get_theme_mod( 'header_sub_menu_link_hover_color' ); 
			if( !$header_sub_menu_link_hover_color == '' ) :
			?>
				@media (min-width: 768px) {
					.header-wrapper .menu-area .navbar-nav li ul li a:hover, .header-wrapper .menu-area .navbar-nav li ul li a:focus {
						color:<?php echo esc_attr( $header_sub_menu_link_hover_color ); ?>;
					}
				}
			<?php endif;

			$heading_color = get_theme_mod( 'heading_color' ); 
			if( !$heading_color == '' ) :
			?>
				h1, h2, h3, h4, h5, h6 {
					color:<?php echo esc_attr( $heading_color ); ?>;
				}
			<?php endif;

			$link_color = get_theme_mod( 'link_color' ); 
			if( !$link_color == '' ) :
			?>
				a, a:visited {
					color:<?php echo esc_attr( $link_color ); ?>;
				}
			<?php endif;

			$link_hover_color = get_theme_mod( 'link_hover_color' ); 
			if( !$link_hover_color == '' ) :
			?>
				a:hover, a:focus {
					color:<?php echo esc_attr( $link_hover_color ); ?>;
				}
			<?php endif;

			$footer_top_background = get_theme_mod( 'footer_top_background' ); 
			if( !$footer_top_background == '' ) :
			?>
				.footer .footer-top {
					background:<?php echo esc_attr( $footer_top_background ); ?>;
				}
			<?php endif;

			$footer_top_border = get_theme_mod( 'footer_top_border' ); 
			if( !$footer_top_border == '' ) :
			?>
				.footer .footer-top {
					border-top-color:<?php echo esc_attr( $footer_top_border ); ?>;
					border-bottom-color:<?php echo esc_attr( $footer_top_border ); ?>;
				}
			<?php endif;

			$footer_bottom_background = get_theme_mod( 'footer_bottom_background' ); 
			if( !$footer_bottom_background == '' ) :
			?>
				.footer .footer-bottom {
					background:<?php echo esc_attr( $footer_bottom_background ); ?>;
				}
			<?php endif;

			$footer_text_color = get_theme_mod( 'footer_text_color' ); 
			if( !$footer_text_color == '' ) :
			?>
				input[type="email"], input[type="number"], input[type="password"], input[type="tel"], input[type="url"], input[type="text"], input[type="time"], input[type="week"], input[type="search"], input[type="month"], input[type="datetime"], input[type="date"], textarea, textarea.form-control, select, .select2-container .select2-choice, .form-control {
					color:<?php echo esc_attr( $footer_text_color ); ?>;
				}
			<?php endif;

			$input_border_color = get_theme_mod( 'input_border_color' ); 
			if( !$input_border_color == '' ) :
			?>
				input[type="email"], input[type="number"], input[type="password"], input[type="tel"], input[type="url"], input[type="text"], input[type="time"], input[type="week"], input[type="search"], input[type="month"], input[type="datetime"], input[type="date"], textarea, textarea.form-control, select, .select2-container .select2-choice, .form-control {
					border-color:<?php echo esc_attr( $input_border_color ); ?>;
				}
			<?php endif;

			$input_text_color = get_theme_mod( 'input_text_color' ); 
			if( !$input_text_color == '' ) :
			?>
				input[type="email"], input[type="number"], input[type="password"], input[type="tel"], input[type="url"], input[type="text"], input[type="time"], input[type="week"], input[type="search"], input[type="month"], input[type="datetime"], input[type="date"], textarea, textarea.form-control, select, .select2-container .select2-choice, .form-control {
					color:<?php echo esc_attr( $input_text_color ); ?>;
				}
			<?php endif;

			$input_placeholder_color = get_theme_mod( 'input_placeholder_color' ); 
			if( !$input_placeholder_color == '' ) :
			?>
				input::-webkit-input-placeholder {
					color:<?php echo esc_attr( $input_placeholder_color ); ?>;
				}
				
				input::-moz-placeholder {
					color:<?php echo esc_attr( $input_placeholder_color ); ?>;
				}
				
				input:-ms-input-placeholder {
					color:<?php echo esc_attr( $input_placeholder_color ); ?>;
				}
				
				input:-moz-placeholder {
					color:<?php echo esc_attr( $input_placeholder_color ); ?>;
				}
				
				textarea::-webkit-input-placeholder {
					color:<?php echo esc_attr( $input_placeholder_color ); ?>;
				}
				
				textarea::-moz-placeholder {
					color:<?php echo esc_attr( $input_placeholder_color ); ?>;
				}
				
				textarea:-ms-input-placeholder {
					color:<?php echo esc_attr( $input_placeholder_color ); ?>;
				}
				
				textarea:-moz-placeholder {
					color:<?php echo esc_attr( $input_placeholder_color ); ?>;
				}
				
				select::-webkit-input-placeholder {
					color:<?php echo esc_attr( $input_placeholder_color ); ?>;
				}
				
				select::-moz-placeholder {
					color:<?php echo esc_attr( $input_placeholder_color ); ?>;
				}
				
				select:-ms-input-placeholder {
					color:<?php echo esc_attr( $input_placeholder_color ); ?>;
				}
				
				select:-moz-placeholder {
					color:<?php echo esc_attr( $input_placeholder_color ); ?>;
				}
			<?php endif;

			$input_background = get_theme_mod( 'input_background' ); 
			if( !$input_background == '' ) :
			?>
				input[type="email"], input[type="number"], input[type="password"], input[type="tel"], input[type="url"], input[type="text"], input[type="time"], input[type="week"], input[type="search"], input[type="month"], input[type="datetime"], input[type="date"], textarea, textarea.form-control, select, .select2-container .select2-choice, .form-control {
					background:<?php echo esc_attr( $input_background ); ?>;
				}
			<?php endif;

			$button_background = get_theme_mod( 'button_background' ); 
			if( !$button_background == '' ) :
			?>
				button.alternative-button, input[type="submit"].alternative-button, button, input[type="submit"], .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button {
					background:<?php echo esc_attr( $button_background ); ?>;
				}
			<?php endif;

			$button_hover_background = get_theme_mod( 'button_hover_background' ); 
			if( !$button_hover_background == '' ) :
			?>
				button.alternative-button:hover, input[type="submit"].alternative-button:hover, button.alternative-button:focus, input[type="submit"].alternative-button:focus, button.alternative-button:active, input[type="submit"].alternative-button:active, button.alternative-button:active:hover, input[type="submit"].alternative-button:active:hover, button.alternative-button:active:focus, input[type="submit"].alternative-button:active:focus, button.alternative-button:active:visited, input[type="submit"].alternative-button:active:visited {
					background:<?php echo esc_attr( $button_hover_background ); ?>;
				}
			<?php endif;

			$button_color = get_theme_mod( 'button_color' ); 
			if( !$button_color == '' ) :
			?>
				button.alternative-button, input[type="submit"].alternative-button, button, input[type="submit"], .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button {
					color:<?php echo esc_attr( $button_color ); ?>;
				}
				
				button.alternative-button:hover, input[type="submit"].alternative-button:hover, button.alternative-button:focus, input[type="submit"].alternative-button:focus, button.alternative-button:active, input[type="submit"].alternative-button:active, button.alternative-button:active:hover, input[type="submit"].alternative-button:active:hover, button.alternative-button:active:focus, input[type="submit"].alternative-button:active:focus, button.alternative-button:active:visited, input[type="submit"].alternative-button:active:visited {
					color:<?php echo esc_attr( $button_color ); ?>;
				}
			<?php endif;

			$copyright_text_color = get_theme_mod( 'copyright_text_color' ); 
			if( !$copyright_text_color == '' ) :
			?>
				.footer .copyright {
					color:<?php echo esc_attr( $copyright_text_color ); ?>;
				}
			<?php endif;
	
			$custom_css = get_theme_mod( 'custom_css' );
			if( !$custom_css == '' ):
				echo esc_attr( $custom_css );
			endif;
			?>
		</style>
		
		<?php
		$custom_js = get_theme_mod( 'custom_js' );
		if( !$custom_js == '' ):
		?>
			<script type="text/javascript">
				<?php echo esc_js( $custom_js ); ?>
			</script>
		<?php endif; ?>
<?php
}
add_action( 'wp_head', 'translogistic_custom_style' );
/*-- THEME CUSTOM STYLE FUNCTION END --*/
/*------------- THEME OPTIONS END -------------*/