<?php
/*------------- THEME SETUP START -------------*/
add_action( 'after_setup_theme', 'translogistic_setup' );
function translogistic_setup(){
	load_theme_textdomain( 'translogistic', get_template_directory() . '/languages' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'custom-background' );
	add_theme_support( 'post-thumbnails' );
	
	if( function_exists( 'add_image_size' ) ) { 
		add_image_size( 'translogistic-blog-post-image', 800, 535, true );
		add_image_size( 'translogistic-latest-posts-widget-image', 360, 213, true );
		add_image_size( 'translogistic-page-banner', 1900, 200, true );
	}
	
	if( ! isset( $content_width ) ) {
		$content_width = 600;
	}
	
	if( is_singular() ) wp_enqueue_script( 'comment-reply' );
}
/*------------- THEME SETUP END -------------*/

/*------------- ENQUE TRANSLOGISTIC SCRIPT FILE AND STYLE FILE START -------------*/
function translogistic_scripts()
{
	wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), false, true );
	wp_enqueue_script( 'translogistic-fixed-sidebar-script', get_template_directory_uri() . '/assets/js/fixed-sidebar.js', array(), false, true  );
	wp_enqueue_script( 'translogistic-animate-script', get_template_directory_uri() . '/assets/js/animate.js', array(), false, true  );
	wp_enqueue_script( 'translogistic-script', get_template_directory_uri() . '/assets/js/translogistic.js', array(), false, true  );
	wp_enqueue_script( 'waypoints-script', get_template_directory_uri() . '/assets/js/waypoints.min.js', array(), false, true  );
	wp_enqueue_script( 'counterup-script', get_template_directory_uri() . '/assets/js/jquery.counterup.js', array(), false, true  );
	$header_fixed = get_theme_mod( 'header_fixed' );
	if( $header_fixed == '1' ) :
		wp_enqueue_script( 'translogistic-admin-bar-script', get_template_directory_uri() . '/assets/js/admin-bar.js', array(), false, true  );
		wp_enqueue_script( 'translogistic-fixed-header-script', get_template_directory_uri() . '/assets/js/fixed-header.js', array(), false, true  );
	endif;
	wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/assets/css/bootstrap.min.css'  );
	wp_enqueue_style( 'font-awesome-style', get_template_directory_uri() . '/assets/css/font-awesome.min.css'  );
	wp_enqueue_style( 'translogistic-style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'translogistic_scripts' );

function translogistic_load_custom_wp_admin() {
	wp_enqueue_style( 'translogistic-admin-style', get_template_directory_uri() . '/assets/css/admin.css'  );
	wp_enqueue_script( 'translogistic-admin-script', get_template_directory_uri() . '/assets/js/admin.js' );
}
add_action( 'admin_enqueue_scripts', 'translogistic_load_custom_wp_admin' );
/*------------- ENQUE TRANSLOGISTIC SCRIPT FILE AND STYLE FILE END -------------*/

/*------------- THEME HEAD META TAGS START -------------*/
function translogistic_meta_tags() {
    global $post;
 
    if(is_single()) {
        if( has_post_thumbnail( $post->ID ) ):
            $head_img_src = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'medium');
		else:
			$head_img_src = "";
			$head_img_src[0] = "";
		endif;
		
        if($excerpt = $post->post_excerpt) {
            $excerpt = strip_tags($post->post_excerpt);
            $excerpt = str_replace("", "'", $excerpt);
        } else {
            $excerpt = get_bloginfo('description');
        }
        ?>
 
    <meta property="og:title" content="<?php the_title(); ?>"/>
    <meta property="og:description" content="<?php echo esc_attr( $excerpt ); ?>"/>
    <meta property="og:url" content="<?php the_permalink(); ?>"/>
    <meta property="og:site_name" content="<?php echo get_bloginfo(); ?>"/>
    <meta property="og:image" content="<?php echo esc_url( $head_img_src[0] ); ?>"/>
 
<?php
    } else {
        return;
    }
}
add_action('wp_head', 'translogistic_meta_tags', 5);
/*------------- THEME HEAD META TAGS END -------------*/

/*------------- COMMENTS START -------------*/
function translogistic_move_comment_field_to_bottom( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;
}
add_filter( 'comment_form_fields', 'translogistic_move_comment_field_to_bottom' );

function translogistic_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) :
		$tag = 'div';
		$add_below = 'comment';
	else:
		$tag = 'li';
		$add_below = 'div-comment';
	endif;
?>
	<<?php echo esc_attr( $tag ) ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
	
	<?php if ( 'div' != $args['style'] ) : ?>
	
		<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
		
	<?php endif; ?>
	
	<div class="comment-author vcard">

		<div class="reply">
		
			<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		
		<?php edit_comment_link( esc_html__( 'Edit', 'translogistic' ), '  ', '' ); ?>
		
		</div>
	
		<?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
		
		<?php $allowed_html = array ( 'span' => array() ); printf( wp_kses( '<cite class="fn">%s</cite>', 'translogistic' ), get_comment_author_link() ); ?>

		<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
		
			<?php printf( esc_html__( '%1$s at %2$s', 'translogistic' ), get_comment_date(),  get_comment_time() ); ?></a>
			
		</div>
		
	</div>
	
	<?php if ( $comment->comment_approved == '0' ) : ?>
	
		<em class="comment-awaiting-moderation"><?php echo esc_html_e( 'Your comment is awaiting moderation.', 'translogistic' ); ?></em>
		
	<?php endif; ?>

	<?php comment_text(); ?>

	<?php if ( 'div' != $args['style'] ) : ?>
	
		</div>
	
	<?php endif; ?>
<?php
}
/*------------- COMMENTS END -------------*/

/*------------- BODY CLASS START -------------*/
function translogistic_class_names( $classes ) {
	$classes[] = 'translogistic-class';
	return $classes;
}
add_filter( 'body_class', 'translogistic_class_names' );
/*------------- BODY CLASS END -------------*/

/*------------- EXCERPT START -------------*/
function translogistic_new_excerpt_more( $more ) {
	return '...';
}
add_filter( 'excerpt_more', 'translogistic_new_excerpt_more' );

function translogistic_my_add_excerpts_to_pages() {
	add_post_type_support( 'page', 'excerpt' );
}
add_action( 'init', 'translogistic_my_add_excerpts_to_pages' );
/*------------- EXCERPT END -------------*/

/*------------- THEME SIDEBAR - WIDGET START -------------*/
if( !function_exists( 'translogistic_sidebars_init' ) ) {
	function translogistic_sidebars_init() {
		register_sidebar(array(
			'id' => 'general-sidebar',
			'name' => esc_html__( 'General Sidebar', 'translogistic' ),
			'before_widget' => '<div id="%1$s" class="general-sidebar-wrap widget-box %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<div class="widget-title"><h4>',
			'after_title' => '</h4></div>',
		));
		
		register_sidebar(array(
			'id' => 'shop-sidebar',
			'name' => esc_html__( 'Shop Sidebar', 'translogistic' ),
			'before_widget' => '<div id="%1$s" class="shop-sidebar-wrap widget-box %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<div class="widget-title"><h4>',
			'after_title' => '</h4></div>',
		));
		
		register_sidebar(array(
			'id' => 'user-sidebar',
			'name' => esc_html__( 'User Sidebar', 'translogistic' ),
			'before_widget' => '<div id="%1$s" class="user-sidebar-wrap widget-box %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<div class="widget-title"><h4>',
			'after_title' => '</h4></div>',
		));
	}
}
add_action( 'widgets_init', 'translogistic_sidebars_init' );
/*------------- THEME SIDEBAR - WIDGET END -------------*/

/*------------- SUB MENU CLASS START -------------*/
class translogistic_walker extends Walker_Nav_Menu {
	/**
	 * @see Walker::start_lvl()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of page. Used for padding.
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat( "\t", $depth );
		$output .= "\n$indent<ul role=\"menu\" class=\" dropdown-menu\">\n";
	}
	/**
	 * @see Walker::start_el()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. Used for padding.
	 * @param int $current_page Menu item ID.
	 * @param object $args
	 */
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$li_attributes = '';
		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		//Add class and attribute to LI element that contains a submenu UL.
		if ($args->has_children){
			$classes[] 		= 'dropdown';
			$li_attributes .= ' data-dropdown="dropdown"';
		}
		$classes[] = 'menu-item-' . $item->ID;
		//If we are on the current page, add the active class to that menu item.
		$classes[] = ($item->current) ? 'active' : '';

		//Make sure you still add all of the WordPress classes.
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';

		//Add attributes to link element.
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		$attributes .= ($args->has_children) ? ' class="dropdown-toggle disabled" data-toggle="dropdown"' : ''; 

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= ($args->has_children) ? '<i class="fa fa-angle-down mobile-up-caret"></i>' : '';
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
	/**
	 * Traverse elements to create list from elements.
	 *
	 * Display one element if the element doesn't have any children otherwise,
	 * display the element and its children. Will only traverse up to the max
	 * depth and no ignore elements under that depth.
	 *
	 * This method shouldn't be called directly, use the walk() method instead.
	 *
	 * @see Walker::start_el()
	 * @since 2.5.0
	 *
	 * @param object $element Data object
	 * @param array $children_elements List of elements to continue traversing.
	 * @param int $max_depth Max depth to traverse.
	 * @param int $depth Depth of current element.
	 * @param array $args
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return null Null on failure with no changes to parameters.
	 */
	public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
        if ( ! $element )
            return;
        $id_field = $this->db_fields['id'];
        // Display this element.
        if ( is_object( $args[0] ) )
           $args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] );
        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
}
/*------------- SUB MENU CLASS END -------------*/

/*------------- TRANSLOGISTIC MENUS START -------------*/
register_nav_menus( 
	array(
		'mainmenu' => esc_html__( 'Main Navigation', 'translogistic' )
	)
);

register_nav_menus( 
	array(
		'usermenu' => esc_html__( 'User Menu', 'translogistic' )
	)
);
/*------------- TRANSLOGISTIC MENUS END -------------*/

/*------------- PAGE LOADING START -------------*/
function translogistic_page_loading() {
	$translogistic_loader = get_theme_mod( 'translogistic_loader' );
	if( !$translogistic_loader == 'inactive' or $translogistic_loader == 'active' ) :
		echo '<div class="loader-wrapper"> <div class="sk-folding-cube"> <div class="sk-cube1 sk-cube"></div> <div class="sk-cube2 sk-cube"></div> <div class="sk-cube4 sk-cube"></div> <div class="sk-cube3 sk-cube"></div> </div> </div>';
	endif;
}
/*------------- PAGE LOADING END -------------*/

/*------------- RELATED POSTS START -------------*/
function translogistic_related_posts() {
	$hide_related_posts = get_theme_mod( 'hide_related_posts' );
	if( !$hide_related_posts == '1' ) :
		global $post;
		$tags = wp_get_post_tags( $post->ID );
		$post_related_limit = 4;
		if( !get_theme_mod( 'post_related_limit' ) == "" ) : $post_related_limit = get_theme_mod( 'post_related_limit' ); endif;
		
		if ($tags) {
		?>
			<div class="post-bottom-element">
				<div class="post-related">
					<h4><?php echo esc_html_e( 'Related Posts', 'translogistic' ); ?></h4>
					<div class="post-related-posts">
						<?php
						$tag_ids = array();
						foreach( $tags as $individual_tag ) $tag_ids[] = $individual_tag->term_id;
							$args = array(
								'tag__in' => $tag_ids,
								'post__not_in' => array($post->ID),
								'post_status' => 'publish',
								'posts_type' => 'post',
								'ignore_sticky_posts'    => true,
								'posts_per_page' => $post_related_limit
							);
				 
						$my_query = new wp_query( $args );
						while( $my_query->have_posts() ) {
							$my_query->the_post();
					?>
							<div class="item">
								<?php if ( has_post_thumbnail() ) { ?>
									<div class="image">
										<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail( 'translogistic-related-post-image' ); ?></a>
									</div>
								<?php } ?>
								<div class="desc">
									<div class="category"><?php the_category( ', ', '' ); ?></div>
									<div class="date"><?php the_time( get_option( 'date_format' ) ); ?></div>
								</div>
								<h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
							</div>
					<?php
						}
					?>
					</div>
				</div>
			</div>
		<?php } ?>
		<?php wp_reset_postdata(); ?>
		<?php function translogistic_related_posts_js( ) { ?>
			<script type='text/javascript'>
				jQuery(document).ready(function($){		
					$('.post-related-posts').owlCarousel({
						items:3,
						loop:true,
						autoplay:true,
						margin:15,
						nav:false,
						dots:false,
						autoplayHoverPause:true,
						responsive:{
							0:{
								items:1
							},
							768:{
								items:3
							}
						}
					});
				});
			</script>
		<?php
		}
		add_action( 'wp_footer', 'translogistic_related_posts_js', 41 );
		?>
	<?php
	endif;
}
/*------------- RELATED POSTS END -------------*/

/*------------- SIDEBAR START -------------*/
function translogistic_post_content_area_start() {
		global $post;
		
		if ( is_page() or is_single() ) {
			$values_layout_select = get_post_custom( $post->ID );
			$layout_select = isset( $values_layout_select['layout_select_meta_box_text'] ) ? strip_tags( esc_attr( $values_layout_select['layout_select_meta_box_text'][0] ) ) :'';
		}
		else {
			$layout_select = "";
		}
		
		$sidebar_position = get_theme_mod( 'sidebar_position' );
		
		if( $layout_select == 'fullwidth' ) {
			echo '<div class="col-lg-12 col-sm-12 col-xs-12 fullwidthsidebar">';
		}
		
		elseif( $layout_select == 'leftsidebar' ) {
			echo '<div class="col-lg-8 col-sm-8 col-xs-12 site-content-right site-content-left pull-right">';
		}
		
		elseif( $layout_select == 'rightsidebar' ) {
			echo '<div class="col-lg-8 col-sm-8 col-xs-12 site-content-left">';
		}
		
		elseif( $sidebar_position == 'nosidebar' ) {
			echo '<div class="col-lg-12 col-sm-12 col-xs-12 fullwidthsidebar">';
		}
		
		elseif( $sidebar_position == 'left' ) {
			echo '<div class="col-lg-8 col-sm-8 col-xs-12 site-content-right site-content-left pull-right">';
		}
		
		elseif( $sidebar_position == 'right' ) {
			echo '<div class="col-lg-8 col-sm-8 col-xs-12 site-content-left">';
		}
		
		else {
			echo '<div class="col-lg-8 col-sm-8 col-xs-12 site-content-left">';
		}
}

function translogistic_post_sidebar_start() {
		global $post;
		
		if ( is_page() or is_single() ) {
			$values_layout_select = get_post_custom( $post->ID );
			$layout_select = isset( $values_layout_select['layout_select_meta_box_text'] ) ? strip_tags( esc_attr( $values_layout_select['layout_select_meta_box_text'][0] ) ) :'';
		}
		else {
			$layout_select = "";
		}
		
		$sidebar_position = get_theme_mod( 'sidebar_position' );
		
		if( $layout_select == 'fullwidth' ) {
			echo '<div class="col-lg-12 col-sm-12 col-xs-12 hide fixedrightSidebar"><div class="theiaStickySidebar">';
		}
		
		elseif( $layout_select == 'leftsidebar' ) {
			echo '<div class="col-lg-4 col-sm-4 col-xs-12 site-content-right leftsidebar fixedrightSidebar"><div class="theiaStickySidebar">';
		}
		
		elseif( $layout_select == 'rightsidebar' ) {
			echo '<div class="col-lg-4 col-sm-4 col-xs-12 site-content-right rightsidebar fixedrightSidebar"><div class="theiaStickySidebar">';
		}
		
		elseif( $sidebar_position == 'nosidebar' ) {
			echo '<div class="col-lg-12 col-sm-12 col-xs-12 hide fixedrightSidebar"><div class="theiaStickySidebar">';
		}
		
		elseif( $sidebar_position == 'left' ) {
			echo '<div class="col-lg-4 col-sm-4 col-xs-12 site-content-right leftsidebar fixedrightSidebar"><div class="theiaStickySidebar">';
		}
		
		elseif( $sidebar_position == 'right' ) {
			echo '<div class="col-lg-4 col-sm-4 col-xs-12 site-content-right rightsidebar fixedrightSidebar"><div class="theiaStickySidebar">';
		}
		
		else {
			echo '<div class="col-lg-4 col-sm-4 col-xs-12 site-content-right fixedrightSidebar"><div class="theiaStickySidebar">';
		}
}

function translogistic_content_area_start() {
		$sidebar_position = get_theme_mod( 'sidebar_position' );
		
		if( $sidebar_position == 'nosidebar' ) {
			echo '<div class="col-lg-12 col-sm-12 col-xs-12 fullwidthsidebar">';
		}
		
		elseif( $sidebar_position == 'left' ) {
			echo '<div class="col-lg-8 col-sm-8 col-xs-12 site-content-right site-content-left pull-right">';
		}
		
		elseif( $sidebar_position == 'right' ) {
			echo '<div class="col-lg-8 col-sm-8 col-xs-12 site-content-left">';
		}
		
		else {
			echo '<div class="col-lg-8 col-sm-8 col-xs-12 site-content-left">';
		}
}

function translogistic_sidebar_start() {
		$sidebar_position = get_theme_mod( 'sidebar_position' );
		
		if( $sidebar_position == 'nosidebar' ) {
			echo '<div class="col-lg-12 col-sm-12 col-xs-12 hide fixedrightSidebar"><div class="theiaStickySidebar">';
		}
		
		elseif( $sidebar_position == 'left' ) {
			echo '<div class="col-lg-4 col-sm-4 col-xs-12 site-content-right leftsidebar fixedrightSidebar"><div class="theiaStickySidebar">';
		}
		
		elseif( $sidebar_position == 'right' ) {
			echo '<div class="col-lg-4 col-sm-4 col-xs-12 site-content-right rightsidebar fixedrightSidebar"><div class="theiaStickySidebar">';
		}
		
		else {
			echo '<div class="col-lg-4 col-sm-4 col-xs-12 site-content-right fixedrightSidebar"><div class="theiaStickySidebar">';
		}
}

function translogistic_content_area_end() {
	echo '</div>';
}

function translogistic_sidebar_end() {
	echo '</div></div>';
}
/*------------- SIDEBAR END -------------*/

/*------------- FOOTER TOP WIDGET START -------------*/
function translogistic_footer_top_widget() {
	function translogistic_footer_top_widget_content() {
		$footer_top_widget_page = get_theme_mod( 'footer_top_widget_page' );
		if( !$footer_top_widget_page == '0' and !empty( $footer_top_widget_page  ) ) :
		?>
			<div class="footer-top">
				<div class="footer_widget footer_top_widget_page">
					<div class="container">
						<div class="row">
							<?php
								$args_footer_page_content = array(
									'p' => $footer_top_widget_page,
									'ignore_sticky_posts' => true,
									'post_type' => 'page',
									'post_status' => 'publish'
								);
								$wp_query = new WP_Query( $args_footer_page_content );
								while ($wp_query->have_posts()) :
								$wp_query->the_post();
								$postid = get_the_ID();
							?>
								<?php the_content(); ?>
							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>
						</div>
					</div>
				</div>
			</div>
		<?php
		endif;
	}

	if ( is_page() or is_single() ) {
		global $post;
		$values_layout_select = get_post_custom( $post->ID );
		$hide_top_widget_footer = isset( $values_layout_select['hide_top_widget_footer'] ) ? strip_tags( esc_attr( $values_layout_select['hide_top_widget_footer'][0] ) ) :'';
	}	
	else {
		$hide_top_widget_footer = "";
	}
	
	if( !$hide_top_widget_footer == "on" ) :
		translogistic_footer_top_widget_content();
	endif;
	
}
/*------------- FOOTER TOP WIDGET END -------------*/

/*------------- FOOTER BOTTOM WIDGET START -------------*/
function translogistic_footer_bottom_widget() {
	
	function translogistic_footer_bottom_widget_content() {
		$footer_bottom_widget_page = get_theme_mod( 'footer_bottom_widget_page' );
		if( !$footer_bottom_widget_page == '0' and !empty( $footer_bottom_widget_page  ) ) :
		?>
			<div class="footer_widget footer_bottom_widget_page">
				<div class="container">
					<?php
						$args_footer_page_content = array(
							'p' => $footer_bottom_widget_page,
							'ignore_sticky_posts' => true,
							'post_type' => 'page',
							'post_status' => 'publish'
						);
						$wp_query = new WP_Query( $args_footer_page_content );
						while ( $wp_query->have_posts() ) :
						$wp_query->the_post();
						$postid = get_the_ID();
					?>
						<?php the_content(); ?>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
				</div>
			</div>
		<?php
		endif;
	}

	if ( is_page() or is_single() ) {
		global $post;
		$values_layout_select = get_post_custom( $post->ID );
		$bottom_footer_hide_widget_check = isset( $values_layout_select['bottom_footer_hide_widget'] ) ? strip_tags( esc_attr( $values_layout_select['bottom_footer_hide_widget'][0] ) ) :'';	
	}
	else {
		$bottom_footer_hide_widget_check = "";
	}
	
	if( !$bottom_footer_hide_widget_check == "on" ) :
		translogistic_footer_bottom_widget_content();
	endif;
	
}
/*------------- FOOTER BOTTOM WIDGET END -------------*/

/*------------- CONTACT FORM 7 START -------------*/
function translogistic_mycustom_wpcf7_form_elements( $form ) {
	$form = do_shortcode( $form );
	return $form;
}
add_filter( 'wpcf7_form_elements', 'translogistic_mycustom_wpcf7_form_elements' );
/*------------- CONTACT FORM 7 END -------------*/

/*------------- WOOCOMMERCE START -------------*/
function translogistic_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'translogistic_woocommerce_support' );

function translogistic_related_products_args( $args ) {
	$args['posts_per_page'] = 4; // 4 related products
	$args['columns'] = 4; // arranged in 2 columns
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'translogistic_related_products_args' );
/*------------- WOOCOMMERCE END -------------*/

/*------------- HEADER LOGO START -------------*/
function translogistic_site_logo() {
	$logo = get_theme_mod( 'translogistic_logo' );
	$logo_height = get_theme_mod( 'logo_height' ); if( !empty( $logo_height ) ) { $logo_height = 'height="' . $logo_height . '"'; }
	$logo_width = get_theme_mod( 'logo_width' ); if( !empty( $logo_width ) ) { $logo_width = 'width="' . $logo_width . '"'; }
	if( !$logo == ""  ) {
		echo '<div class="header-logo"><a href="' . home_url() . '" class="site-logo"><img alt="' . esc_html__( 'Logo', 'translogistic' ) . '" src="' . get_theme_mod( 'translogistic_logo' ) . '" ' . $logo_height . $logo_width . ' /></a></div>';
	} else {
		echo '<div class="header-logo"><a href="' . home_url() . '" class="site-logo"><img alt="' . esc_html__( 'Logo', 'translogistic' ) . '" src="' . esc_url( get_template_directory_uri() ) . '/assets/img/logo.png" /></a></div>';
	}
}
/*------------- HEADER LOGO END -------------*/

/*------------- HEADER CONTACT START -------------*/
function translogistic_header_contact() {
	$translogistic_hide_header_contact = get_theme_mod( 'translogistic_hide_header_contact' ); 
	$header_email = get_theme_mod( 'header_email' ); 
	$header_phone = get_theme_mod( 'header_phone' ); 
	if( $translogistic_hide_header_contact == '' ) :
	?>
		<div class="header-contact">
			<?php if( !empty( $header_email ) ): ?>
			<div class="header-contact-email">
				<h2><?php echo esc_html__( 'Write Us For Your Questions', 'translogistic' ); ?></h2>
				<div class="content">
					<i class="fa fa-envelope"></i>
					<span><?php echo esc_attr( get_theme_mod( 'header_email' ) ); ?></span>
				</div>
			</div>
			<?php endif; ?>
			<?php if( !empty( $header_phone ) ): ?>
			<div class="header-contact-phone">
				<i class="fa fa-phone"></i>
				<div class="content">
					<h2><?php echo esc_html__( 'Call Us For Your Questions', 'translogistic' ); ?></h2>
					<span><?php echo get_theme_mod( 'header_phone' ); ?></span>
				</div>
			</div>
			<?php endif; ?>
		</div>
	<?php
	endif;
}
/*------------- HEADER CONTACT END -------------*/

/*------------- HEADER SEARCH START -------------*/
function translogistic_header_search() {
	$translogistic_hide_header_search = get_theme_mod( 'translogistic_hide_header_search' ); 
	if( $translogistic_hide_header_search == '' ) :
	?>
		<div class="header-search">
			<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<input type="text" placeholder="<?php echo esc_html_e( 'SEARCH', 'translogistic' ); ?>" name="s" class="search">
				<button type="submit"><i class="fa fa-search"></i></button>
			</form>
		</div>
	<?php
	endif;
}
/*------------- HEADER SEARCH END -------------*/

/*------------- PAGE CONTENT BANNER START -------------*/
function translogistic_page_content_banner() {
	$blog_single_banner = get_theme_mod( 'blog_single_banner' ); 
	
	if ( is_404() ) {
		$image = get_template_directory_uri() . '/assets/img/404.jpg';
	} else {
		$image = get_theme_mod( 'blog_single_banner' );
	}
	
	if( !$blog_single_banner == '' ) :
	?>
		<div class="page-content-banner" style="background-image:url('<?php echo esc_attr( $image ); ?>');"></div>
	<?php
	endif;
}
/*------------- PAGE CONTENT BANNER END -------------*/

/*------------- SOCIAL MEDIA TEMPLATE FUNCTION START -------------*/
function translogistic_header_social_media_links() {
	$translogistic_hide_header_social_media = get_theme_mod( 'translogistic_hide_header_social_media' ); 
	if( $translogistic_hide_header_social_media == '' ) :
		if( !get_theme_mod( 'translogistic_facebook' ) == "" or !get_theme_mod( 'translogistic_googleplus' ) == "" or !get_theme_mod( 'translogistic_instagram' ) == "" or !get_theme_mod( 'translogistic_linkedin' ) == "" or !get_theme_mod( 'translogistic_vine' ) == "" or !get_theme_mod( 'translogistic_twitter' ) == "" or !get_theme_mod( 'translogistic_youtube' ) == "" or !get_theme_mod( 'translogistic_pinterest' ) == "" or !get_theme_mod( 'translogistic_rss' ) == "" ) { ?>
			<div class="header-social-media">
				<ul>
					<?php if( !get_theme_mod( 'translogistic_facebook' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_facebook' ); ?>" class="facebook" title="<?php echo esc_html__( 'Facebook', 'translogistic' ); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_googleplus' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_googleplus' ); ?>" class="googleplus" title="<?php echo esc_html__( 'Google+', 'translogistic' ); ?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_instagram' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_instagram' ); ?>" class="instagram" title="<?php echo esc_html__( 'Instagram', 'translogistic' ); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_linkedin' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_linkedin' ); ?>" class="linkedin" title="<?php echo esc_html__( 'Linkedin', 'translogistic' ); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_vine' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_vine' ); ?>" class="vine" title="<?php echo esc_html__( 'Vine', 'translogistic' ); ?>" target="_blank"><i class="fa fa-vine"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_twitter' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_twitter' ); ?>" class="twitter" title="<?php echo esc_html__( 'Twitter', 'translogistic' ); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_youtube' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_youtube' ); ?>" class="youtube" title="<?php echo esc_html__( 'YouTube', 'translogistic' ); ?>" target="_blank"><i class="fa fa-youtube"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_pinterest' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_pinterest' ); ?>" class="pinterest" title="<?php echo esc_html__( 'Pinterest', 'translogistic' ); ?>" target="_blank"><i class="fa fa-pinterest"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_behance' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_behance' ); ?>" class="behance" title="<?php echo esc_html__( 'Behance', 'translogistic' ); ?>" target="_blank"><i class="fa fa-behance"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_deviantart' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_deviantart' ); ?>" class="deviantart" title="<?php echo esc_html__( 'Deviantart', 'translogistic' ); ?>" target="_blank"><i class="fa fa-deviantart"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_digg' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_digg' ); ?>" class="digg" title="<?php echo esc_html__( 'Digg', 'translogistic' ); ?>" target="_blank"><i class="fa fa-digg"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_dribbble' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_dribbble' ); ?>" class="dribbble" title="<?php echo esc_html__( 'Dribbble', 'translogistic' ); ?>" target="_blank"><i class="fa fa-dribbble"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_flickr' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_flickr' ); ?>" class="flickr" title="<?php echo esc_html__( 'Flickr', 'translogistic' ); ?>" target="_blank"><i class="fa fa-flickr"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_github' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_github' ); ?>" class="github" title="<?php echo esc_html__( 'GitHub', 'translogistic' ); ?>" target="_blank"><i class="fa fa-github"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_lastfm' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_lastfm' ); ?>" class="lastfm" title="<?php echo esc_html__( 'Last.fm', 'translogistic' ); ?>" target="_blank"><i class="fa fa-lastfm"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_reddit' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_reddit' ); ?>" class="reddit" title="<?php echo esc_html__( 'Reddit', 'translogistic' ); ?>" target="_blank"><i class="fa fa-reddit"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_soundcloud' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_soundcloud' ); ?>" class="soundcloud" title="<?php echo esc_html__( 'SoundCloud', 'translogistic' ); ?>" target="_blank"><i class="fa fa-soundcloud"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_tumblr' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_tumblr' ); ?>" class="tumblr" title="<?php echo esc_html__( 'Tumblr', 'translogistic' ); ?>" target="_blank"><i class="fa fa-tumblr"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_vimeo' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_vimeo' ); ?>" class="vimeo" title="<?php echo esc_html__( 'Vimeo', 'translogistic' ); ?>" target="_blank"><i class="fa fa-vimeo"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_vk' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_vk' ); ?>" class="vk" title="<?php echo esc_html__( 'VK', 'translogistic' ); ?>" target="_blank"><i class="fa fa-vk"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_medium' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_medium' ); ?>" class="medium" title="<?php echo esc_html__( 'Medium', 'translogistic' ); ?>" target="_blank"><i class="fa fa-medium"></i></a></li>
					<?php } ?>

					<?php if( !get_theme_mod( 'translogistic_rss' ) == ""  ) { ?>
					<li><a href="<?php echo get_theme_mod( 'translogistic_rss' ); ?>" class="rss" title="<?php echo esc_html__( 'RSS', 'translogistic' ); ?>" target="_blank"><i class="fa fa-rss"></i></a></li>
					<?php } ?>
				</ul>
			</div>
		<?php }
	endif;
}
/*------------- SOCIAL MEDIA TEMPLATE FUNCTION END -------------*/

/*------------- POST SOCIAL SHARE START -------------*/
function translogistic_general_post_social_share() {
	$post_share_hide = get_theme_mod( 'post_share_hide' );
	$social_share_facebook = get_theme_mod( 'social_share_facebook' );
	$social_share_twitter = get_theme_mod( 'social_share_twitter' );
	$social_share_googleplus = get_theme_mod( 'social_share_googleplus' );
	$social_share_linkedin = get_theme_mod( 'social_share_linkedin' );
	$social_share_pinterest = get_theme_mod( 'social_share_pinterest' );
	$social_share_reddit = get_theme_mod( 'social_share_reddit' );
	$social_share_delicious = get_theme_mod( 'social_share_delicious' );
	$social_share_stumbleupon = get_theme_mod( 'social_share_stumbleupon' );
	$social_share_tumblr = get_theme_mod( 'social_share_tumblr' );
	$social_share_link_title = esc_html__( 'Share to', 'translogistic' );
	$hide_general_post_share = get_theme_mod( 'hide_general_post_share' );
	$share_post_id = get_the_ID();
	$share_featured_post_image[0] = "";
	
	if( has_post_thumbnail( $share_post_id ) ) :
		$share_featured_post_image = wp_get_attachment_image_src( get_post_thumbnail_id( $share_post_id ), 'medium' );
	endif;
	
	
	$facebook = "";
	$twitter = "";
	$googleplus = "";
	$linkedin = "";
	$pinterest = "";	
	$reddit = "";
	$delicious = "";
	$stumbleupon = "";
	$tumblr = "";
	
	if( !$hide_general_post_share == '1' ) : 
		if( !$social_share_facebook == '1' ) : $facebook = '<li><a class="share-facebook"  href="https://www.facebook.com/sharer/sharer.php?u=' . get_the_permalink() . '&t=' . get_the_title() . '" title="' . esc_attr( $social_share_link_title ) . esc_html__( 'Facebook', 'translogistic' ) . '" target="_blank"><i class="fa fa-facebook"></i></a></li>'; else: endif;
		if( !$social_share_twitter == '1' ) : $twitter = '<li><a class="share-twitter"  href="https://twitter.com/intent/tweet?url=' . get_the_permalink() . '&text=' . get_the_title(). '" title="' . esc_attr( $social_share_link_title ) . esc_html__( 'Twitter', 'translogistic' ) . '" target="_blank"><i class="fa fa-twitter"></i></a></li>'; else: endif;
		if( !$social_share_googleplus == '1' ) : $googleplus = '<li><a class="share-googleplus"  href="https://plus.google.com/share?url=' . get_the_permalink() . '" title="' . esc_attr( $social_share_link_title ) . esc_html__( 'Google+', 'translogistic' ) . '" target="_blank"><i class="fa fa-google-plus"></i></a></li>'; else: endif;
		if( !$social_share_linkedin == '1' ) : $linkedin = '<li><a class="share-linkedin"  href="https://www.linkedin.com/shareArticle?mini=true&amp;url=' . get_the_permalink() . '&title=' . get_the_title() . '" title="' . esc_attr( $social_share_link_title ) . esc_html__( 'Linkedin', 'translogistic' ) . '" target="_blank"><i class="fa fa-linkedin"></i></a></li>'; else: endif;
		if( !$social_share_pinterest == '1' ) : $pinterest = '<li><a class="share-pinterest"  href="https://pinterest.com/pin/create/button/?url=' . get_the_permalink() . '&description=' . get_the_title() . '" title="' . esc_attr( $social_share_link_title ) . esc_html__( 'Pinterest', 'translogistic' ) . '" target="_blank"><i class="fa fa-pinterest-p"></i></a></li>'; else: endif;
		if( !$social_share_reddit == '1' ) : $reddit = '<li><a class="share-reddit"  href="http://reddit.com/submit?url=' . get_the_permalink() . '&title=' . get_the_title() . '" title="' . esc_attr( $social_share_link_title ) . esc_html__( 'Reddit', 'translogistic' ) . '" target="_blank"><i class="fa fa-reddit"></i></a></li>'; else: endif;
		if( !$social_share_delicious == '1' ) : $delicious = '<li><a class="share-delicious"  href="http://del.icio.us/post?url=' . get_the_permalink() . '" title="' . esc_attr( $social_share_link_title ) . esc_html__( 'Delicious', 'translogistic' ) . '" target="_blank"><i class="fa fa-delicious"></i></a></li>'; else: endif;
		if( !$social_share_stumbleupon == '1' ) : $stumbleupon = '<li><a class="share-stumbleupon"  href="http://www.stumbleupon.com/submit?url=' . get_the_permalink() . '&title=' . get_the_title() . '" title="' . esc_attr( $social_share_link_title ) . esc_html__( 'Stumbleupon', 'translogistic' ) . '" target="_blank"><i class="fa fa-stumbleupon"></i></a></li>'; else: endif;
		if( !$social_share_tumblr == '1' ) : $tumblr = '<li><a class="share-tumblr"  href="http://www.tumblr.com/share/link?url=' . get_the_permalink() . '" title="' . esc_attr( $social_share_link_title ) . esc_html__( 'Tumblr', 'translogistic' ) . '" target="_blank"><i class="fa fa-tumblr"></i></a></li>'; else: endif;
	endif;
	
	$before = '<div class="post-share"><ul>';
	$after = '</ul></div>';
	
	$output = $before . $facebook . $twitter . $googleplus . $linkedin . $pinterest . $reddit . $delicious . $stumbleupon . $tumblr . $after;
	echo balanceTags( stripslashes( addslashes( $output ) ) );
}
/*------------- POST SOCIAL SHARE END -------------*/

/*------------- WRAPPER BEFORE START -------------*/
function translogistic_wrapper_before() {
	$layout_style = get_theme_mod( 'layout_style' );
	?>
		<div class="translogistic-wrapper<?php if( $layout_style == "boxed"  ) : ?> wrapper-boxed<?php endif; ?>" id="general-wrapper">
	<?php
}
/*------------- WRAPPER BEFORE END -------------*/

/*------------- WRAPPER BEFORE START -------------*/
function translogistic_go_top() {
	$translogistic_go_top = get_theme_mod( 'translogistic_go_top' );
	if( $translogistic_go_top == ""  ) {
	?>
		<div class="go-top-icon"><i class="fa fa-sort-asc"></i></div>
	<?php
	}
}
/*------------- WRAPPER BEFORE END -------------*/

/*------------- WRAPPER AFTER START -------------*/
function translogistic_wrapper_after() {
	?>
		</div>
	<?php
}
/*------------- WRAPPER AFTER END -------------*/

/*------------- SITE CONTENT START -------------*/
function translogistic_site_content_start() {
	?>
		<div class="site-content<?php $header_fixed = get_theme_mod( 'header_fixed' ); if( $header_fixed == '1' ) : echo " fixed-header-body"; endif; ?>">
	<?php
}

function translogistic_site_content_end() {
	?>			
		</div>
	<?php
}
/*------------- SITE CONTENT END -------------*/

/*------------- SITE SUB CONTENT START -------------*/
function translogistic_site_sub_content_start() {
	?>
		<div class="site-sub-content clearfix">
	<?php
}

function translogistic_site_sub_content_end() {
	?>			
		</div>
	<?php
}
/*------------- SITE SUB CONTENT END -------------*/

/*------------- WIDGET CONTENT BEFORE START -------------*/
function translogistic_widget_content_before() {
	?>
		<div class="widget-content">
	<?php
}
/*------------- WIDGET CONTENT BEFORE END -------------*/

/*------------- WIDGET CONTENT AFTER START -------------*/
function translogistic_widget_content_after() {
	?>
		</div>
	<?php
}
/*------------- WIDGET CONTENT AFTER END -------------*/

/*------------- SITE PAGE CONTENT BEFORE START -------------*/
function translogistic_site_page_content_before() {
	?>
		<div class="site-page-content">
	<?php
}
/*------------- SITE PAGE CONTENT BEFORE END -------------*/

/*------------- SITE PAGE CONTENT AFTER START -------------*/
function translogistic_site_page_content_after() {
	?>
		</div>
	<?php
}
/*------------- SITE PAGE CONTENT AFTER END -------------*/

/*------------- CONTAINER BEFORE START -------------*/
function translogistic_container_before() {
	?>
		<div class="container">
	<?php
}
/*------------- CONTAINER BEFORE END -------------*/

/*------------- CONTAINER AFTER START -------------*/
function translogistic_container_after() {
	?>
		</div>
	<?php
}
/*------------- CONTAINER AFTER END -------------*/

/*------------- ROW BEFORE START -------------*/
function translogistic_row_before() {
	?>
		<div class="row">
	<?php
}
/*------------- ROW BEFORE END -------------*/

/*------------- ROW AFTER START -------------*/
function translogistic_row_after() {
	?>
		</div>
	<?php
}
/*------------- ROW AFTER END -------------*/