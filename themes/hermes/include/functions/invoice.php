<?php

function invoice_init() {
    $labels = array(
        'name'                  => 'Invoices',
        'singular_name'         => 'Invoice',
        'menu_name'             => 'Invoices',
        'name_admin_bar'        => 'Invoice',
        'add_new'               => 'Add invoice',
        'add_new_item'          => 'Add new invoice',
        'new_item'              => 'New invoice',
        'edit_item'             => 'Update invoice',
        'all_items'             => 'All invoices',
        'search_items'          => 'Find invoice',
        'not_found'             => 'Invoice not found',
        'not_found_in_trash'    => 'Invoice is not in trash',
    );

    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'publicly_queryable'    => true,
        'show_ui'               => true,
        'has_archive'           => false,
        'menu_icon'             => 'dashicons-portfolio',
        'supports'              => array('title', 'autor'),
    );

    register_post_type( 'invoice' , $args);
}
add_action( 'init', 'invoice_init' );


function invoice_custom_column($defaults) {
    
    $columns = array(
        'cb'                    => '<input type="checkbox" />',
        'title'           		=> 'Title',        
        'username'         		=> 'Username',
        'first_name'      		=> 'First name',
        'last_name'          	=> 'Last name',
        'user_email'			=> 'Email',
        'date'          		=> 'Дата создания <span class="dashicons dashicons-calendar"></span>',
    );

    return $columns;
}
add_filter( 'manage_invoice_posts_columns', 'invoice_custom_column' );

function invoice_get_column_value($column, $post_id) {
   $post_info = get_post($post_id);
   $user_info = get_userdata($post_info->post_author);

    if ( $column == 'username') {
    	if (isset($user_info->user_login) && !empty($user_info->user_login)) {
    		echo $user_info->user_login;
    	} else {
    		echo 'not set';
    	}
    }       
        
    if ( $column == 'first_name') {
    	if (isset($user_info->first_name) && !empty($user_info->first_name)) {
    		echo $user_info->first_name;
    	} else {
    		echo 'not set';
    	}
    }
    if ( $column == 'last_name') {
    	if (isset($user_info->last_name) && !empty($user_info->last_name)) {
    		echo $user_info->last_name;
    	} else {
    		echo 'not set';
    	}
    }
     if ( $column == 'user_email') {
    	if (isset($user_info->user_email) && !empty($user_info->user_email)) {
    		echo $user_info->user_email;
    	} else {
    		echo 'not set';
    	}
    }    
}
add_action( 'manage_invoice_posts_custom_column', 'invoice_get_column_value', 10, 2 );

add_action('add_meta_boxes', 'invoice_extra_fields', 1);

//Добавление произвольных полей к товарам и фасадам
function invoice_extra_fields() {
    add_meta_box( 'extra_fields', 'Info invoice', 'fields_invoice', 'invoice', 'normal', 'high'  );
}

function fields_invoice($post) {
     $user_info = get_userdata($post->post_author);
     $user_meta = get_user_meta($post->post_author);
    ?>

    <p>
        <span>Username:</span>
        <span><?php echo $user_info->user_login; ?></span>
    </p>
    <p>
        <span>First name:</span>
         <span><?php echo $user_info->first_name; ?> </span>
    </p>
    <p>
        <span>Last name:</span>
         <span><?php echo $user_info->last_name; ?></span>
    </p>
    <p>
        <span>Email:</span>
        <span><?php echo $user_info->user_email; ?></span>
    </p>
    <p>
        <span>Phone:</span>
        <span><?php echo $user_meta['phone'][0]; ?></span>
    </p>
    <p>
        <span>Link invoice:</span>
        <a href="<?php echo get_post_meta($post->ID, 'invoice_link', 1); ?>" target="blank">Link</a>
    </p>

    <?php
}