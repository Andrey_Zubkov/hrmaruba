<?php

function wish_list_init() {
    $labels = array(
        'name'                  => 'Wish List',
        'singular_name'         => 'Wish List',
        'menu_name'             => 'Wish List',
        'name_admin_bar'        => 'Wish List',
        'add_new'               => 'Add Wish List',
        'add_new_item'          => 'Add new Wish List',
        'new_item'              => 'New Wish List',
        'edit_item'             => 'Update Wish List',
        'all_items'             => 'All Wish Lists',
        'search_items'          => 'Find Wish List',
        'not_found'             => 'Wish List not found',
        'not_found_in_trash'    => 'Wish List is not in trash',
    );

    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'publicly_queryable'    => true,
        'show_ui'               => true,
        'has_archive'           => false,
        'menu_icon'             => 'dashicons-tickets-alt',
        'supports'              => array('title', 'autor'),
    );

    register_post_type( 'wish_list' , $args);
}
add_action( 'init', 'wish_list_init' );


function wish_list_custom_column($defaults) {
    
    $columns = array(
        'cb'                    => '<input type="checkbox" />',
        'img'                   => '<span class="dashicons dashicons-format-image"></span>',  
        'title'           		=> 'Title',        
        'username'         		=> 'Username',
        'first_name'      		=> 'First name',
        'last_name'          	=> 'Last name',
        'user_email'			=> 'Email',
        'date'          		=> 'Дата создания <span class="dashicons dashicons-calendar"></span>',
    );

    return $columns;
}
add_filter( 'manage_wish_list_posts_columns', 'wish_list_custom_column' );

function wish_list_get_column_value($column, $post_id) {
   $post_info = get_post($post_id);
   $user_info = get_userdata($post_info->post_author);

    if ( $column == 'username') {
    	if (isset($user_info->user_login) && !empty($user_info->user_login)) {
    		echo $user_info->user_login;
    	} else {
    		echo 'not set';
    	}
    }       
        
    if ( $column == 'first_name') {
    	if (isset($user_info->first_name) && !empty($user_info->first_name)) {
    		echo $user_info->first_name;
    	} else {
    		echo 'not set';
    	}
    }
    if ( $column == 'last_name') {
    	if (isset($user_info->last_name) && !empty($user_info->last_name)) {
    		echo $user_info->last_name;
    	} else {
    		echo 'not set';
    	}
    }
     if ( $column == 'user_email') {
    	if (isset($user_info->user_email) && !empty($user_info->user_email)) {
    		echo $user_info->user_email;
    	} else {
    		echo 'not set';
    	}
    }
    if ( $column == 'img') {
        
        echo '<img style="height: 60px; width:60px;" src="'.get_post_meta($post_id, 'image_product', 1).'" alt="" />';;
        
    }    
}
add_action( 'manage_wish_list_posts_custom_column', 'wish_list_get_column_value', 10, 2 );

add_action('add_meta_boxes', 'wish_list_extra_fields', 1);

//Добавление произвольных полей к товарам и фасадам
function wish_list_extra_fields() {
    add_meta_box( 'extra_fields', 'Info Wish List Product', 'fields_wish_list', 'wish_list', 'normal', 'high'  );
}

function fields_wish_list($post) {
     $user_info = get_userdata($post->post_author);
     $user_meta = get_user_meta($post->post_author);
    ?>

    <p>
        <span>Username:</span>
        <span><?php echo $user_info->user_login; ?></span>
    </p>
    <p>
        <span>First name:</span>
         <span><?php echo $user_info->first_name; ?> </span>
    </p>
    <p>
        <span>Last name:</span>
         <span><?php echo $user_info->last_name; ?></span>
    </p>
    <p>
        <span>Email:</span>
        <span><?php echo $user_info->user_email; ?></span>
    </p>
    <p>
        <span>Phone:</span>
        <span><?php echo $user_meta['phone'][0]; ?></span>
    </p>
    <p>
        <span>Image :</span>
        <img src="<?php echo get_post_meta($post->ID, 'image_product', 1); ?>" style="width: 60px; height: 60px;">
    </p>
    <p>
        <span>Product name :</span>
        <span><?php echo get_post_meta($post->ID, 'product_name', 1); ?></span>

    </p>
    <p>
        <span>Link webshop:</span>
        <a href="<?php echo get_post_meta($post->ID, 'link_webshop', 1); ?>" target="blank">Link</a>
    </p>

    <?php
}