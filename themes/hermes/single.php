<?php
/**
	* The template for displaying single
*/
get_header(); ?>

	<?php translogistic_page_content_banner(); ?>

	<?php translogistic_site_sub_content_start(); ?>
		<?php translogistic_container_before(); ?>
			<?php translogistic_row_before(); ?>
				<?php translogistic_post_content_area_start(); ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'include/formats/content', get_post_format() ); ?>
					<?php endwhile; ?>
					<?php while ( have_posts() ) : the_post(); 
						if ( comments_open() || get_comments_number() ) {
							comments_template();
						}
					endwhile; ?>
				<?php translogistic_content_area_end(); ?>
				<?php translogistic_post_sidebar_start(); ?>
					<?php if ( is_active_sidebar( 'general-sidebar' ) ) : ?>
						<div class="sidebar-general sidebar">
							<?php dynamic_sidebar( 'general-sidebar' ); ?>
						</div>
					<?php endif; ?>
				<?php translogistic_sidebar_end(); ?>
			<?php translogistic_row_after(); ?>
		<?php translogistic_container_after(); ?>
	<?php translogistic_site_sub_content_end(); ?>

<?php get_footer();