jQuery(document).ready(function($) {
	'use strict';
	
	$(function(){
		var controldatecalendar = $(".datecalendar").is("input");
		if( controldatecalendar == "" ) {
		} else {
			 $( '.datecalendar' ).datepicker({
				dateFormat: 'dd.m.yy',
				changeMonth: true,
				changeYear: true,
				onClose: function( selectedDate ){
					$( '#uep-event-end-date' ).datepicker( 'option', 'minDate', selectedDate );
				}
			});
		}
	});
	
});

