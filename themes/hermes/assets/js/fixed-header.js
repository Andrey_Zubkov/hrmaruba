(function($){
	'use strict';
	
	$(window).scroll(function(){
		if ($(window).scrollTop() >= 150) {
			$('.header-wrapper').addClass('fixed-header-class');
		}
		else {
			$('.header-wrapper').removeClass('fixed-header-class');
		}
	});

} )( jQuery );