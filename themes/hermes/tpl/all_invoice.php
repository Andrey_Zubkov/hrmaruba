<?php /* Template Name: Invoices */

get_header(); ?>
	
	<?php translogistic_page_content_banner(); ?>
			
	<?php translogistic_site_sub_content_start(); ?>
		<?php translogistic_container_before(); ?>
			<?php translogistic_row_before(); ?>
				<?php translogistic_content_area_start(); ?>
					
				<?php translogistic_content_area_end(); ?>
				 <?php if ( is_user_logged_in() ): ?>
				 	<?php
				 		$user_id = get_current_user_id();
				 		$user_info = get_userdata($user_id);
				 		$args = array(
						    'author'        =>  $user_id, // I could also use $user_ID, right?
						    'orderby'       =>  'post_date',
						    'order'         =>  'ASC',
						    'post_type'		=> 'invoice', 
						    );

						// get his posts 'ASC'
						$user_invoices = get_posts( $args );
						
				 	?>
	<table class="wp-list-table widefat fixed striped posts">
		<thead>
			<tr>
				<th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
					<a href="http://localhost/hrmaruba/wp-admin/edit.php?post_type=invoice&amp;orderby=title&amp;order=asc">
						<span>Title</span>
						<span class="sorting-indicator"></span>
					</a>
				</th>
				<th scope="col" id="username" class="manage-column column-username">Username</th>
				<th scope="col" id="first_name" class="manage-column column-first_name">First name</th>
				<th scope="col" id="last_name" class="manage-column column-last_name">Last name</th>
				<th scope="col" id="user_email" class="manage-column column-user_email">Email</th>
				<th scope="col" id="user_email" class="manage-column column-user_email">Link invoice</th>
				<th scope="col" id="date" class="manage-column column-date sortable asc">
					<a href="http://localhost/hrmaruba/wp-admin/edit.php?post_type=invoice&amp;orderby=date&amp;order=desc">
						<span>Creating Date
							<span class="dashicons dashicons-calendar"></span>
						</span>
						<span class="sorting-indicator"></span>
					</a>
				</th>	
			</tr>
		</thead>

		<tbody id="the-list">
		<?php foreach ($user_invoices as $key => $item):?>
			<tr id="post-1606" class="iedit author-self level-0 post-1606 type-invoice status-publish hentry">
				<td class="username column-username" data-colname="Username"><?php echo $item->post_title; ?></td>
				<td class="username column-username" data-colname="Username"><?php echo $user_info->user_login; ?></td>
				<td class="first_name column-first_name" data-colname="First name"><?php echo $user_info->first_name; ?></td>
				<td class="last_name column-last_name" data-colname="Last name"><?php echo $user_info->last_name;?></td>
				<td class="user_email column-user_email" data-colname="Email"><?php echo $user_info->user_email;?></td>
				<td class="user_email column-user_email" data-colname="Email"> <a href="<?php echo get_post_meta($item->ID, 'invoice_link', 1); ?>" target="blank" >Link invoice</a></td>
				<td class="date column-date" data-colname="Дата создания">Published<br>
					<?php echo $item->post_date; ?>
				</td>
			</tr>
		<?php endforeach;?>

		</tbody>
	</table>
				 <?php endif; ?>

                <?php translogistic_post_sidebar_start(); ?>
                    <?php if ( is_active_sidebar( 'general-sidebar' ) ) : ?>
                        <div class="sidebar-general sidebar">
                            <?php dynamic_sidebar( 'general-sidebar' ); ?>
                        </div>
                    <?php endif; ?>
                <?php translogistic_sidebar_end(); ?>
    
			<?php translogistic_row_after(); ?>
			
		<?php translogistic_container_after(); ?>
	<?php translogistic_site_sub_content_end(); ?>

<?php get_footer();
