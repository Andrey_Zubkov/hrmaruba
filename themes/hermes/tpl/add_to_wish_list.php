<?php /* Template Name:  Add to Wish List */

get_header(); ?>
	
	<?php translogistic_page_content_banner(); ?>
			
	<?php translogistic_site_sub_content_start(); ?>
		<?php translogistic_container_before(); ?>
			<?php translogistic_row_before(); ?>
				<?php translogistic_content_area_start(); ?>
					
				<?php translogistic_content_area_end(); ?>
				 <?php if ( is_user_logged_in() ): ?>
				 		<?php				 			
				 			if(isset($_POST['wish_list_us']) && !empty($_POST['wish_list_us']['title'])) {
				 				$success_wish_list = false;
	 		                    if($_FILES['wish_list_us']) {
			                      $upload_dir = __DIR__.'/../../../uploads/wishlist/';
			                      $uploadfile = $upload_dir . basename($_FILES['wish_list_us']['name']['file']);               
			                      if(move_uploaded_file($_FILES['wish_list_us']['tmp_name']['file'], $uploadfile)){
			                         $user_id = get_current_user_id();
			                         $wp_upload_dir = wp_upload_dir();
									    $post_data = array(
									        'post_title'    => $_POST['wish_list_us']['title'],
									        'post_status'   => 'publish',
									        'post_author'   => $user_id,
									        'post_type'     => 'wish_list',
									    );
									    $wishlist_id = wp_insert_post( $post_data );
									    update_post_meta($wishlist_id,'image_product', $wp_upload_dir['baseurl']."/wishlist/" . basename($_FILES['wish_list_us']['name']['file']));
									    update_post_meta($wishlist_id, 'product_name', $_POST['wish_list_us']['product_name']);
									    update_post_meta($wishlist_id, 'link_webshop', $_POST['wish_list_us']['link_webshop']);
									    $success_wish_list = true;
			                      }
			                    }
				 			}
				 		
				 		?>
					<?php endif;?>
				    <div class="wrap">
				        <h2>Your Wish List</h2>
				        <form method="POST" enctype="multipart/form-data">
				            <div id="extra_fields" class="postbox ">
				            <?php 
				                global $success;
				                if(isset($success_wish_list) && $success_wish_list):                    
				            ?>
				                    <div class="updated notice">
				                        <p>Wish list was send successfuly!</p>
				                    </div>
				            <?php endif;?>
				            <?php
				                if(isset($success_wish_list) && !$success_wish_list):                    
				            ?>
				                    <div class="error notice">
				                        <p>There has been an error. Send wish list failure.</p>
				                    </div>
				            <?php endif;?>
				                <div class="inside">
				                    <p>				                        
				                        <p> 
				                        	<span>Title</span>
				                        	<input type="text" name="wish_list_us[title]">
				                        </p>
				                        <p> 
				                        	<span>Product name</span>
				                        	<input type="text" name="wish_list_us[product_name]">
				                        </p>
				                        <p> 
				                        	<span>Link webshop</span>
				                        	<input type="text" name="wish_list_us[link_webshop]">
				                        </p>				                        
				                        <p> 
				                        	<span>Image</span>
				                        	<input type="file" name="wish_list_us[file]">
				                        </p>
				                    </p>            
				                    <button class="button button-primary" name="wish_list_us[send]">Send</button>
				                </div>
				            </div>            
				        </form>
				    </div>				    
			<?php translogistic_row_after(); ?>
			
		<?php translogistic_container_after(); ?>
	<?php translogistic_site_sub_content_end(); ?>

<?php get_footer();
