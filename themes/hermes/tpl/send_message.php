<?php /* Template Name: Send message */

get_header(); ?>
	
	<?php translogistic_page_content_banner(); ?>
			
	<?php translogistic_site_sub_content_start(); ?>
		<?php translogistic_container_before(); ?>
			<?php translogistic_row_before(); ?>
				<?php translogistic_content_area_start(); ?>
					
				<?php translogistic_content_area_end(); ?>
				 <?php if ( is_user_logged_in() ): ?>
				 	<?php

						if (isset($_POST['mes']) && !empty($_POST['mes']['message'])) {
						    $admin_email = get_option('admin_email');
						    $user_id = get_current_user_id();
						    $post_data = array(
						        'post_title'    => '',
						        'post_content'  => $_POST['mes']['message'],
						        'post_status'   => 'publish',
						        'post_author'   => $user_id,
						        'post_type'     => 'message',
						    );

						    wp_insert_post( $post_data );

						    $user_info = get_userdata($user_id);
						    $headers[] = 'From: <callback@hermesaruba.express>';
						    $headers[] = 'content-type: text/html';
						    $message = ' Message from Username: '.$user_info->user_login.', First name: '.$user_info->first_name.', Last name: '.$user_info->last_name.', Email: '.$user_info->user_email.', Message: '.$_POST['mes']['message'];

						    wp_mail(array('cathy@pdo.aw', 'jorge.zarraga1@gmail.com'), 'Direct Message', $message, $headers);
						    $success = true;
						}

						if(isset($_POST['mes']) && empty($_POST['mes']['message'])) {
						    $success= false;
						}
					?>
					<?php endif;?>
				    <div class="wrap">
				        <h2>Direct Message</h2>
				        <form method="POST">
				            <div id="extra_fields" class="postbox ">
				            <?php 
				                global $success;
				                if(isset($success) && $success):                    
				            ?>
				                    <div class="updated notice">
				                        <p>Message was send successfuly!</p>
				                    </div>
				            <?php endif;?>
				            <?php
				                if(isset($success) && !$success):                    
				            ?>
				                    <div class="error notice">
				                        <p>There has been an error. Send message failure.</p>
				                    </div>
				            <?php endif;?>
				                <div class="inside">
				                    <p>
				                        <h2>Your message</h2>
				                        <textarea name="mes[message]" rows="10" cols="200"></textarea>
				                    </p>            
				                    <button class="button button-primary" name="mes[send]">Send</button>
				                </div>
				            </div>            
				        </form>
				    </div>				    
			<?php translogistic_row_after(); ?>
			
		<?php translogistic_container_after(); ?>
	<?php translogistic_site_sub_content_end(); ?>

<?php get_footer();