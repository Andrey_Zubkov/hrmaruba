<?php /* Template Name: Send invoice */

get_header(); ?>
	
	<?php translogistic_page_content_banner(); ?>
			
	<?php translogistic_site_sub_content_start(); ?>
		<?php translogistic_container_before(); ?>
			<?php translogistic_row_before(); ?>
				<?php translogistic_content_area_start(); ?>
					
				<?php translogistic_content_area_end(); ?>
				 <?php if ( is_user_logged_in() ): ?>
				 		<?php				 			
				 			if(isset($_POST['invoice_us']) && !empty($_POST['invoice_us']['title'])) {
				 				$success_invoice = false;
	 		                    if($_FILES['invoice_us']) {
			                      $upload_dir = __DIR__.'/../../../uploads/invoice/';
			                      $uploadfile = $upload_dir . basename($_FILES['invoice_us']['name']['file']);               
			                      if(move_uploaded_file($_FILES['invoice_us']['tmp_name']['file'], $uploadfile)){
			                         $user_id = get_current_user_id();
			                         $wp_upload_dir = wp_upload_dir();
									    $post_data = array(
									        'post_title'    => $_POST['invoice_us']['title'],
									        'post_status'   => 'publish',
									        'post_author'   => $user_id,
									        'post_type'     => 'invoice',
									    );
									    $invoice_id = wp_insert_post( $post_data );
									    update_post_meta($invoice_id,'invoice_link', $wp_upload_dir['baseurl']."/invoice/" . basename($_FILES['invoice_us']['name']['file']));
									    $attachment = array($uploadfile);
									    $admin_email = get_option('admin_email');
									    $site_name = get_bloginfo('name');
									    $headers[] = 'From: <callback@hermesaruba.express>';
									    $headers[] = 'content-type: text/html';
									    $message = 'You send invoice on '.$site_name.', invoice title '.$_POST['invoice_us']['title'];
									    $user_id = get_current_user_id();
				 						$user_info = get_userdata($user_id);
									    wp_mail(array('cathy@pdo.aw', 'jorge.zarraga1@gmail.com'), 'Send Invoice', $message, $headers, $attachment);
									    $success_invoice = true;
			                      }
			                    }
				 			}
				 		
				 		?>
					<?php endif;?>
				    <div class="wrap">
				        <h2>Your Invoice</h2>
				        <form method="POST" enctype="multipart/form-data">
				            <div id="extra_fields" class="postbox ">
				            <?php 
				                global $success;
				                if(isset($success_invoice) && $success_invoice):                    
				            ?>
				                    <div class="updated notice">
				                        <p>Invoice was send successfuly!</p>
				                    </div>
				            <?php endif;?>
				            <?php
				                if(isset($success_invoice) && !$success_invoice):                    
				            ?>
				                    <div class="error notice">
				                        <p>There has been an error. Send invoice failure.</p>
				                    </div>
				            <?php endif;?>
				                <div class="inside">
				                    <p>				                        
				                        <p> 
				                        	<span>Title</span>
				                        	<input type="text" name="invoice_us[title]">
				                        </p>
				                        <p> 
				                        	<span>File</span>
				                        	<input type="file" name="invoice_us[file]">
				                        </p>
				                    </p>            
				                    <button class="button button-primary" name="invoice_us[send]">Send</button>
				                </div>
				            </div>            
				        </form>
				    </div>				    
			<?php translogistic_row_after(); ?>
			
		<?php translogistic_container_after(); ?>
	<?php translogistic_site_sub_content_end(); ?>

<?php get_footer();
