<?php
/**
	* The template for displaying woocommerce single
*/
get_header(); ?>

	<?php translogistic_page_content_banner(); ?>

	<?php translogistic_site_sub_content_start(); ?>
		<?php translogistic_container_before(); ?>
			<?php translogistic_row_before(); ?>
				<?php translogistic_post_content_area_start(); ?>
					<div class="page-content">
						<?php woocommerce_content(); ?>
					</div>
				<?php translogistic_content_area_end(); ?>
				<?php translogistic_post_sidebar_start(); ?>
					<?php get_sidebar( 'shop' ); ?>			
				<?php translogistic_sidebar_end(); ?>
			<?php translogistic_row_after(); ?>
		<?php translogistic_container_after(); ?>
	<?php translogistic_site_sub_content_end(); ?>

<?php get_footer();