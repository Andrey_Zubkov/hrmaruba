<?php
/*
 * The template for displaying comments part
*/
$post_comment_hide = get_theme_mod( 'hide_post_comment' );
if( !$post_comment_hide == '1' ):
if ( post_password_required() )
	return;
?>

	<div id="comments" class="comments-area">
	
		<?php if ( have_comments() ) : ?>
			<div class="post-bottom-element">
				<div class="comments-list-area">
					<div class="comment-reply-title"><h2><?php
							printf( _nx( '1 Comment', '%1$s Comments', get_comments_number(), 'comments title', 'translogistic' ),
							number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
						?></h2>
					</div>
					<ol class="comment-list">
					
						<?php
							wp_list_comments( array(
								'style'       => 'ol',
								'short_ping'  => true,
								'avatar_size' => 84,
								'callback' => 'translogistic_comment',
							) );
						?>
					
						<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
						
							<nav class="navigation comment-navigation" role="navigation">
								<h1 class="screen-reader-text section-heading"><?php esc_html_e( 'Comment Navigation', 'translogistic' ); ?></h1>
								<div class="nav-previous"><?php previous_comments_link( esc_html__( '&larr; Older Comments', 'translogistic' ) ); ?></div>
								<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments &rarr;', 'translogistic' ) ); ?></div>
							</nav>
							
						<?php endif; ?>

						<?php if ( ! comments_open() && get_comments_number() ) : ?>
						
							<p class="no-comments"><?php esc_html_e( 'Comments are closed.' , 'translogistic' ); ?></p>
						
						<?php endif; ?>
						
					</ol>
				</div>
			</div>
		<?php endif; ?>

		<div class="post-bottom-element">
		<?php
			$comments_args = array(
				'id_form'           => 'commentform',
				'id_submit'         => 'submit',
				'class_submit'		=> 'btn btn-danger',
				'title_reply_before'    => '<div class="comment-reply-title"><h2>',
				'title_reply_after'    => '</div></h2>',
				'title_reply_to'    => '<div class="comment-title">' . esc_html__('Leave a Reply to', 'translogistic') . ' %s' . '</div>',
				'cancel_reply_link' => esc_html__( 'Cancel Reply', 'translogistic'),
				'label_submit'      => esc_html__( 'Send Message', 'translogistic'),
				'comment_field' =>  '<div class="col-sm-6 col-xs-12 comments-area-col comments-area-col-left"><div class="form-group comments-area-textarea"><textarea class="form-control" placeholder="' . esc_html__('Your Message', 'translogistic') . '' . esc_html__('*', 'translogistic') .  '" name="comment" class="commentbody" id="comment" rows="5" tabindex="4"></textarea></div></div>',

				'comment_notes_before' => '',

				'fields' => apply_filters( 'comment_form_default_fields', array(
					'author' =>
						'<div class="col-sm-6 col-xs-12 comments-area-col comments-area-col-right"><div class="form-group name clearfix"><input class="form-control" type="text" placeholder="' . esc_html__('Name', 'translogistic') . '' . ( $req ?  '' . esc_html__('*', 'translogistic') . '' : '') . '" name="author" id="author" value="' . esc_attr($comment_author) . '" size="22" tabindex="1"' . ($req ? "aria-required='true'" : '' ). ' /></div>',

					'email' =>
						'<div class="form-group email clearfix"><input class="form-control" type="text" placeholder="' . esc_html__('Email', 'translogistic') . '' . ( $req ? '' . esc_html__('*', 'translogistic') . '' : '') . '" name="email" id="email" value="' . esc_attr($comment_author_email) . '" size="22" tabindex="1"' . ($req ? "aria-required='true'" : '' ). ' /></div>',

					'url' =>
						'<div class="form-group website clearfix"><input class="form-control" type="text" placeholder="' . esc_html__('Website URL', 'translogistic') . '" name="url" id="url" value="' . esc_attr($comment_author_url) . '" size="22" tabindex="1" /></div></div>'
					)
				),

			);
			comment_form( $comments_args );
		?>
		</div>
		
	</div>
	
<?php endif; ?>