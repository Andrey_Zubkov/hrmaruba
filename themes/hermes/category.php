<?php
/*
	* The template for displaying category
*/
get_header(); ?>

	<?php translogistic_page_content_banner(); ?>

	<div class="page-title-wrapper">
		<h1><?php printf( '', single_cat_title( '', true ) ); ?></h1>
	</div>
			
	<?php translogistic_site_sub_content_start(); ?>
		<?php translogistic_container_before(); ?>
			<?php translogistic_row_before(); ?>
				<?php translogistic_content_area_start(); ?>
					<?php if ( have_posts() ) : ?>
						<div class="category-post-list post-list">
							<?php while ( have_posts() ) : the_post(); ?>
								<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									<div class="post-wrapper">
										<div class="post-header">
											<h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
											<?php 
											$hide_categoryarchive_name = get_theme_mod( 'hide_categoryarchive_name' );
											if( !$hide_categoryarchive_name == '1' ) :
											?>
												<div class="category"><?php the_category( '', '' ); ?></div>
											<?php endif; ?>
											<?php
											$hide_categoryarchive_post_information = get_theme_mod( 'hide_categoryarchive_post_information' );
											if( !$hide_categoryarchive_post_information == '1' ) : ?>
												<ul class="post-information">
													<li class="author"><?php the_author_posts_link(); ?></li>
													<li class="date"><?php the_time( get_option( 'date_format' ) ); ?></li>
													<li class="comment"><a href="<?php the_permalink(); ?>#comments" title="<?php the_title_attribute(); ?>"><?php comments_number( esc_html__( '0 Comment', 'translogistic' ), esc_html__( '1 Comment', 'translogistic' ), esc_html__( '% Comments', 'translogistic' ) ); ?></a></li>
												</ul>
											<?php endif; ?>
										</div>
										<div class="post-content">
											<?php 
											$hide_categoryarchive_featured_image = get_theme_mod( 'hide_categoryarchive_featured_image' );
											if( !$hide_categoryarchive_featured_image == '1' ) :
											?>
												<?php if ( has_post_thumbnail() ) : ?>
													<div class="post-image">
														<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
															<?php the_post_thumbnail( 'translogistic-blog-post-image' ); ?>
														</a>
													</div>
												<?php endif; ?>
											<?php endif; ?>
											<?php
											$hide_categoryarchive_excerpt = get_theme_mod( 'hide_categoryarchive_excerpt' );
											if( !$hide_categoryarchive_excerpt == '1' ) : ?>
												<div class="post-excerpt">
													<?php the_excerpt(); ?>
												</div>
											<?php endif; ?>
										</div>
										<?php 
										$hide_categoryarchive_post_read_more = get_theme_mod( 'hide_categoryarchive_post_read_more' );
										$hide_categoryarchive_post_social_share_more = get_theme_mod( 'hide_categoryarchive_post_social_share_more' );
										if( !$hide_categoryarchive_post_read_more == '1' or !$hide_categoryarchive_post_social_share_more == '1' ) :
										?>
											<div class="post-bottom">
												<?php
												$hide_categoryarchive_post_read_more = get_theme_mod( 'hide_categoryarchive_post_read_more' );
												if( !$hide_categoryarchive_post_read_more == '1' ) : ?>
													<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="more"><?php printf( esc_html__( 'More', 'translogistic' ) ); ?></a>
												<?php endif; ?>
												<?php
												$hide_categoryarchive_post_social_share_more = get_theme_mod( 'hide_categoryarchive_post_social_share_more' );
												if( !$hide_categoryarchive_post_social_share_more == '1' ) : ?>
													<?php translogistic_general_post_social_share(); ?>
												<?php endif; ?>
											</div>
										<?php endif; ?>
									</div>
								</article>
							<?php endwhile; ?>
						</div>
						<?php translogistic_pagination(); ?>
					<?php else : ?>
						<?php get_template_part( 'include/formats/content', 'none' ); ?>
					<?php endif; ?>
				<?php translogistic_content_area_end(); ?>
				
				<?php get_sidebar(); ?> 
			<?php translogistic_row_after(); ?>
			
		<?php translogistic_container_after(); ?>
	<?php translogistic_site_sub_content_end(); ?>

<?php get_footer();