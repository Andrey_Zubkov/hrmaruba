<?php
/**
	* The template for displaying single
*/
get_header(); ?>

	<?php translogistic_page_content_banner(); ?>

	<div class="page-title-wrapper">
		<h1><?php printf( esc_html__( 'Attachment', 'translogistic' ) ); ?></h1>
	</div>

	<?php translogistic_site_sub_content_start(); ?>
		<?php translogistic_container_before(); ?>
			<?php translogistic_row_before(); ?>
				<?php translogistic_content_area_start(); ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'include/formats/content-attachment' ); ?>
					<?php endwhile; ?>
				<?php translogistic_content_area_end(); ?>
				<?php get_sidebar(); ?>
			<?php translogistic_row_after(); ?>
		<?php translogistic_container_after(); ?>
	<?php translogistic_site_sub_content_end(); ?>

<?php get_footer();