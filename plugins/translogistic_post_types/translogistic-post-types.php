<?php
/**
* Plugin Name: Translogistic Theme: Post Types
* Plugin URI: http://themeforest.net/user/gloriatheme
* Description: Translogistic WordPress theme post types plugin.
* Version: 1.0
* Author: Gloria Theme
* Author URI: http://gloriatheme.com/
*/

/*--------------- TEAM START ---------------*/
if ( ! function_exists('translogistic_cargo') ) {
	function translogistic_cargo() {
		$labels = array(
			'name'                => _x( 'Cargo', 'Post Type General Name', 'translogistic' ),
			'singular_name'       => _x( 'cargo', 'Post Type Singular Name', 'translogistic' ),
			'menu_name'           => esc_html__( 'Cargo', 'translogistic' ),
			'parent_item_colon'   => esc_html__( 'Parent Cargo:', 'translogistic' ),
			'all_items'           => esc_html__( 'All Cargo', 'translogistic' ),
			'view_item'           => esc_html__( 'View Cargo', 'translogistic' ),
			'add_new_item'        => esc_html__( 'Add New Cargo Item', 'translogistic' ),
			'add_new'             => esc_html__( 'Add New Cargo', 'translogistic' ),
			'edit_item'           => esc_html__( 'Edit Cargo', 'translogistic' ),
			'update_item'         => esc_html__( 'Update Cargo', 'translogistic' ),
			'search_items'        => esc_html__( 'Search Cargo', 'translogistic' ),
			'not_found'           => esc_html__( 'Not Cargo found', 'translogistic' ),
			'not_found_in_trash'  => esc_html__( 'Not Cargo found in Trash', 'translogistic' ),
		);
		$args = array(
			'label'               => esc_html__( 'Cargo', 'translogistic' ),
			'description'         => esc_html__( 'Cargo post type description.', 'translogistic' ),
			'labels'              => $labels,
			'supports'            => array( 'title' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => false,
			'menu_position'       => 20,
			'menu_icon'           => 'dashicons-location-alt',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
		);
		register_post_type( 'translogistic_cargo', $args );
	}
	add_action( 'init', 'translogistic_cargo', 0 );
}
/*--------------- TEAM END ---------------*/