<?php
/**
* Plugin Name: Translogistic Theme: Page Builder Elements
* Plugin URI: http://themeforest.net/user/gloriatheme
* Description: Translogistic WordPress theme builder elements plugin.
* Version: 1.0
* Author: Gloria Theme
* Author URI: http://gloriatheme.com/
*/

/*------------- Translogistic Theme Visual Composer Elements -------------*/

/*------------- COLUMN SHORTCODE START -------------*/
function translogistic_shortcode_column( $column_atts , $column_content = null ) {
	extract( shortcode_atts(
		array(
			'size' => '1',
		), $column_atts )
	);	
	$size = strip_tags( esc_attr( $size ) );
	if ( $size == "1" )
	{
		return '<div class="col-sm-1 col-xs-12">' . do_shortcode( $column_content ) . '</div>';
	}
	elseif ( $size == "2" )
	{
		return '<div class="col-sm-2 col-xs-12">' . do_shortcode( $column_content ) . '</div>';
	}
	elseif ( $size == "3" )
	{
		return '<div class="col-sm-3 col-xs-12">' . do_shortcode( $column_content ) . '</div>';
	}
	elseif ( $size == "4" )
	{
		return '<div class="col-sm-4 col-xs-12">' . do_shortcode( $column_content ) . '</div>';
	}
	elseif ( $size == "5" )
	{
		return '<div class="col-sm-5 col-xs-12">' . do_shortcode( $column_content ) . '</div>';
	}
	elseif ( $size == "6" )
	{
		return '<div class="col-sm-6 col-xs-12">' . do_shortcode( $column_content ) . '</div>';
	}
	elseif ( $size == "7" )
	{
		return '<div class="col-sm-7 col-xs-12">' . do_shortcode( $column_content ) . '</div>';
	}
	elseif ( $size == "8" )
	{
		return '<div class="col-sm-8 col-xs-12">' . do_shortcode( $column_content ) . '</div>';
	}
	elseif ( $size == "9" )
	{
		return '<div class="col-sm-9 col-xs-12">' . do_shortcode( $column_content ) . '</div>';
	}
	elseif ( $size == "10" )
	{
		return '<div class="col-sm-10 col-xs-12">' . do_shortcode( $column_content ) . '</div>';
	}
	
	elseif ( $size == "11" )
	{
		return '<div class="col-sm-11 col-xs-12">' . do_shortcode( $column_content ) . '</div>';
	}
	elseif ( $size == "12" )
	{
		return '<div class="col-sm-12 col-xs-12">' . do_shortcode( $column_content ) . '</div>';
	}
}
add_shortcode( 'col', 'translogistic_shortcode_column' );
/*------------- COLUMN SHORTCODE END -------------*/

/*------------- ROW SHORTCODE START -------------*/
function translogistic_shortcode_row( $row_atts , $row_content = null ) {
	extract( shortcode_atts(
		array(
			'class' => 'clearfix',
		), $row_atts )
	);
	$class = strip_tags( esc_attr( $class ) );
	return '<div class="row ' . $class . '">' . do_shortcode( $row_content ) . '</div>';	
}
add_shortcode( 'row', 'translogistic_shortcode_row' );
/*------------- ROW SHORTCODE END -------------*/

/*------------- CONTENT WIDGET TITLE START -------------*/
function content_widget_title_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'title' => '',
			'titlecolor' => '',
			'align' => '',
			'iconcolor' => '',
			'titlefontsize' => '',
			'customclass' => ''
		), $atts
	);
	
	$output = '';
	
		if( $atts['align'] == "right" ) {
			$align = " text-right";
		} elseif( $atts['align'] == "center" ) {
			$align = " text-center";
		} else {
			$align = " text-left";		
		}
	
		if( !empty( $atts['titlecolor'] ) ) {
			$text_color = 'color:' . esc_attr( $atts['titlecolor'] ) . ';';
		}
		else {
			$text_color ="";
		}
	
		if( !empty( $atts['titlefontsize'] ) ) {
			$titlefontsize = 'font-size:' . esc_attr( $atts['titlefontsize'] ) . ';';
		}
		else {
			$titlefontsize ="";
		}
		
		if( !empty( $atts['titlecolor'] ) or !empty( $atts['titlefontsize'] ) ) {
			$titleStyle = ' style="' . $text_color . $titlefontsize . '"';
		} else {
			$titleStyle = "";
		}
	
	if( !empty( $atts['title'] ) ) {
		$output .= '<div class="content-widget-title ' . esc_attr( $atts['customclass'] ) . ' ' . esc_attr( $atts['iconcolor'] ) . esc_attr( $align ) .  '">';
			if( !empty( $atts['title'] ) ) {
				$allowed_html = array ( 'br' => array() );
				$output .= '<h3' . $titleStyle . '>' . wp_kses( $atts['title'] , $allowed_html ) . '</h3>';
			}
		$output .= '</div>';
	}

	return $output;
}
add_shortcode("contentwidgettitle", "content_widget_title_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Content Widget Title", 'translogistic' ),
		"base" => "contentwidgettitle",
		"class" => "",
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/custom_widget_title.png',
		"description" =>esc_html__( 'Content title widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Title",'translogistic' ),
				"description" => esc_html__( "You can enter the title.",'translogistic' ),
				"param_name" => "title",
				"value" => "",
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__( "Align",'translogistic' ),
				"description" => esc_html__( "You can select alignment state of the button.",'translogistic' ),
				"param_name" => "align",
				"value" => array(
					esc_html__( "Left", 'translogistic' ) => "left",
					esc_html__( "Right", 'translogistic' ) => "right",
					esc_html__( "Center", 'translogistic' ) => "center"
				)
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Title Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "titlecolor",
				"value" => "",
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__( "Custom Icon Color",'translogistic' ),
				"description" => esc_html__( "Please select the icon color.",'translogistic' ),
				"param_name" => "iconcolor",
				"value" => array(
					esc_html__( "Default", 'translogistic' ) => "default",
					esc_html__( "White", 'translogistic' ) => "white",
					esc_html__( "Black", 'translogistic' ) => "black",
					esc_html__( "Gray", 'translogistic' ) => "gray",
					esc_html__( "Red", 'translogistic' ) => "red",
					esc_html__( "Yellow", 'translogistic' ) => "yellow",
					esc_html__( "Green", 'translogistic' ) => "green",
					esc_html__( "Blue", 'translogistic' ) => "blue"
				)
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Title Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the title font size. Example: 15px.",'translogistic' ),
				"param_name" => "titlefontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Extra Class Name",'translogistic' ),
				"description" => esc_html__( "Please write your custom class.",'translogistic' ),
				"param_name" => "customclass",
				"value" => "",
			)
		)
	) );
}
/*------------- CONTENT WIDGET TITLE END -------------*/

/*------------- SERVICES BOX START -------------*/
function services_box_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'title' => '',
			'text' => '',
			'icon' => '',
			'iconstyle' => '',
			'align' => '',
			'iconcolor' => '',
			'titlecolor' => '',
			'textcolor' => '',
			'titlefontsize' => '',
			'textfontsize' => '',
			'customclass' => ''
		), $atts
	);
	
		if( $atts['align'] == "right" ) {
			$align = "text-right";
		} elseif( $atts['align'] == "center" ) {
			$align = "text-center";
		} elseif( $atts['align'] == "icontitlecenterparagraphleft" ) {
			$align = "icontitlecenterparagraphleft";
		} else {
			$align = "text-left";		
		}
	
	$output = '';
	
		if( !empty( $atts['textcolor'] ) ) {
			$text_color = ' color:' . esc_attr( $atts['textcolor'] ) . ';';
		}
		else {
			$text_color="";
		}

		if( !empty( $atts['iconstyle'] ) ) {
			$iconstyle = ' icon-style-' . esc_attr( $atts['iconstyle'] );
		} else {
			$iconstyle = "";
		}
	
		if( !empty( $atts['titlecolor'] ) ) {
			$titlecolor = ' color:' . esc_attr( $atts['titlecolor'] ) . ';';
		}
		else {
			$titlecolor ="";
		}
	
		if( !empty( $atts['iconcolor'] ) ) {
			$iconcolor = ' style="color:' . esc_attr( $atts['iconcolor'] ) . ';"';
		}
		else {
			$iconcolor ="";
		}
	
		if( !empty( $atts['iconcolor'] ) ) {
			$iconcolor_border = ' style="border-color:' . esc_attr( $atts['iconcolor'] ) . ';"';
		}
		else {
			$iconcolor_border ="";
		}
	
		if( !empty( $atts['titlefontsize'] ) ) {
			$titlefontsize = 'font-size:' . esc_attr( $atts['titlefontsize'] ) . ';';
		}
		else {
			$titlefontsize ="";
		}
	
		if( !empty( $atts['textfontsize'] ) ) {
			$textfontsize = 'font-size:' . esc_attr( $atts['textfontsize'] ) . ';';
		}
		else {
			$textfontsize ="";
		}
		
		if( !empty( $atts['titlecolor'] ) or !empty( $atts['titlefontsize'] ) ) {
			$titleStyle = ' style="' . $titlecolor . $titlefontsize . '"';
		} else {
			$titleStyle = "";
		}
		
		if( !empty( $atts['textcolor'] ) or !empty( $atts['textfontsize'] ) ) {
			$textStyle = ' style="' . $text_color . $textfontsize . '"';
		} else {
			$textStyle = "";
		}
		
		$output .= '<div class="services-box ' . esc_attr( $atts['customclass'] ) . $iconstyle . '">';
		
			if( !empty( $atts['icon'] ) ) {
				$output .= '<div class="services-box-icon ' . esc_attr( $align ) . '"' . $iconcolor_border . '>';
					$output .= '<i class="fa fa-' . esc_attr( $atts['icon'] ) . '"' . $iconcolor . '></i>';
				$output .= '</div>';
			}
			
			if( !empty( $atts['title'] ) ) {
				$output .= '<h3 class="' . esc_attr( $align ) . '"' . $titleStyle . '>' . esc_attr( $atts['title'] ) . '</h3>';
			}
			
			if( !empty( $atts['text'] ) ) {
				$output .= '<p class="' . esc_attr( $align ) . '"' . $textStyle . '>' . esc_attr( $atts['text'] ) . '</p>';
			}
			
		$output .= '</div>';
			
	return $output;
}
add_shortcode("services_box", "services_box_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Services Box", 'translogistic' ),
		"base" => "services_box",
		"class" => "",
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/services_box.png',
		"description" =>esc_html__( 'Services box widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Title",'translogistic' ),
				"description" => esc_html__( "You can enter the title.", 'translogistic' ),
				"param_name" => "title",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Text",'translogistic' ),
				"description" => esc_html__( "You can enter the text.", 'translogistic' ),
				"param_name" => "text",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Icon",'translogistic' ),
				"description" => esc_html__( "You can enter the icon name. List of the icons is available in the documentation file. Example: edge, automobile, bel-o.", 'translogistic' ),
				"param_name" => "icon",
				"value" => "",
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Icon Style",'translogistic' ),
				"description" => esc_html__( "You can select the icon style.",'translogistic' ),
				"param_name" => "iconstyle",
				"value" => array(
					esc_html__( "Default", 'translogistic' ) => "default",
					esc_html__( "Alternative", 'translogistic' ) => "alternative"
				)
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__( "Align",'translogistic' ),
				"description" => esc_html__( "You can select alignment state of the service box.",'translogistic' ),
				"param_name" => "align",
				"value" => array(
					esc_html__( "Left", 'translogistic' ) => "left",
					esc_html__( "Right", 'translogistic' ) => "right",
					esc_html__( "Center", 'translogistic' ) => "center",
					esc_html__( "Icon And Title Center - Paragraph Left", 'translogistic' ) => "icontitlecenterparagraphleft"
				)
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Icon Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "iconcolor",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Title Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "titlecolor",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Text Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "textcolor",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Title Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "titlefontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Text Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "textfontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Extra Class Name",'translogistic' ),
				"description" => esc_html__( "Please write your custom class.",'translogistic' ),
				"param_name" => "customclass",
				"value" => "",
			)
		)
	) );
}
/*------------- SERVICES BOX END -------------*/

/*------------- DEPARTMENTS START -------------*/
function translogistic_departments_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'carousel' => '',
			'columns' => ''
		), $atts
	);
	
	$output = '';
	
		if( !empty( $atts['columns'] ) ) {
			$columns = esc_attr( $atts['columns'] );
		} else {
			$columns = "2";
		}
		
		$random_id = rand( 0, 99999999 );
		$random_id = $columns * $random_id;
		
		$output .= '<div class="departments-content">';
		
		if( $atts['carousel'] != "false" ) {
			$output .= '<div class="departments-navigation">
								<a class="departments-navigation-prev"><i class="fa fa-long-arrow-left"></i></a>
								<a class="departments-navigation-next"><i class="fa fa-long-arrow-right"></i></a>
							</div>';
		}
						
			$output .= '<div class="departments-content-owl departments-content-' . esc_attr( $random_id ) . '">';
				$output .= do_shortcode( $content );
			$output .= '</div>';		
		$output .= '</div>';
		
		$output .= "<script>
							jQuery(document).ready(function($){
								$('.departments-content-" . esc_attr( $random_id ) . "').owlCarousel({
									loop:true,
									nav:false,
									margin:30,
									dots:false,
									responsive:{
										0:{
											items:1,
										},
										768:{
											items:" . esc_attr( $columns ) .",
										},
									}
								});
								
								departmentsowl = $('.departments-content-" . esc_attr( $random_id ) . "').owlCarousel();
								$('.departments-navigation-prev').click(function () {
									departmentsowl.trigger('prev.owl.carousel');
								});

								$('.departments-navigation-next').click(function () {
									departmentsowl.trigger('next.owl.carousel');
								});
							});
						</script>";

	return $output;
}
add_shortcode("translogistic_departments", "translogistic_departments_shortcode");

function translogistic_departments_item_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'title' => '',
			'text' => '',
			'buttontitle' => '',
			'buttonlink' => '',
			'departmentimage' => '',
			'icon' => '',
			'designstyle' => '',
			'iconcolor' => '',
			'titlecolor' => '',
			'textcolor' => '',
			'buttoncolor' => '',
			'titlefontsize' => '',
			'textfontsize' => '',
			'buttonfontsize' => '',
			'customclass' => ''
		), $atts
	);
	
	$output = '';
	
		if( !empty( $atts['title'] ) or !empty( $atts['text'] ) ) {

			if( !empty( $atts['customclass'] ) ) {
				$customClass = ' ' . esc_attr( $atts['customclass'] );
			} else {
				$customClass = "";
			}
		
			$output .= '<div class="item' . $customClass . '">';

				if( !empty( $atts['iconcolor'] ) ) {
					$iconcolor = ' style="color:' . esc_attr( $atts['iconcolor'] ) . ';"';
				}
				else {
					$iconcolor ="";
				}
			
				if( !empty( $atts['iconcolor'] ) ) {
					$iconcolor_border = ' style="border-color:' . esc_attr( $atts['iconcolor'] ) . ';"';
				}
				else {
					$iconcolor_border ="";
				}

				if( !empty( $atts['buttoncolor'] ) ) {
					$buttoncolor = ' style="color:' . esc_attr( $atts['buttoncolor'] ) . ';"';
				}
				else {
					$buttoncolor ="";
				}
			
				if( !empty( $atts['buttonfontsize'] ) ) {
					$buttonfontsize = 'font-size:' . esc_attr( $atts['buttonfontsize'] ) . ';';
				}
				else {
					$buttonfontsize ="";
				}
			
				if( !empty( $atts['buttoncolor'] ) or !empty( $atts['buttonfontsize'] ) ) {
					$buttoncolor_border = 'border-color:' . esc_attr( $atts['buttoncolor'] ) . ';color:' . esc_attr( $atts['buttoncolor'] ) . ';';
					$buttonStyle = ' style="' . $buttoncolor_border  . $buttonfontsize . '"';
				}
				else {
					$buttonStyle ="";
				}
	
				if( !empty( $atts['textcolor'] ) ) {
					$text_color = ' color:' . esc_attr( $atts['textcolor'] ) . ';';
				}
				else {
					$text_color="";
				}
			
				if( !empty( $atts['titlecolor'] ) ) {
					$titlecolor = ' color:' . esc_attr( $atts['titlecolor'] ) . ';';
				}
				else {
					$titlecolor ="";
				}
	
				if( !empty( $atts['titlefontsize'] ) ) {
					$titlefontsize = 'font-size:' . esc_attr( $atts['titlefontsize'] ) . ';';
				}
				else {
					$titlefontsize ="";
				}
			
				if( !empty( $atts['textfontsize'] ) ) {
					$textfontsize = 'font-size:' . esc_attr( $atts['textfontsize'] ) . ';';
				}
				else {
					$textfontsize ="";
				}
				
				if( !empty( $atts['titlecolor'] ) or !empty( $atts['titlefontsize'] ) ) {
					$titleStyle = ' style="' . $titlecolor . $titlefontsize . '"';
				} else {
					$titleStyle = "";
				}
				
				if( !empty( $atts['textcolor'] ) or !empty( $atts['textfontsize'] ) ) {
					$textStyle = ' style="' . $text_color . $textfontsize . '"';
				} else {
					$textStyle = "";
				}

				if( !empty( $atts['designstyle'] ) ) {
					$designstyle = ' ' . esc_attr( $atts['designstyle'] );
				} else {
					$designstyle = "";
				}
			
				if( !empty( $atts['departmentimage'] ) ) {
					$image = wp_get_attachment_image_src($atts["departmentimage"], "full");
					$output .= '<div class="image">';
						$output .= '<a href="' . esc_url( $atts['buttonlink'] ) . '" title="' . esc_attr( $atts['title'] ) . '" ><img src="' . esc_url( $image[0] ) . '" alt="' . esc_attr( $atts['title'] ) . '" /></a>';
				
							if( !empty( $atts['icon'] ) ) {
							$output .= '<div class="department-icon"' . $iconcolor_border . '>';
								$output .= '<i class="fa fa-' . esc_attr( $atts['icon'] ) . '"' . $iconcolor . '></i>';
							$output .= '</div>';
							}
				
					$output .= '</div>';
				}
				
				if( !empty( $atts['title'] ) ) {
					$output .= '<h4' . $titleStyle . '>' . esc_attr( $atts['title'] ) . '</h4>';
				}
				
				if( !empty( $atts['text'] ) ) {
					$output .= '<div class="department-text"' . $textStyle . '>' . esc_attr( $atts['text'] ) . '</div>';
				}
				
				if( !empty( $atts['buttontitle'] ) ) {
					$output .= '<div class="department-button' . $designstyle . '">';
						$output .= '<a href="' . esc_url( $atts['buttonlink'] ) . '" title="' . esc_attr( $atts['buttontitle'] ) . '"' . $buttonStyle . '><i class="fa fa-long-arrow-right"' . $buttoncolor . '></i>' . esc_attr( $atts['buttontitle'] ) . '</a>';
					$output .= '</div>';
				}
			
			$output .= '</div>';
		}

	return $output;
}
add_shortcode("translogistic_departments_item", "translogistic_departments_item_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Departments", 'translogistic' ),
		"base" => "translogistic_departments",
		"class" => "",
		"as_parent" => array('only' => 'translogistic_departments_item'),
		"js_view" => 'VcColumnView',
		"content_element" => true,
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/departments.png',
		"description" =>esc_html__( 'Departments widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__( "Navigation",'translogistic' ),
				"description" => esc_html__( "You can select the navigation status.",'translogistic' ),
				"param_name" => "carousel",
				"value" => array(
					esc_html__( "True", 'translogistic' ) => "true",
					esc_html__( "False", 'translogistic' ) => "false",
				)
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__( "Column",'translogistic' ),
				"description" => esc_html__( "You can select the column size.",'translogistic' ),
				"param_name" => "columns",
				"value" => array(
					esc_html__( "1", 'translogistic' ) => "1",
					esc_html__( "2", 'translogistic' ) => "2",
					esc_html__( "3", 'translogistic' ) => "3",
					esc_html__( "4", 'translogistic' ) => "4",
					esc_html__( "5", 'translogistic' ) => "5"
				)
			)
		)
	) );
}

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Departments Item", 'translogistic' ),
		"base" => "translogistic_departments_item",
		"class" => "",
		"as_child" => array( 'only' => 'translogistic_departments' ),
		"content_element" => true,
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/departments.png',
		"description" =>esc_html__( 'Departments item widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Title",'translogistic' ),
				"description" => esc_html__( "You can enter the title.", 'translogistic' ),
				"param_name" => "title",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Text",'translogistic' ),
				"description" => esc_html__( "You can enter the text.", 'translogistic' ),
				"param_name" => "text",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Button Title",'translogistic' ),
				"description" => esc_html__( "You can enter the button title.", 'translogistic' ),
				"param_name" => "buttontitle",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Button Link",'translogistic' ),
				"description" => esc_html__( "You can enter the button link.", 'translogistic' ),
				"param_name" => "buttonlink",
				"value" => "",
			),
			array(
				"type" => "attach_image",
				"class" => "",
				"heading" => esc_html__( "Image",'translogistic' ),
				"description" => esc_html__( "You can the upload your department image.", 'translogistic' ),
				"param_name" => "departmentimage",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Icon",'translogistic' ),
				"description" => esc_html__( "You can enter the icon name. List of the icons is available in the documentation file. Example: edge, automobile, bel-o.", 'translogistic' ),
				"param_name" => "icon",
				"value" => "",
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Button Design Style",'translogistic' ),
				"description" => esc_html__( "You can select the button design style.",'translogistic' ),
				"param_name" => "designstyle",
				"value" => array(
					esc_html__( "Theme One Color", 'translogistic' ) => "themeonecolor",
					esc_html__( "Theme Two Color", 'translogistic' ) => "themetwocolor",
					esc_html__( "Theme Three Color", 'translogistic' ) => "themethreecolor",
					esc_html__( "Theme Four Color", 'translogistic' ) => "themefourcolor",
					esc_html__( "Custom", 'translogistic' ) => "custom"
				)
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Icon Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "iconcolor",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Title Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "titlecolor",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Text Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "textcolor",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Button Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "buttoncolor",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Title Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "titlefontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Text Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "textfontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Button Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "buttonfontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Extra Class Name",'translogistic' ),
				"description" => esc_html__( "Please write your custom class.",'translogistic' ),
				"param_name" => "customclass",
				"value" => "",
			)
		)
	) );
}

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
	class WPBakeryShortCode_translogistic_departments extends WPBakeryShortCodesContainer {}
}
/*------------- DEPARTMENTS END -------------*/

/*------------- TESTIMONIAL - BIG STYLE START -------------*/
function testimonialsinglebigstyle_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'contentsingle' => '',
			'colorone' => '',
			'colortwo' => '',
			'textfontsize' => ''
		), $atts
	);
	
	$output = '';
	
		if( !empty( $atts['contentsingle'] ) ) {
			
			$output .= '<div class="testimonial-single">';
		
				if( !empty( $atts['colorone'] ) ) {
					$colorone = ' style="color:' . esc_attr( $atts['colorone'] ) . ';"';
				}
				else {
					$colorone = "";
				}
		
				if( !empty( $atts['colorone'] ) ) {
					$colorone2 = ' style="border-color:' . esc_attr( $atts['colorone'] ) . ';"';
				}
				else {
					$colorone2 = "";
				}
		
				if( !empty( $atts['colortwo'] ) ) {
					$colortwo = 'color:' . esc_attr( $atts['colortwo'] ) . ';';
				}
				else {
					$colortwo = "";
				}
			
				if( !empty( $atts['textfontsize'] ) ) {
					$textfontsize = 'font-size:' . esc_attr( $atts['textfontsize'] ) . ';';
				}
				else {
					$textfontsize ="";
				}
				
				if( !empty( $atts['colortwo'] ) or !empty( $atts['textfontsize'] ) ) {
					$textStyle = ' style="' . $colortwo . $textfontsize . '"';
				} else {
					$textStyle = "";
				}
			
				$output .= '<div class="icon"' . $colorone . '><i class="fa fa-quote-right testimonial-blockquote-icon"></i></div>';
			
				if( !empty( $atts['contentsingle'] ) ) {
					$allowed_html = array ( 'br' => array() );
					$output .= '<div class="testimonial-single-content"' . $textStyle . '><p>' . wp_kses( $atts['contentsingle'], $allowed_html ) . '</p></div>';
				}
				
				$output .= '<div class="testimonial-single-star-icon"' . $colorone2 . '></div>
									<div class="testimonial-single-star-icon"' . $colorone2 . '></div>
									<div class="testimonial-single-star-icon"' . $colorone2 . '></div>
									<div class="testimonial-single-star-icon"' . $colorone2 . '></div>';
				
			$output .= '</div>';
			
		}
		
	return $output;
	
}
add_shortcode("testimonialsinglebigstyle", "testimonialsinglebigstyle_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Testimonial - Big Style", 'translogistic' ),
		"base" => "testimonialsinglebigstyle",
		"class" => "",
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/testimonial_single.png',
		"description" =>esc_html__( 'Testimonial big single style widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Content",'translogistic' ),
				"description" => esc_html__( "You can enter the customer feedback.", 'translogistic' ),
				"param_name" => "contentsingle",
				"value" => ""
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Content Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "colortwo",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Icon Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "colorone",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Text Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "textfontsize",
				"value" => "",
			)
		)
	) );
}
/*------------- TESTIMONIAL - BIG STYLE END -------------*/

/*------------- TESTIMONIAL - SINGLE START -------------*/
function testimonialsingle_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'contentsingle' => '',
			'name' => '',
			'company' => '',
			'colorone' => '',
			'textfontsize' => '',
			'namecompanyfontsize' => ''
		), $atts
	);
	
	$output = '';
	
		if( !empty( $atts['contentsingle'] ) ) {
			
			if( !empty( $atts['name'] ) ) {
				$name = '<span class="name">' .  $atts['name'] . '</span>';
			}
			else {
				$name = "";
			}
			
			if( !empty( $atts['company'] ) ) {
				$company = '<span class="company">' .  $atts['company'] . '</span>';
			}
			else {
				$company = "";
			}
	
			if( !empty( $atts['colorone'] ) ) {
				$colorone = 'color:' . $atts['colorone'] . ';';
			}
			else {
				$colorone = "";
			}
		
			if( !empty( $atts['textfontsize'] ) ) {
				$textfontsize = 'font-size:' . esc_attr( $atts['textfontsize'] ) . ';';
			}
			else {
				$textfontsize ="";
			}
		
			if( !empty( $atts['namecompanyfontsize'] ) ) {
				$namecompanyfontsize = 'font-size:' . esc_attr( $atts['namecompanyfontsize'] ) . ';';
			}
			else {
				$namecompanyfontsize ="";
			}
			
			if( !empty( $atts['colorone'] ) ) {
				$textColorStyle = ' style="' . $colorone . '"';
			} else {
				$textColorStyle = "";
			}
			
			if( !empty( $atts['textfontsize'] ) ) {
				$textFontStyle = ' style="' . $textfontsize . '"';
			} else {
				$textFontStyle = "";
			}
			
			if( !empty( $atts['namecompanyfontsize'] ) ) {
				$namecompanyFontStyle = ' style="' . $namecompanyfontsize . '"';
			} else {
				$namecompanyFontStyle = "";
			}
		
			$output .= '<div class="testimonial-single-two"' . $textColorStyle . '>';
				
			
				$output .= '<div class="icon"><i class="fa fa-quote-right testimonial-single-two-blockquote-icon"></i></div>';
			
				if( !empty( $atts['contentsingle'] ) ) {
					$output .= '<div class="testimonial-single-two-content"' . $textFontStyle . '><p>' . $atts['contentsingle'] . '</p></div>';
				}
				
				if( !empty( $atts['name'] ) or !empty( $atts['company'] ) ) {
					$output .= '<div class="testimonial-single-two-name"' . $namecompanyFontStyle . '>' . $name . $company . '</div>';
				}
				
			$output .= '</div>';
			
		}
		
	return $output;
	
}
add_shortcode("testimonialsingle", "testimonialsingle_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Testimonial - Single", 'translogistic' ),
		"base" => "testimonialsingle",
		"class" => "",
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/testimonial_single2.png',
		"description" =>esc_html__( 'Testimonial single style widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Content",'translogistic' ),
				"description" => esc_html__( "You can enter the customer feedback.", 'translogistic' ),
				"param_name" => "contentsingle",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Name",'translogistic' ),
				"description" => esc_html__( "You can enter the customer name.", 'translogistic' ),
				"param_name" => "name",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Company",'translogistic' ),
				"description" => esc_html__( "You can enter the company name.", 'translogistic' ),
				"param_name" => "company",
				"value" => ""
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "colorone",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Text Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "textfontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Name &amp; Company Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "namecompanyfontsize",
				"value" => "",
			)
		)
	) );
}
/*------------- TESTIMONIAL - SINGLE END -------------*/

/*------------- TESTIMONIAL - MODERN START -------------*/
function testimonial_modern_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'column' => '',
			'carousel' => '',
			'dotscolor' => ''
		), $atts
	);
	
	$output = '';
	
		if( !empty( $atts['column'] ) ) {
			$column = esc_attr( $atts['column'] );
		} else {
			$column = "1";
		}
		
		$random_id = rand( 0, 99999999 );
		$random_id = $column * $random_id;
		
		$output .= '<div class="testimonial-modern-widget ' . esc_attr( $atts['dotscolor'] ) . '">';
		
			if( $atts['carousel'] != "false" ) {
				$output .= '<div class="testimonial-modern-widget-navigation">
									<a class="testimonial-modern-widget-prev"><i class="fa fa-long-arrow-left"></i></a>
									<a class="testimonial-modern-widget-next"><i class="fa fa-long-arrow-right"></i></a>
								</div>';
			}
			
			$output .= '<div class="owl-carousel-testimonial-modern-' . esc_attr( $random_id ) . ' owl-theme">';
				$output .= do_shortcode( $content );
			$output .= '</div>';
		$output .= '</div>';
		
		$output .= "<script>
							jQuery(document).ready(function($){
								$('.owl-carousel-testimonial-modern-" . esc_attr( $random_id ) . "').owlCarousel({
									loop:true,
									nav:false,
									margin:65,
									dots:true,
									responsive:{
										0:{
											items:1,
										},
										768:{
											items:" . esc_attr( $column ) .",
										},
									}
								});
								
								testimonialowl = $('.owl-carousel-testimonial-modern-" . esc_attr( $random_id ) . "').owlCarousel();
								$('.testimonial-modern-widget-prev').click(function () {
									testimonialowl.trigger('prev.owl.carousel');
								});

								$('.testimonial-modern-widget-next').click(function () {
									testimonialowl.trigger('next.owl.carousel');
								});
							});
						</script>";

	return $output;
}
add_shortcode("testimonial_modern", "testimonial_modern_shortcode");

function testimonial_modern_item_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'image' => '',
			'style' => '',
			'name' => '',
			'company' => '',
			'contentmodern' => '',
			'namecolor' => '',
			'contentcolor' => '',
			'textfontsize' => '',
			'namecompanyfontsize' => '',
			'customclass' => ''
		), $atts
	);
	
	$output = '';

		if( !empty( $atts['style'] ) ) {
			$style = ' ' . esc_attr( $atts['style'] );
		} else {
			$style = "";
		}
		
		if( !empty( $atts['contentcolor'] ) ) {
			$contentcolor = 'color:' . esc_attr( $atts['contentcolor'] ) . ';';
		}
		else {
			$contentcolor = "";
		}
		
		if( !empty( $atts['textfontsize'] ) ) {
			$textfontsize = 'font-size:' . esc_attr( $atts['textfontsize'] ) . ';';
		}
		else {
			$textfontsize ="";
		}
		
		if( !empty( $atts['textfontsize'] ) or !empty( $atts['contentcolor'] ) ) {
			$textStyle = ' style="' . $textfontsize . $contentcolor . '"';
		} else {
			$textStyle = "";
		}
		
		if( !empty( $atts['namecompanyfontsize'] ) ) {
			$namecompanyfontsize = 'font-size:' . esc_attr( $atts['namecompanyfontsize'] ) . ';';
		}
		else {
			$namecompanyfontsize ="";
		}
		
		if( !empty( $atts['textfontsize'] ) or !empty( $atts['contentcolor'] ) ) {
			$namecompanytextStyle = ' style="' . $namecompanyfontsize . $contentcolor . '"';
		} else {
			$namecompanytextStyle = "";
		}
	
		if( !empty( $atts['image'] ) ) {
			$image = wp_get_attachment_image_src( esc_attr( $atts["image"] ), "full" );
			$image = '<div class="image"><img src="' . esc_url( $image[0] ) . '" alt="' . esc_html__( 'Customer','translogistic' ) . '" /></div>';
		}
		else {
			$image = "";
		}
				
		if( !empty( $atts['name'] ) ) {
			$name = '<span class="name">' .  esc_attr( $atts['name'] ) . '</span>';
		}
		else {
			$name = "";
		}
		
		if( !empty( $atts['company'] ) ) {
			$company = '<span class="company">' .  esc_attr( $atts['company'] ) . '</span>';
		}
		else {
			$company = "";
		}

		if( !empty( $atts['customclass'] ) ) {
			$customClass = ' ' . esc_attr( $atts['customclass'] );
		} else {
			$customClass = "";
		}
		
		$output .= '<div class="item' . $style . $customClass . '">';
			
			$output .= $image;
			
			if( !empty( $atts['name'] ) or !empty( $atts['company'] ) ) {
				$output .= '<div class="testimonial-modern-item-name"' . $namecompanytextStyle . '>' . $name . $company . '</div>';
			}
			
			if( !empty( $atts['contentmodern'] ) ) {
				$output .= '<div class="testimonial-modern-item-content"' . $textStyle . '><p>' . esc_attr( $atts['contentmodern'] ) . '</p></div>';
			}
				
		$output .= '</div>';

	return $output;
}
add_shortcode("testimonial_modern_item", "testimonial_modern_item_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Testimonial - Modern", 'translogistic' ),
		"base" => "testimonial_modern",
		"class" => "",
		"as_parent" => array('only' => 'testimonial_modern_item'),
		"js_view" => 'VcColumnView',
		"content_element" => true,
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/testimonial_modern.png',
		"description" =>esc_html__( 'Testimonial modern widget content.','translogistic' ),
		"params" => array(
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Column",'translogistic' ),
				"description" => esc_html__( "You can enter the column size. Example: 3, 4, 5. Default: 1", 'translogistic' ),
				"param_name" => "column",
				"value" => "",
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__( "Navigation",'translogistic' ),
				"description" => esc_html__( "You can select the navigation status.",'translogistic' ),
				"param_name" => "carousel",
				"value" => array(
					esc_html__( "True", 'translogistic' ) => "true",
					esc_html__( "False", 'translogistic' ) => "false",
				)
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__( "Dots Color",'translogistic' ),
				"description" => esc_html__( "You can select the dots color.",'translogistic' ),
				"param_name" => "dotscolor",
				"value" => array(
					esc_html__( "Default", 'translogistic' ) => "default",
					esc_html__( "White", 'translogistic' ) => "white",
					esc_html__( "None", 'translogistic' ) => "none"
				)
			)
		)
	) );
}

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Testimonial - Modern Item", 'translogistic' ),
		"base" => "testimonial_modern_item",
		"class" => "",
		"as_child" => array( 'only' => 'testimonial_modern' ),
		"content_element" => true,
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/testimonial_modern.png',
		"description" =>esc_html__( 'Testimonial modern item widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "attach_image",
				"class" => "",
				"heading" => esc_html__( "Image",'translogistic' ),
				"description" => esc_html__( "You can the upload your customer image. Suitably: 132x132", 'translogistic' ),
				"param_name" => "image",
				"value" => "",
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Style",'translogistic' ),
				"description" => esc_html__( "You can select the design style.",'translogistic' ),
				"param_name" => "style",
				"value" => array(
					esc_html__( "Default", 'translogistic' ) => "default",
					esc_html__( "Alternative", 'translogistic' ) => "alternative",
				)
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Name",'translogistic' ),
				"description" => esc_html__( "You can enter the customer name.", 'translogistic' ),
				"param_name" => "name",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Company",'translogistic' ),
				"description" => esc_html__( "You can enter the company name.", 'translogistic' ),
				"param_name" => "company",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Content",'translogistic' ),
				"description" => esc_html__( "You can enter the customer feedback.", 'translogistic' ),
				"param_name" => "contentmodern",
				"value" => ""
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Text Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "contentcolor",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Text Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "textfontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Name &amp; Company Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "namecompanyfontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Extra Class Name",'translogistic' ),
				"description" => esc_html__( "Please write your custom class.",'translogistic' ),
				"param_name" => "customclass",
				"value" => "",
			)
		)
	) );
}

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
	class WPBakeryShortCode_testimonial_modern extends WPBakeryShortCodesContainer {}
}
/*------------- TESTIMONIAL - MODERN END -------------*/

/*------------- TESTIMONIAL - BLOCK START -------------*/
function testimonial_block_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'dotscolor' => ''
		), $atts
	);
	
	$output = '';
		
		$random_id = rand( 0, 99999999 );
		
		$output .= '<div class="testimonial-block-widget ' . esc_attr( $atts['dotscolor'] ) . '">';
		
			$output .= '<div class="owl-carousel-testimonial-block-' . esc_attr( $random_id ) . ' owl-theme">';
				$output .= do_shortcode( $content );
			$output .= '</div>';
		$output .= '</div>';
		
		$output .= "<script>
							jQuery(document).ready(function($){
								$('.owl-carousel-testimonial-block-" . esc_attr( $random_id ) . "').owlCarousel({
									loop:true,
									nav:false,
									margin:65,
									dots:true,
									responsive:{
										0:{
											items:1,
										}
									}
								});
								
								testimonialblockowl = $('.owl-carousel-testimonial-block-" . esc_attr( $random_id ) . "').owlCarousel();
								$('.testimonial-block-widget-prev').click(function () {
									testimonialblockowl.trigger('prev.owl.carousel');
								});

								$('.testimonial-block-widget-next').click(function () {
									testimonialblockowl.trigger('next.owl.carousel');
								});
							});
						</script>";

	return $output;
}
add_shortcode("testimonial_block", "testimonial_block_shortcode");

function testimonial_block_item_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'col1name' => '',
			'col1company' => '',
			'col1contentblock' => '',
			'col2name' => '',
			'col2company' => '',
			'col2contentblock' => '',
			'col3name' => '',
			'col3company' => '',
			'col3contentblock' => '',
			'col4name' => '',
			'col4company' => '',
			'col4contentblock' => '',
			'contentcolor' => '',
			'namecolor' => '',
			'companycolor' => '',
			'designstyle' => '',
			'textfontsize' => '',
			'namecompanyfontsize' => ''
		), $atts
	);
	
	$output = '';
		
		if( !empty( $atts['contentcolor'] ) ) {
			$contentcolor = 'color:' . esc_attr( $atts['contentcolor'] ) . ';';
		}
		else {
			$contentcolor = "";
		}
		
		if( !empty( $atts['textfontsize'] ) ) {
			$textfontsize = 'font-size:' . esc_attr( $atts['textfontsize'] ) . ';';
		}
		else {
			$textfontsize ="";
		}
		
		if( !empty( $atts['textfontsize'] ) or !empty( $atts['contentcolor'] ) ) {
			$textStyle = ' style="' . $textfontsize . $contentcolor . '"';
		} else {
			$textStyle = "";
		}
		
		if( !empty( $atts['companycolor'] ) ) {
			$companycolor = ' style="color:' . esc_attr( $atts['companycolor'] ) . ';"';
		}
		else {
			$companycolor = "";
		}
		
		if( !empty( $atts['namecolor'] ) ) {
			$namecolor = ' style="color:' . esc_attr( $atts['namecolor'] ) . ';"';
		}
		else {
			$namecolor = "";
		}
		
		if( !empty( $atts['namecompanyfontsize'] ) ) {
			$namecompanyfontsize = 'font-size:' . esc_attr( $atts['namecompanyfontsize'] ) . ';';
		}
		else {
			$namecompanyfontsize ="";
		}
		
		if( !empty( $atts['textfontsize'] ) or !empty( $atts['contentcolor'] ) ) {
			$namecompanytextStyle = ' style="' . $namecompanyfontsize . '"';
		} else {
			$namecompanytextStyle = "";
		}

		if( !empty( $atts['col1name'] ) ) {
			$name1 = '<span class="name"'. $namecolor . '>' .  esc_attr( $atts['col1name'] ) . '</span>';
		}
		else {
			$name1 = "";
		}
		
		if( !empty( $atts['col1company'] ) ) {
			$company1 = '<span class="company"' . $companycolor . '>' .  esc_attr( $atts['col1company'] ) . '</span>';
		}
		else {
			$company1 = "";
		}

		if( !empty( $atts['col2name'] ) ) {
			$name2 = '<span class="name"'. $namecolor . '>' .  esc_attr( $atts['col2name'] ) . '</span>';
		}
		else {
			$name2 = "";
		}
		
		if( !empty( $atts['col2company'] ) ) {
			$company2 = '<span class="company"' . $companycolor . '>' .  esc_attr( $atts['col2company'] ) . '</span>';
		}
		else {
			$company2 = "";
		}

		if( !empty( $atts['col3name'] ) ) {
			$name3 = '<span class="name"'. $namecolor . '>' .  esc_attr( $atts['col3name'] ) . '</span>';
		}
		else {
			$name3 = "";
		}
		
		if( !empty( $atts['col3company'] ) ) {
			$company3 = '<span class="company"' . $companycolor . '>' .  esc_attr( $atts['col3company'] ) . '</span>';
		}
		else {
			$company3 = "";
		}

		if( !empty( $atts['col4name'] ) ) {
			$name4 = '<span class="name"'. $namecolor . '>' .  esc_attr( $atts['col4name'] ) . '</span>';
		}
		else {
			$name4 = "";
		}
		
		if( !empty( $atts['col4company'] ) ) {
			$company4 = '<span class="company"' . $companycolor . '>' .  esc_attr( $atts['col4company'] ) . '</span>';
		}
		else {
			$company4 = "";
		}

		if( !empty( $atts['designstyle'] ) ) {
			$designstyle = ' ' . esc_attr( $atts['designstyle'] );
		} else {
			$designstyle = "";
		}
		
		$output .= '<div class="item">';
		
			if( !empty( $atts['col1contentblock'] ) or !empty( $atts['col2contentblock'] ) ) {
				
				$output .= '<div class="testimonial-block-widget-content-area-columns' . $designstyle . '">';
		
					if( !empty( $atts['col1contentblock'] ) ) {
						
						$output .= '<div class="testimonial-block-widget-content-area animate anim-fadeIn animate-delay-0">';
					
							if( !empty( $atts['col1contentblock'] ) ) {
								$output .= '<div class="testimonial-block-item-content"' . $textStyle . '><p>' . esc_attr( $atts['col1contentblock'] ) . '</p></div>';
							}
							
							if( !empty( $atts['col1name'] ) or !empty( $atts['col1company'] ) ) {
								$output .= '<div class="testimonial-block-item-name"' . $namecompanytextStyle . '>' . $name1 . $company1 . '</div>';
							}
							
						$output .= '</div>';
						
					}
			
					if( !empty( $atts['col3contentblock'] ) ) {
						
						$output .= '<div class="testimonial-block-widget-content-area animate anim-fadeIn animate-delay-0-25">';

							if( !empty( $atts['col3contentblock'] ) ) {
								$output .= '<div class="testimonial-block-item-content"' . $textStyle . '><p>' . esc_attr( $atts['col3contentblock'] ) . '</p></div>';
							}
							
							if( !empty( $atts['col3name'] ) or !empty( $atts['col3company'] ) ) {
								$output .= '<div class="testimonial-block-item-name"' . $namecompanytextStyle . '>' . $name3 . $company3 . '</div>';
							}
						
						$output .= '</div>';
						
					}
				
				$output .= '</div>';
				
			}
			
			if( !empty( $atts['col2contentblock'] ) or !empty( $atts['col3contentblock'] ) ) {
				
				$output .= '<div class="testimonial-block-widget-content-area-columns' . $designstyle . '">';
		
					if( !empty( $atts['col2contentblock'] ) ) {
						
						$output .= '<div class="testimonial-block-widget-content-area animate anim-fadeIn animate-delay-0-50">';
						
							if( !empty( $atts['col2contentblock'] ) ) {
								$output .= '<div class="testimonial-block-item-content"' . $textStyle . '><p>' . esc_attr( $atts['col2contentblock'] ) . '</p></div>';
							}
							
							if( !empty( $atts['col2name'] ) or !empty( $atts['col2company'] ) ) {
								$output .= '<div class="testimonial-block-item-name"' . $namecompanytextStyle . '>' . $name2 . $company2 . '</div>';
							}
						
						$output .= '</div>';
					}
				
					if( !empty( $atts['col4contentblock'] ) ) {
						
						$output .= '<div class="testimonial-block-widget-content-area animate anim-fadeIn animate-delay-0-75">';
						
							if( !empty( $atts['col4contentblock'] ) ) {
								$output .= '<div class="testimonial-block-item-content"' . $textStyle . '><p>' . esc_attr( $atts['col4contentblock'] ) . '</p></div>';
							}
							
							if( !empty( $atts['col4name'] ) or !empty( $atts['col4company'] ) ) {
								$output .= '<div class="testimonial-block-item-name"' . $namecompanytextStyle . '>' . $name4 . $company4 . '</div>';
							}
							
						$output .= '</div>';
						
					}
					
				$output .= '</div>';
				
			}
				
		$output .= '</div>';

	return $output;
}
add_shortcode("testimonial_block_item", "testimonial_block_item_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Testimonial - Block", 'translogistic' ),
		"base" => "testimonial_block",
		"class" => "",
		"as_parent" => array('only' => 'testimonial_block_item'),
		"js_view" => 'VcColumnView',
		"content_element" => true,
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/testimonial-block.png',
		"description" =>esc_html__( 'Testimonial block widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__( "Dots Color",'translogistic' ),
				"description" => esc_html__( "You can select the dots color.",'translogistic' ),
				"param_name" => "dotscolor",
				"value" => array(
					esc_html__( "Default", 'translogistic' ) => "default",
					esc_html__( "White", 'translogistic' ) => "white"
				)
			)
		)
	) );
}

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Testimonial - Block Item", 'translogistic' ),
		"base" => "testimonial_block_item",
		"class" => "",
		"as_child" => array( 'only' => 'testimonial_block' ),
		"content_element" => true,
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/testimonial-block.png',
		"description" =>esc_html__( 'Testimonial block item widget content.','translogistic' ),
		"params" => array(
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Col 1 - Name",'translogistic' ),
				"description" => esc_html__( "You can enter the customer name.", 'translogistic' ),
				"param_name" => "col1name",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Col 1 - Company",'translogistic' ),
				"description" => esc_html__( "You can enter the company name.", 'translogistic' ),
				"param_name" => "col1company",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Col 1 - Text",'translogistic' ),
				"description" => esc_html__( "You can enter the customer feedback.", 'translogistic' ),
				"param_name" => "col1contentblock",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Col 2 - Name",'translogistic' ),
				"description" => esc_html__( "You can enter the customer name.", 'translogistic' ),
				"param_name" => "col2name",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Col 2 - Company",'translogistic' ),
				"description" => esc_html__( "You can enter the company name.", 'translogistic' ),
				"param_name" => "col2company",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Col 2 - Text",'translogistic' ),
				"description" => esc_html__( "You can enter the customer feedback.", 'translogistic' ),
				"param_name" => "col2contentblock",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Col 3 - Name",'translogistic' ),
				"description" => esc_html__( "You can enter the customer name.", 'translogistic' ),
				"param_name" => "col3name",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Col 3 - Company",'translogistic' ),
				"description" => esc_html__( "You can enter the company name.", 'translogistic' ),
				"param_name" => "col3company",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Col 3 - Text",'translogistic' ),
				"description" => esc_html__( "You can enter the customer feedback.", 'translogistic' ),
				"param_name" => "col3contentblock",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Col 4 - Name",'translogistic' ),
				"description" => esc_html__( "You can enter the customer name.", 'translogistic' ),
				"param_name" => "col4name",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Col 4 - Company",'translogistic' ),
				"description" => esc_html__( "You can enter the company name.", 'translogistic' ),
				"param_name" => "col4company",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Col 4 - Text",'translogistic' ),
				"description" => esc_html__( "You can enter the customer feedback.", 'translogistic' ),
				"param_name" => "col4contentblock",
				"value" => ""
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Text Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "contentcolor",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Name Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "namecolor",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Company Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "companycolor",
				"value" => "",
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Custom Content Design Color",'translogistic' ),
				"description" => esc_html__( "You can select the design style.",'translogistic' ),
				"param_name" => "designstyle",
				"value" => array(
					esc_html__( "Default", 'translogistic' ) => "default",
					esc_html__( "Theme One Color", 'translogistic' ) => "themeonecolor",
					esc_html__( "Theme Two Color", 'translogistic' ) => "themetwocolor",
					esc_html__( "Theme Three Color", 'translogistic' ) => "themethreecolor",
					esc_html__( "Theme Four Color", 'translogistic' ) => "themefourcolor"
				)
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Text Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "textfontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Name &amp; Company Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "namecompanyfontsize",
				"value" => "",
			)
		)
	) );
}

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
	class WPBakeryShortCode_testimonial_block extends WPBakeryShortCodesContainer {}
}
/*------------- TESTIMONIAL - BLOCK END -------------*/

/*------------- LIST BOX START -------------*/
function list_box_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'style' => '',
			'title' => '',
			'text' => '',
			'icon' => '',
			'iconcolor' => '',
			'titlecolor' => '',
			'textcolor' => '',
			'iconcolortype' => '',
			'titlefontsize' => '',
			'textfontsize' => '',
			'customclass' => ''
		), $atts
	);
	
	$output = '';
	
		if( !empty( $atts['textcolor'] ) ) {
			$text_color = 'color:' . esc_attr( $atts['textcolor'] ) . ';';
		}
		else {
			$text_color="";
		}
		
		if( !empty( $atts['textfontsize'] ) ) {
			$textfontsize = 'font-size:' . esc_attr( $atts['textfontsize'] ) . ';';
		}
		else {
			$textfontsize ="";
		}
		
		if( !empty( $atts['textfontsize'] ) or !empty( $atts['contentcolor'] ) ) {
			$textStyle = ' style="' . $textfontsize . $text_color . '"';
		} else {
			$textStyle = "";
		}
	
		if( !empty( $atts['titlecolor'] ) ) {
			$titlecolor = 'color:' . esc_attr( $atts['titlecolor'] ) . ';';
		}
		else {
			$titlecolor ="";
		}
		
		if( !empty( $atts['titlefontsize'] ) ) {
			$titlefontsize = 'font-size:' . esc_attr( $atts['titlefontsize'] ) . ';';
		}
		else {
			$titlefontsize ="";
		}
		
		if( !empty( $atts['titlefontsize'] ) or !empty( $atts['titlecolor'] ) ) {
			$titleStyle = ' style="' . $titlefontsize . $titlecolor . '"';
		} else {
			$titleStyle = "";
		}
	
		if( !empty( $atts['iconcolor'] ) ) {
			$iconcolor = ' style="color:' . esc_attr( $atts['iconcolor'] ) . ';"';
		}
		else {
			$iconcolor = "";
		}
	
		if( !empty( $atts['iconcolor'] ) ) {
			$iconcolor_border = ' style="border-color:' . esc_attr( $atts['iconcolor'] ) . ';"';
		}
		else {
			$iconcolor_border ="";
		}
		
		$output .= '<div class="list-box ' . esc_attr( $atts['customclass'] ). ' ' . esc_attr( $atts['style'] ) . '">';
		
			if( !empty( $atts['icon'] ) ) {
				$output .= '<div class="list-box-icon ' . esc_attr( $atts['iconcolortype'] ) . '"' . $iconcolor_border . '>';
					$output .= '<i class="fa fa-' . esc_attr( $atts['icon'] ) . '" ' .  $iconcolor . '></i>';
				$output .= '</div>';
			}
			
			if( !empty( $atts['title'] ) ) {
				$output .= '<h3 ' . $titleStyle . '>' . esc_attr( $atts['title'] ) . '</h3>';
			}
			
			if( !empty( $atts['text'] ) ) {
				$output .= '<p' . $textStyle . '>' . esc_attr( $atts['text'] ) . '</p>';
			}
			
		$output .= '</div>';
			
	return $output;
}
add_shortcode("list_box", "list_box_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "List Box", 'translogistic' ),
		"base" => "list_box",
		"class" => "",
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/list-box.png',
		"description" =>esc_html__( 'List box widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Style",'translogistic' ),
				"description" => esc_html__( "You can select the design style.",'translogistic' ),
				"param_name" => "style",
				"value" => array(
					esc_html__( "Square", 'translogistic' ) => "square",
					esc_html__( "Cross", 'translogistic' ) => "cross"
				)
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Title",'translogistic' ),
				"description" => esc_html__( "You can enter the title.", 'translogistic' ),
				"param_name" => "title",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Text",'translogistic' ),
				"description" => esc_html__( "You can enter the text.", 'translogistic' ),
				"param_name" => "text",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Icon",'translogistic' ),
				"description" => esc_html__( "You can enter the icon name. List of the icons is available in the documentation file. Example: edge, automobile, bel-o.", 'translogistic' ),
				"param_name" => "icon",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Icon Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "iconcolor",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Title Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "titlecolor",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Text Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "textcolor",
				"value" => "",
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Custom Icon Color",'translogistic' ),
				"description" => esc_html__( "Select the white icon true/false",'translogistic' ),
				"param_name" => "iconcolortype",
				"value" => array(
					esc_html__( "Default or Custom Color", 'translogistic' ) => "default",
					esc_html__( "White", 'translogistic' ) => "white"
				)
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Title Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "titlefontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Text Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "textfontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Extra Class Name",'translogistic' ),
				"description" => esc_html__( "Please write your custom class.",'translogistic' ),
				"param_name" => "customclass",
				"value" => "",
			)
		)
	) );
}
/*------------- LIST BOX END -------------*/

/*------------- ICON LIST START -------------*/
function icon_list_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'style' => '',
			'title' => '',
			'text' => '',
			'icon' => '',
			'number' => '',
			'iconsize' => '',
			'iconcolor' => '',
			'titlecolor' => '',
			'textcolor' => '',
			'titlefontsize' => '',
			'textfontsize' => '',
			'borderwidth' => '',
			'customclass' => ''
		), $atts
	);
	
	$output = '';
	
		if( !empty( $atts['titlecolor'] ) ) {
			$titlecolor = 'color:' . esc_attr( $atts['titlecolor'] ) . ';';
		}
		else {
			$titlecolor ="";
		}
		
		if( !empty( $atts['titlefontsize'] ) ) {
			$titlefontsize = 'font-size:' . esc_attr( $atts['titlefontsize'] ) . ';';
		}
		else {
			$titlefontsize ="";
		}
		
		if( !empty( $atts['titlefontsize'] ) or !empty( $atts['titlecolor'] ) ) {
			$titleStyle = ' style="' . $titlefontsize . $titlecolor . '"';
		} else {
			$titleStyle = "";
		}
	
		if( !empty( $atts['textcolor'] ) ) {
			$text_color = 'color:' . esc_attr( $atts['textcolor'] ) . ';';
		}
		else {
			$text_color="";
		}
		
		if( !empty( $atts['textfontsize'] ) ) {
			$textfontsize = 'font-size:' . esc_attr( $atts['textfontsize'] ) . ';';
		}
		else {
			$textfontsize ="";
		}
		
		if( !empty( $atts['textfontsize'] ) or !empty( $atts['textcolor'] ) ) {
			$textStyle = ' style="' . $textfontsize . $text_color . '"';
		} else {
			$textStyle = "";
		}
	
		if( !empty( $atts['borderwidth'] ) ) {
			$borderwidth = 'border-width:' . esc_attr( $atts['borderwidth'] ) . ';';
		}
		else {
			$borderwidth ="";
		}
	
		if( !empty( $atts['iconcolor'] ) ) {
			$iconColor = ' border-color:' . esc_attr( $atts['iconcolor'] ) . ';color:' . esc_attr( $atts['iconcolor'] ) . ';';
		}
		else {
			$iconColor ="";
		}
	
		if( !empty( $atts['iconcolor'] ) or !empty( $atts['borderwidth'] ) ) {
			$iconStyle = ' style="' . $iconColor . $borderwidth . '"';
		}
		else {
			$iconStyle ="";
		}
		
		$output .= '<div class="icon-list ' . esc_attr( $atts['customclass'] ). ' ' . esc_attr( $atts['iconsize'] ). '">';
		
			if( $atts['style'] == "number" ) {
				if( !empty( $atts['number'] ) ) {
					$output .= '<div class="icon-list-number"' . $iconStyle . '><span>' . esc_attr( $atts['number'] ) . '</span></div>';
				}
			} else {
				if( !empty( $atts['icon'] ) ) {
					$output .= '<div class="icon-list-icon"' . $iconStyle . '>';
						$output .= '<i class="fa fa-' . esc_attr( $atts['icon'] ) . '"></i>';
					$output .= '</div>';
				}
			}
			
			if( !empty( $atts['title'] ) ) {
				$output .= '<h3' . $titleStyle . '>' . esc_attr( $atts['title'] ) . '</h3>';
			}
			
			if( !empty( $atts['text'] ) ) {
				$output .= '<p ' . $textStyle . '>' . esc_attr( $atts['text'] ) . '</p>';
			}
			
		$output .= '</div>';
			
	return $output;
}
add_shortcode("icon_list", "icon_list_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Icon List", 'translogistic' ),
		"base" => "icon_list",
		"class" => "",
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/icon-list.png',
		"description" =>esc_html__( 'Icon list widget content.','translogistic' ),
		"params" => array(
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Style",'translogistic' ),
				"description" => esc_html__( "You can select the design style.",'translogistic' ),
				"param_name" => "style",
				"value" => array(
					esc_html__( "Icon", 'translogistic' ) => "icon",
					esc_html__( "Number", 'translogistic' ) => "number"
				)
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Title",'translogistic' ),
				"description" => esc_html__( "You can enter the title.", 'translogistic' ),
				"param_name" => "title",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Text",'translogistic' ),
				"description" => esc_html__( "You can enter the text.", 'translogistic' ),
				"param_name" => "text",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Icon",'translogistic' ),
				"description" => esc_html__( "You can enter the icon name. List of the icons is available in the documentation file. Example: edge, automobile, bel-o.", 'translogistic' ),
				"param_name" => "icon",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Number",'translogistic' ),
				"description" => esc_html__( "You can enter the number.", 'translogistic' ),
				"param_name" => "number",
				"value" => "",
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_html__( "Icon Size",'translogistic' ),
				"description" => esc_html__( "You can select the icon size.",'translogistic' ),
				"param_name" => "iconsize",
				"value" => array(
					esc_html__( "Normal", 'translogistic' ) => "normal",
					esc_html__( "Big", 'translogistic' ) => "big"
				)
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Icon Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "iconcolor",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Title Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "titlecolor",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Text Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "textcolor",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Title Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "titlefontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Text Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "textfontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Border Width",'translogistic' ),
				"description" => esc_html__( "You can enter the icon border width size. Example: 2px.",'translogistic' ),
				"param_name" => "borderwidth",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Extra Class Name",'translogistic' ),
				"description" => esc_html__( "Please write your custom class.",'translogistic' ),
				"param_name" => "customclass",
				"value" => "",
			)
		)
	) );
}
/*------------- ICON LIST END -------------*/

/*------------- TRANSLOGISTIC BUTTON START -------------*/
function translogistic_button_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'style' => '',
			'buttontitle' => '',
			'buttonlink' => '',
			'icon' => '',
			'bgcolor' => '',
			'titlecolor' => '',
			'titlefontsize' => '',
			'buttonborderwidth' => '',
			'buttonpadding' => '',
			'customclass' => ''
		), $atts
	);
	
	$output = '';

		if( !empty( $atts['titlecolor'] ) ) {
			$titlecolor = 'color:' . esc_attr( $atts['titlecolor'] ) . ';border-color:' . esc_attr( $atts['titlecolor'] ) . ';';
		}
		else {
			$titlecolor ="";
		}

		if( !empty( $atts['bgcolor'] ) ) {
			$bgcolor = 'background:' . esc_attr( $atts['bgcolor'] ) . ';';
		}
		else {
			$bgcolor ="";
		}
	
		if( !empty( $atts['titlefontsize'] ) ) {
			$titlefontsize = 'font-size:' . esc_attr( $atts['titlefontsize'] ) . ';';
		}
		else {
			$titlefontsize ="";
		}
	
		if( !empty( $atts['buttonpadding'] ) ) {
			$buttonpadding = 'padding:' . esc_attr( $atts['buttonpadding'] ) . ';';
		}
		else {
			$buttonpadding ="";
		}
	
		if( !empty( $atts['buttonborderwidth'] ) ) {
			$buttonborderwidth = 'border-width:' . esc_attr( $atts['buttonborderwidth'] ) . ';';
		}
		else {
			$buttonborderwidth ="";
		}
	
		if( !empty( $atts['titlecolor'] ) or !empty( $atts['titlefontsize'] ) or !empty( $atts['buttonpadding'] ) or !empty( $atts['buttonborderwidth'] ) ) {
			$buttonStyle = ' style="' . $titlecolor . $bgcolor . $titlefontsize . $buttonpadding . $buttonborderwidth . '"';
		}
		else {
			$buttonStyle ="";
		}

		if( !empty( $atts['customclass'] ) ) {
			$customClass = ' ' . esc_attr( $atts['customclass'] );
		} else {
			$customClass = "";
		}

		if( !empty( $atts['icon'] ) ) {
			$icon = '<i class="fa fa-' . esc_attr( $atts['icon'] ) . '"></i>';
		} else {
			$icon = "";
		}

		if( !empty( $atts['style'] ) ) {
			$style = ' ' . esc_attr( $atts['style'] ) ;
		} else {
			$style = "";
		}
		
		if( !empty( $atts['buttontitle'] ) ) {
			$output .= '<div class="translogistic-button' . $customClass . $style . '">';
				$output .= '<a href="' . esc_url( $atts['buttonlink'] ) . '" title="' . esc_attr( $atts['buttontitle'] ) . '"' . $buttonStyle . '>' . $icon . esc_attr( $atts['buttontitle'] ) . '</a>';
			$output .= '</div>';
		}

	return $output;
}
add_shortcode("translogistic_button", "translogistic_button_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Translogistic Button", 'translogistic' ),
		"base" => "translogistic_button",
		"class" => "",
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/translogistic-button.png',
		"description" =>esc_html__( 'Translogistic button widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Style",'translogistic' ),
				"description" => esc_html__( "You can select the design style. Example: White - Green => White normal status / Green hover status.",'translogistic' ),
				"param_name" => "style",
				"value" => array(
					esc_html__( "Green - White", 'translogistic' ) => "greenwhite",
					esc_html__( "White - Green", 'translogistic' ) => "whitegreen",
					esc_html__( "Blue - White", 'translogistic' ) => "bluewhite",
					esc_html__( "Green - Transparent", 'translogistic' ) => "greentransparent",
					esc_html__( "Blue - Transparent", 'translogistic' ) => "bluetransparent",
					esc_html__( "White - Blue", 'translogistic' ) => "whiteblue",
					esc_html__( "Theme One Color", 'translogistic' ) => "themeonecolor",
					esc_html__( "Theme Two Color", 'translogistic' ) => "themetwocolor",
					esc_html__( "Theme Three Color", 'translogistic' ) => "themethreecolor",
					esc_html__( "Theme Four Color", 'translogistic' ) => "themefourcolor",
					esc_html__( "Custom", 'translogistic' ) => "custom"
				)
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Button Title",'translogistic' ),
				"description" => esc_html__( "You can enter the button title.", 'translogistic' ),
				"param_name" => "buttontitle",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Button Link",'translogistic' ),
				"description" => esc_html__( "You can enter the button link.", 'translogistic' ),
				"param_name" => "buttonlink",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Icon",'translogistic' ),
				"description" => esc_html__( "You can enter the icon name. List of the icons is available in the documentation file. Example: edge, automobile, bel-o.", 'translogistic' ),
				"param_name" => "icon",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Button Background Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "bgcolor",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Button Title & Border Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "titlecolor",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Button Title Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "titlefontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Button Border Width",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 2px.",'translogistic' ),
				"param_name" => "buttonborderwidth",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Button Padding",'translogistic' ),
				"description" => esc_html__( "You can enter the button padding value. Example: 20px 20px.",'translogistic' ),
				"param_name" => "buttonpadding",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Extra Class Name",'translogistic' ),
				"description" => esc_html__( "Please write your custom class.",'translogistic' ),
				"param_name" => "customclass",
				"value" => "",
			)
		)
	) );
}
/*------------- TRANSLOGISTIC BUTTON END -------------*/

/*------------- TRANSLOGISTIC ICON START -------------*/
function translogistic_icon_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'icon' => '',
			'iconcolor' => '',
			'iconsize' => '',
			'buttonmargin' => '',
			'customclass' => ''
		), $atts
	);
	
	$output = '';

		if( !empty( $atts['iconcolor'] ) ) {
			$iconcolor = 'color:' . esc_attr( $atts['iconcolor'] ) . ';';
		}
		else {
			$iconcolor ="";
		}

		if( !empty( $atts['iconsize'] ) ) {
			$iconsize = 'font-size:' . esc_attr( $atts['iconsize'] ) . ';';
		}
		else {
			$iconsize ="";
		}

		if( !empty( $atts['customclass'] ) ) {
			$customClass = ' ' . esc_attr( $atts['customclass'] );
		} else {
			$customClass = "";
		}
	
		if( !empty( $atts['buttonmargin'] ) ) {
			$buttonmargin = ' margin:' . esc_attr( $atts['buttonmargin'] ) . ';';
		}
		else {
			$buttonmargin ="";
		}
	
		if( !empty( $atts['iconcolor'] ) or !empty( $atts['iconsize'] ) ) {
			$iconStyle = ' style="' . $iconcolor . $iconsize . $buttonmargin . '"';
		}
		else {
			$iconStyle ="";
		}
			
		if( !empty( $atts['icon'] ) ) {
			$output .= '<i class="fa fa-' . esc_attr( $atts['icon'] ) . $customClass . '"' . $iconStyle . '></i>';
		}

	return $output;
}
add_shortcode("translogistic_icon", "translogistic_icon_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Translogistic Icon", 'translogistic' ),
		"base" => "translogistic_icon",
		"class" => "",
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/translogistic-icon.png',
		"description" =>esc_html__( 'Translogistic icon widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Icon",'translogistic' ),
				"description" => esc_html__( "You can enter the icon name. List of the icons is available in the documentation file. Example: edge, automobile, bel-o.", 'translogistic' ),
				"param_name" => "icon",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Icon Color",'translogistic' ),
				"description" => esc_html__( "Please choose icon color.",'translogistic' ),
				"param_name" => "iconcolor",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Icon Size",'translogistic' ),
				"description" => esc_html__( "Please icon size. Example: 15px.",'translogistic' ),
				"param_name" => "iconsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Button Margin",'translogistic' ),
				"description" => esc_html__( "You can enter the button margin value. Example: 20px 20px.",'translogistic' ),
				"param_name" => "buttonmargin",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Extra Class Name",'translogistic' ),
				"description" => esc_html__( "Please write your custom class.",'translogistic' ),
				"param_name" => "customclass",
				"value" => "",
			)
		)
	) );
}
/*------------- TRANSLOGISTIC ICON END -------------*/

/*------------- CLIENTS START -------------*/
function translogistic_clients_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'customclass' => ''
		), $atts
	);
	
	$output = '';

		if( !empty( $atts['customclass'] ) ) {
			$customClass = ' ' . esc_attr( $atts['customclass'] );
		} else {
			$customClass = "";
		}
		
		$random_id = rand( 0, 99999999 );
		
		$output .= '<div class="clients-widget' . esc_attr( $customClass ) . '">';
			$output .= '<div class="owl-carousel-clients-' . esc_attr( $random_id ) . '">';
				$output .= do_shortcode( $content );
			$output .= '</div>';
		$output .= '</div>';
		
		$output .= "<script>
							jQuery(document).ready(function($){
								$('.owl-carousel-clients-" . esc_attr( $random_id ) . "').owlCarousel({
									loop:true,
									nav:false,
									margin:50,
									dots:false,
									autoWidth:true,
									responsive:{
										0:{
											items:2,
										},
										768:{
											items:6,
										},
									}
								});
							});
						</script>";
			

	return $output;
}
add_shortcode("translogistic_clients", "translogistic_clients_shortcode");

function client_logo_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'clientimage' => '',
			'clientlink' => ''
		), $atts
	);
	
	$output = '';
	
	if( !empty( $atts['clientimage'] ) ) {
		$image = wp_get_attachment_image_src($atts["clientimage"], "full");
		$output .= '<div class="item">';
			$output .= '<a href="' . esc_url( $atts['clientlink'] ) . '" title="' . esc_html__( 'Client','translogistic' ) . '" ><img src="' . esc_url( $image[0] ) . '" alt="' . esc_html__( 'Client','translogistic' ) . '" /></a>';
		$output .= '</div>';
	}

	return $output;
}
add_shortcode("client_logo", "client_logo_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Clients", 'translogistic' ),
		"base" => "translogistic_clients",
		"class" => "",
		"as_parent" => array('only' => 'client_logo'),
		"js_view" => 'VcColumnView',
		"content_element" => true,
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/client_logo.png',
		"description" =>esc_html__( 'Clients widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Extra Class Name",'translogistic' ),
				"description" => esc_html__( "Please write your custom class.",'translogistic' ),
				"param_name" => "customclass",
				"value" => "",
			)
		)
	) );
}

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Client Logo", 'translogistic' ),
		"base" => "client_logo",
		"class" => "",
		"as_child" => array( 'only' => 'translogistic_clients' ),
		"content_element" => true,
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/client_logo.png',
		"description" =>esc_html__( 'Client logo widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "attach_image",
				"class" => "",
				"heading" => esc_html__( "Client Image",'translogistic' ),
				"description" => esc_html__( "You can the upload your client logo.", 'translogistic' ),
				"param_name" => "clientimage",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Client Link",'translogistic' ),
				"description" => esc_html__( "You can enter the logo link.", 'translogistic' ),
				"param_name" => "clientlink",
				"value" => "",
			),
		)
	) );
}

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
	class WPBakeryShortCode_translogistic_clients extends WPBakeryShortCodesContainer {}
}
/*------------- CLIENTS END -------------*/

/*------------- FEATURE BOX START -------------*/
function feature_box_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'style' => '',
			'designstyle' => '',
			'featureimage' => '',
			'imageposition' => '',
			'textspositon' => '',
			'title' => '',
			'text' => '',
			'texttwo' => '',
			'icon' => '',
			'buttontitle' => '',
			'buttonlink' => '',
			'textcolor' => '',
			'buttoncolor' => '',
			'bgcolor' => '',
			'iconcolor' => '',
			'titlefontsize' => '',
			'textfontsize' => '',
			'buttonfontsize' => '',
			'customclass' => '',
		), $atts
	);
	
	$output = '';
	
		if( $atts['textspositon'] == "left" ) {
			$align = ' class="text-left"';
		} elseif( $atts['textspositon'] == "right" ) {
			$align = ' class="text-right"';
		} else {
			$align = '';		
		}

		if( !empty( $atts['bgcolor'] ) ) {
			$bgcolor = 'background:' . esc_attr( $atts['bgcolor'] ) . ';';
		}
		else {
			$bgcolor ="";
		}
	
		if( !empty( $atts['bgcolor'] ) ) {
			$contentStyle = ' style="' . $bgcolor . '"';
		}
		else {
			$contentStyle ="";
		}

		if( !empty( $atts['imageposition'] ) ) {
			$imageposition = ' ' . esc_attr( $atts['imageposition'] );
		} else {
			$imageposition = "";
		}

		if( !empty( $atts['iconcolor'] ) ) {
			$iconcolor = 'color:' . esc_attr( $atts['iconcolor'] ) . ';border-color:' . esc_attr( $atts['iconcolor'] ) . ';';
		}
		else {
			$iconcolor ="";
		}
	
		if( !empty( $atts['iconcolor'] ) ) {
			$iconStyle = ' style="' . $iconcolor . '"';
		}
		else {
			$iconStyle ="";
		}
		
		if( !empty( $atts['icon'] ) ) {
			$icon = '<div class="feature-box-icon"' . $iconStyle . '>';
				$icon .= '<i class="fa fa-' . esc_attr( $atts['icon'] ) . '"></i>';
			$icon .= '</div>';
		} else {
			$icon = "";
		}

		if( !empty( $atts['textcolor'] ) ) {
			$textcolor = 'color:' . esc_attr( $atts['textcolor'] ) . ';';
		}
		else {
			$textcolor ="";
		}

		if( !empty( $atts['titlefontsize'] ) ) {
			$titlefontsize = 'font-size:' . esc_attr( $atts['titlefontsize'] ) . ';';
		}
		else {
			$titlefontsize ="";
		}
	
		if( !empty( $atts['textcolor'] ) or !empty( $atts['titlefontsize'] ) ) {
			$titleStyle = ' style="' . $textcolor . $titlefontsize . '"';
		}
		else {
			$titleStyle ="";
		}

		if( !empty( $atts['textfontsize'] ) ) {
			$textfontsize = 'font-size:' . esc_attr( $atts['textfontsize'] ) . ';';
		}
		else {
			$textfontsize ="";
		}
	
		if( !empty( $atts['textcolor'] ) or !empty( $atts['textfontsize'] ) ) {
			$textStyle = ' style="' . $textcolor . $textfontsize . '"';
		}
		else {
			$textStyle ="";
		}

		if( !empty( $atts['buttonfontsize'] ) ) {
			$buttonfontsize = 'font-size:' . esc_attr( $atts['buttonfontsize'] ) . ';';
		}
		else {
			$buttonfontsize ="";
		}

		if( !empty( $atts['buttoncolor'] ) ) {
			$buttoncolor = 'color:' . esc_attr( $atts['buttoncolor'] ) . ';border-color:' . esc_attr( $atts['buttoncolor'] ) . ';';
		}
		else {
			$buttoncolor ="";
		}
	
		if( !empty( $atts['buttonfontsize'] ) or !empty( $atts['buttoncolor'] ) ) {
			$buttonStyle = ' style="' . $buttonfontsize . $buttoncolor . '"';
		}
		else {
			$buttonStyle ="";
		}

		if( !empty( $atts['featureimage'] ) ) {
			$image = wp_get_attachment_image_src( esc_attr( $atts["featureimage"] ), "full" );
			$image = '<div class="image' . $imageposition . '" style="background-image:url(' . esc_url( $image[0] ) . ');"></div>';
		}
		else {
			$image = "";
		}

		if( !empty( $atts['title'] ) ) {
			$title = '<h4' . $titleStyle . $align . '>' . esc_attr( $atts['title'] ) . '</h4>';
		} else {
			$title = "";
		}

		if( !empty( $atts['text'] ) ) {
			$text = '<p' . $textStyle . $align . '>' . esc_attr( $atts['text'] ) . '</p>';
		} else {
			$text = "";
		}

		if( !empty( $atts['texttwo'] ) ) {
			$texttwo = '<p' . $textStyle . $align . '>' . esc_attr( $atts['texttwo'] ) . '</p>';
		} else {
			$texttwo = "";
		}
				
		if( !empty( $atts['buttontitle'] ) ) {
			$button = '<div class="button">';
				$button .= '<a href="' . esc_url( $atts['buttonlink'] ) . '" title="' . esc_attr( $atts['buttontitle'] ) . '"' . $buttonStyle . '>' . esc_attr( $atts['buttontitle'] ) . '</a>';
			$button .= '</div>';
		} else {
			$button = "";
		}

		if( !empty( $atts['style'] ) ) {
			$style = ' ' . esc_attr( $atts['style'] );
		} else {
			$style = "";
		}

		if( !empty( $atts['designstyle'] ) ) {
			$designstyle = ' ' . esc_attr( $atts['designstyle'] );
		} else {
			$designstyle = "";
		}

		if( !empty( $atts['customclass'] ) ) {
			$customClass = ' ' . esc_attr( $atts['customclass'] );
		} else {
			$customClass = "";
		}
			
		if( !empty( $atts['featureimage'] ) or !empty( $atts['title'] ) or !empty( $atts['text'] ) ) {
			$output .= '<div class="feature-box' . $customClass . $style . $designstyle . '">';
				if( $atts['style'] == ""  and $atts['imageposition'] == "" ) {
					$output .= $image;
				}
				if( $atts['style'] == "horizontal"  and $atts['imageposition'] == "left" ) {
					$output .= $image;
				}
				$output .= '<div class="feature-box-content' . $imageposition . '"'. $contentStyle . '>';
					$output .= $icon;
					$output .= $title;
					$output .= $text;
					$output .= $texttwo;
					$output .= $button;
				$output .= '</div>';
				if( $atts['style'] == ""  and $atts['imageposition'] == "bottom") {
					$output .= $image;
				}
				if( $atts['style'] == "horizontal"  and $atts['imageposition'] == "right" ) {
					$output .= $image;
				}
			$output .= '</div>';
		}

	return $output;
}
add_shortcode("feature_box", "feature_box_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Feature Box", 'translogistic' ),
		"base" => "feature_box",
		"class" => "",
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/feature-box.png',
		"description" =>esc_html__( 'Feature box widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Style",'translogistic' ),
				"description" => esc_html__( "You can select the style.",'translogistic' ),
				"param_name" => "style",
				"value" => array(
					esc_html__( "Vertical", 'translogistic' ) => "vertical",
					esc_html__( "Horizontal", 'translogistic' ) => "horizontal"
				)
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Design Style",'translogistic' ),
				"description" => esc_html__( "You can select the design color style.",'translogistic' ),
				"param_name" => "designstyle",
				"value" => array(
					esc_html__( "Theme One Color", 'translogistic' ) => "themeonecolor",
					esc_html__( "Theme Two Color", 'translogistic' ) => "themetwocolor",
					esc_html__( "Theme Three Color", 'translogistic' ) => "themethreecolor",
					esc_html__( "Theme Four Color", 'translogistic' ) => "themefourcolor",
					esc_html__( "Custom", 'translogistic' ) => "custom"
				)
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Image Position",'translogistic' ),
				"description" => esc_html__( "You can select the image position. Vertical: Top - Bottom, Horizontal: Left - Right.",'translogistic' ),
				"param_name" => "imageposition",
				"value" => array(
					esc_html__( "Top", 'translogistic' ) => "top",
					esc_html__( "Bottom", 'translogistic' ) => "bottom",
					esc_html__( "Right", 'translogistic' ) => "right",
					esc_html__( "Left", 'translogistic' ) => "left",
				)
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Text Position",'translogistic' ),
				"description" => esc_html__( "You can select the text position.",'translogistic' ),
				"param_name" => "textspositon",
				"value" => array(
					esc_html__( "Center", 'translogistic' ) => "center",
					esc_html__( "Right", 'translogistic' ) => "right",
					esc_html__( "Left", 'translogistic' ) => "left",
				)
			),
			array(
				"type" => "attach_image",
				"class" => "",
				"heading" => esc_html__( "Image",'translogistic' ),
				"description" => esc_html__( "You can upload the your feature image.", 'translogistic' ),
				"param_name" => "featureimage",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Title",'translogistic' ),
				"description" => esc_html__( "You can enter the title.", 'translogistic' ),
				"param_name" => "title",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Text Row One",'translogistic' ),
				"description" => esc_html__( "You can enter the text of one row.", 'translogistic' ),
				"param_name" => "text",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Text Row Two",'translogistic' ),
				"description" => esc_html__( "You can enter the text of two row.", 'translogistic' ),
				"param_name" => "texttwo",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Icon",'translogistic' ),
				"description" => esc_html__( "You can enter the icon name. List of the icons is available in the documentation file. Example: edge, automobile, bel-o.", 'translogistic' ),
				"param_name" => "icon",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Button Title",'translogistic' ),
				"description" => esc_html__( "You can enter the button title.", 'translogistic' ),
				"param_name" => "buttontitle",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Button Link",'translogistic' ),
				"description" => esc_html__( "You can enter the button link.", 'translogistic' ),
				"param_name" => "buttonlink",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Background Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "bgcolor",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Text Color",'translogistic' ),
				"description" => esc_html__( "Please choose text color.",'translogistic' ),
				"param_name" => "textcolor",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Button Color",'translogistic' ),
				"description" => esc_html__( "Please choose text color.",'translogistic' ),
				"param_name" => "buttoncolor",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Icon Color",'translogistic' ),
				"description" => esc_html__( "Please choose icon color.",'translogistic' ),
				"param_name" => "iconcolor",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Title Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "titlefontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Text Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "textfontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Button Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "buttonfontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Extra Class Name",'translogistic' ),
				"description" => esc_html__( "Please write your custom class.",'translogistic' ),
				"param_name" => "customclass",
				"value" => "",
			)
		)
	) );
}
/*------------- FEATURE BOX END -------------*/

/*------------- NUMBERMATOR START -------------*/
function numbermator_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'title' => '',
			'number' => '',
			'numbercolor' => '',
			'textcolor' => '',
			'numberfontsize' => '',
			'textfontsize' => ''
		), $atts
	);
	
	$output = '';
	
		if( !empty( $atts['title'] ) or !empty( $atts['number'] ) ) {
			
			if( !empty( $atts['number'] ) ) {
				$number = $atts['number'];
			}
			else
			{
				$number = "0";
			}

			if( !empty( $atts['numberfontsize'] ) ) {
				$numberfontsize = 'font-size:' . esc_attr( $atts['numberfontsize'] ) . ';';
			}
			else {
				$numberfontsize ="";
			}

			if( !empty( $atts['numbercolor'] ) ) {
				$numbercolor = 'color:' . esc_attr( $atts['numbercolor'] ) . ';';
			}
			else {
				$numbercolor ="";
			}

			if( !empty( $atts['textcolor'] ) ) {
				$textcolor = 'color:' . esc_attr( $atts['textcolor'] ) . ';';
			}
			else {
				$textcolor ="";
			}
		
			if( !empty( $atts['numberfontsize'] ) or !empty( $atts['numbercolor'] ) ) {
				$titleStyle = ' style="' . $numberfontsize . $numbercolor . '"';
			}
			else {
				$titleStyle ="";
			}

			if( !empty( $atts['textfontsize'] ) ) {
				$textfontsize = 'font-size:' . esc_attr( $atts['textfontsize'] ) . ';';
			}
			else {
				$textfontsize ="";
			}
		
			if( !empty( $atts['textfontsize'] ) or !empty( $atts['textcolor'] ) ) {
				$textStyle = ' style="' . $textfontsize . $textcolor . '"';
			}
			else {
				$textStyle ="";
			}
	
			$random_id = rand( 0, 99999999 );
			$random_id = $number * $random_id;
			
			$output .= '<div class="numbermator">';
			
				if( !empty( $atts['number'] ) ) {
					$output .= '<h3' . $titleStyle . '><div class="counter scounter' . esc_attr( $random_id ) . '" data-last="' . esc_attr( $number )  . '">' . esc_attr( $number ) . '</div></h3>';
				}
				
				if( !empty( $atts['title'] ) ) {
					$allowed_html = array ( 'br' => array() );
					$output .= '<h4' . $textStyle . '>' . wp_kses( $atts['title'] , $allowed_html ) . '</h4>';
				}
				
			$output .= '</div>';
			$output .= '<script type="text/javascript">
								jQuery(document).ready(function($){
									$(".scounter' . esc_attr( $random_id ) . '").counterUp({
										delay: 10,
										time: 1200
									});
								});
							</script>';
		}
	

	return $output;
}
add_shortcode("numbermator", "numbermator_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Numbermator", 'translogistic' ),
		"base" => "numbermator",
		"class" => "",
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/numbermator.png',
		"description" =>esc_html__( 'Numbermator widget content.','translogistic' ),
		"params" => array(
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Title",'translogistic' ),
				"description" => esc_html__( "You can enter the title.", 'translogistic' ),
				"param_name" => "title",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Number",'translogistic' ),
				"description" => esc_html__( "You can enter the percent.", 'translogistic' ),
				"param_name" => "number",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Number Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "numbercolor",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Text Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "textcolor",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Number Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "numberfontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Text Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "textfontsize",
				"value" => "",
			)
		)
	) );
}
/*------------- NUMBERMATOR END -------------*/

/*------------- PROGRESS BAR START -------------*/
function progressbar_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'title' => '',
			'percent' => '',
			'progressbarbg' => '',
			'percentcolor' => '',
			'titlecolor' => '',
			'numberfontsize' => '',
			'textfontsize' => '',
		), $atts
	);
	
	$output = '';

		if( !empty( $atts['textfontsize'] ) ) {
			$textfontsize = 'font-size:' . esc_attr( $atts['textfontsize'] ) . ';';
		}
		else {
			$textfontsize ="";
		}

		if( !empty( $atts['titlecolor'] ) ) {
			$titlecolor = 'color:' . esc_attr( $atts['titlecolor'] ) . ';';
		}
		else {
			$titlecolor ="";
		}
	
		if( !empty( $atts['titlecolor'] ) or !empty( $atts['textfontsize'] ) ) {
			$titleStyle = ' style="' . $titlecolor . $textfontsize . '"';
		}
		else {
			$titleStyle ="";
		}

		if( !empty( $atts['numberfontsize'] ) ) {
			$numberfontsize = 'font-size:' . esc_attr( $atts['numberfontsize'] ) . ';';
		}
		else {
			$numberfontsize ="";
		}

		if( !empty( $atts['percentcolor'] ) ) {
			$percentcolor = 'color:' . esc_attr( $atts['percentcolor'] ) . ';';
		}
		else {
			$percentcolor ="";
		}
	
		if( !empty( $atts['percentcolor'] ) or !empty( $atts['numberfontsize'] ) ) {
			$numberStyle = ' style="' . $percentcolor . $numberfontsize . '"';
		}
		else {
			$numberStyle ="";
		}

		if( !empty( $atts['percentcolor'] ) ) {
			$progressbarborder = 'border-color:' . esc_attr( $atts['percentcolor'] ) . ';';
		}
		else {
			$progressbarborder ="";
		}

		if( !empty( $atts['progressbarbg'] ) ) {
			$progressbarbg = 'background:' . esc_attr( $atts['progressbarbg'] ) . ';';
		}
		else {
			$progressbarbg ="";
		}
	
		if( !empty( $atts['progressbarbg'] ) ) {
			$progressStyle = ' style="' . $progressbarbg . $progressbarborder . '"';
		}
		else {
			$progressStyle ="";
		}

		if( !empty( $atts['percentcolor'] ) ) {
			$progressLoad = 'background:' . esc_attr( $atts['percentcolor'] ) . ';';
		}
		else {
			$progressLoad ="";
		}
	
		if( !empty( $atts['percentcolor'] ) ) {
			$progressLoadStyle = ' style="' . $progressLoad . '"';
		}
		else {
			$progressLoadStyle ="";
		}

		if( !empty( $atts['title'] ) ) {
			$title = '<div class="title"' . $titleStyle . '>' .  $atts['title'] . '</div>';
		}
		else {
			$title = "";
		}
		
		if( !empty( $atts['percent'] ) ) {
			$percent = $atts['percent'];
		}
		else
		{
			$percent = "0";
		}
		
		$random_id = rand( 0, 99999999 );
		$random_id = $percent * $random_id;
		
		if( !empty( $atts['percent'] ) ) {
			$output .= '<div class="translogistic-progress-bar">';
				$output .= '<div class="percent"' . $numberStyle . '><span>%</span><label class="counter-' . $random_id . '">' . $percent . '</label></div>';
				$output .= '<div class="progress-content">';
					$output .= $title;
					$output .= '<div id="progress-wrap-' . $random_id . '" class="progress-wrap progress" data-progress-percent="' . $percent . '"' . $progressStyle . '>
										<div id="progress-bar-' . $random_id . '" class="progress-bar progress"' . $progressLoadStyle . '></div>
									</div>';
				$output .= '</div>';
			$output .= '</div>';
		
		$output .= "<script>							
							jQuery(document).ready(function($){						
								var progressBar = function() {
									var percent = $('#progress-wrap-" . $random_id . "').attr('data-progress-percent');
									$('#progress-bar-" . $random_id . "').animate({
										width: percent + '%'
									}, 500);
								}
								$('#progress-bar-" . $random_id . "').waypoint(progressBar, { offset: '100%', triggerOnce: true });

							});
	
						</script>";
		}
			

	return $output;
}
add_shortcode("progressbar", "progressbar_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Progress Bar", 'translogistic' ),
		"base" => "progressbar",
		"class" => "",
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/progress-bar.png',
		"description" =>esc_html__( 'Progress bar widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Title",'translogistic' ),
				"description" => esc_html__( "You can enter the title.", 'translogistic' ),
				"param_name" => "title",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Percent",'translogistic' ),
				"description" => esc_html__( "You can enter the percent. Select from 1 to 100.", 'translogistic' ),
				"param_name" => "percent",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Progress Bar Background",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "progressbarbg",
				"value" => ""
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Progress Bar & Percent Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "percentcolor",
				"value" => ""
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Title Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "titlecolor",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Title Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "textfontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Percent Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "numberfontsize",
				"value" => "",
			)
		)
	) );
}
/*------------- PROGRESS BAR END -------------*/

/*------------- CHRONOLOGY START  -------------*/
function chronologyyears_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'bgcolor' => '',
			'textcolor' => '',
			'yearfontsize' => '',
			'designstyle' => '',
			'customclass' => ''
		), $atts
	);
	
	$output = '';

		if( !empty( $atts['customclass'] ) ) {
			$customClass = ' ' . esc_attr( $atts['customclass'] );
		} else {
			$customClass = "";
		}

		if( !empty( $atts['yearfontsize'] ) ) {
			$yearfontsize = 'font-size:' . esc_attr( $atts['yearfontsize'] ) . ';';
		}
		else {
			$yearfontsize ="";
		}

		if( !empty( $atts['bgcolor'] ) ) {
			$bgcolor = 'background:' . esc_attr( $atts['bgcolor'] ) . ';';
		}
		else {
			$bgcolor ="";
		}

		if( !empty( $atts['textcolor'] ) ) {
			$textcolor = 'color:' . esc_attr( $atts['textcolor'] ) . ';';
		}
		else {
			$textcolor ="";
		}
	
		if( !empty( $atts['yearfontsize'] ) or !empty( $atts['bgcolor'] ) ) {
			$yearStyle = ' style="' . $yearfontsize . $bgcolor . $textcolor . '"';
		}
		else {
			$yearStyle ="";
		}

		if( !empty( $atts['designstyle'] ) ) {
			$designstyle = ' ' . esc_attr( $atts['designstyle'] );
		} else {
			$designstyle = "";
		}

		$output .= '<div class="chronologyyears-content' . $customClass . $designstyle . '"' . $yearStyle . '>';
			$output .= do_shortcode( $content );
		$output .= '</div>';
	
	return $output;
}
add_shortcode("chronologyyears", "chronologyyears_shortcode");

function chronologyyears_item_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'year' => ''
		), $atts
	);
	
	$output = '';
	
		$output .= '<div class="year-block">' . esc_attr( $atts["year"] ) . '</div><div class="separate"></div>';

	return $output;
}
add_shortcode("chronologyyears_item", "chronologyyears_item_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Chronology Year Item", 'translogistic' ),
		"base" => "chronologyyears_item",
		"class" => "",
		"as_child" => array('only' => 'chronologyyears'),
		"content_element" => true,
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/chronology-years.png',
		"description" =>esc_html__( 'Chronology years item widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Year",'translogistic' ),
				"description" => esc_html__( "You can enter the year.",'translogistic' ),
				"param_name" => "year",
				"value" => "",
			)
		)
	) );
}

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Chronology Year List", 'translogistic' ),
		"base" => "chronologyyears",
		"class" => "",
		"as_parent" => array( 'only' => 'chronologyyears_item' ),
		"content_element" => true,
		"js_view" => 'VcColumnView',
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/chronology-years.png',
		"description" =>esc_html__( 'Chronology years widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Style",'translogistic' ),
				"description" => esc_html__( "You can select the design style.",'translogistic' ),
				"param_name" => "designstyle",
				"value" => array(
					esc_html__( "Custom Style", 'translogistic' ) => "custom",
					esc_html__( "Theme One Color", 'translogistic' ) => "themeonecolor",
					esc_html__( "Theme Two Color", 'translogistic' ) => "themetwocolor",
					esc_html__( "Theme Three Color", 'translogistic' ) => "themethreecolor",
					esc_html__( "Theme Four Color", 'translogistic' ) => "themefourcolor"
				)
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Background Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "bgcolor",
				"value" => ""
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Text Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "textcolor",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Year Font Size",'translogistic' ),
				"description" => esc_html__( "Please chronology year font size. Example: 15px.",'translogistic' ),
				"param_name" => "yearfontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Extra Class Name",'translogistic' ),
				"description" => esc_html__( "Please write your custom class.",'translogistic' ),
				"param_name" => "customclass",
				"value" => "",
			)
		)
	) );
}

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
	class WPBakeryShortCode_chronologyyears extends WPBakeryShortCodesContainer {}
}

function chronologyitem_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'year' => '',
			'chronologytext' => '',
			'percent' => '',
			'textcolor' => '',
			'bgcolor' => '',
			'textfontsize' => '',
			'customclass' => ''
		), $atts
	);
	
	$output = '';

		if( !empty( $atts['textfontsize'] ) ) {
			$textfontsize = 'font-size:' . esc_attr( $atts['textfontsize'] ) . ';';
		}
		else {
			$textfontsize ="";
		}

		if( !empty( $atts['textcolor'] ) ) {
			$textcolor = 'color:' . esc_attr( $atts['textcolor'] ) . ';';
		}
		else {
			$textcolor ="";
		}
	
		if( !empty( $atts['textcolor'] ) or !empty( $atts['textfontsize'] ) ) {
			$titleStyle = ' style="' . $textcolor . $textfontsize . '"';
		}
		else {
			$titleStyle ="";
		}

		if( !empty( $atts['textcolor'] ) ) {
			$progressbarborder = 'border-color:' . esc_attr( $atts['textcolor'] ) . ';';
		}
		else {
			$progressbarborder ="";
		}

		if( !empty( $atts['bgcolor'] ) ) {
			$bgcolor = 'background:' . esc_attr( $atts['bgcolor'] ) . ';';
		}
		else {
			$bgcolor ="";
		}
	
		if( !empty( $atts['textcolor'] ) ) {
			$progressStyle = ' style="' . $bgcolor . $progressbarborder . '"';
		}
		else {
			$progressStyle ="";
		}

		if( !empty( $atts['textcolor'] ) ) {
			$textcolor = 'background:' . esc_attr( $atts['textcolor'] ) . ';';
		}
		else {
			$textcolor ="";
		}
	
		if( !empty( $atts['textcolor'] ) ) {
			$progressLoadStyle = ' style="' . $textcolor . '"';
		}
		else {
			$progressLoadStyle ="";
		}

		if( !empty( $atts['chronologytext'] ) ) {
			$chronologytext =  '<span class="text">' . $atts['chronologytext'] . '</span>';
		}
		else {
			$chronologytext = "";
		}

		if( !empty( $atts['year'] ) ) {
			$year =  '<span class="year">' . $atts['year'] . '</span>';
		}
		else {
			$year = "";
		}

		if( !empty( $atts['year'] ) or !empty( $atts['chronologytext'] ) ) {
			$chronologyitemtext =  '<div class="chronologyitem-text"' . $titleStyle . '>' . $year . $chronologytext . '</div>';
		}
		else {
			$chronologyitemtext = "";
		}

		if( !empty( $atts['customclass'] ) ) {
			$customClass = ' ' . esc_attr( $atts['customclass'] );
		} else {
			$customClass = "";
		}
		
		if( !empty( $atts['percent'] ) ) {
			$percent = $atts['percent'];
		}
		else
		{
			$percent = "0";
		}
		
		$random_id = rand( 0, 99999999 );
		$random_id = $percent * $random_id;
		
		if( !empty( $atts['percent'] ) ) {
			$output .= '<div class="chronologyitem-bar">';
				$output .= '<div class="progress-content' . $customClass . '">';
					$output .= '<div id="progress-wrap-' . $random_id . '" class="progress-wrap progress" data-progress-percent="' . $percent . '"' . $progressStyle . '>
										<div id="progress-bar-' . $random_id . '" class="progress-bar progress"' . $progressLoadStyle . '></div>
									</div>';
				$output .= '</div>';
				$output .= $chronologyitemtext;
			$output .= '</div>';
		
		$output .= "<script>							
							jQuery(document).ready(function($){						
								var progressBar = function() {
									var percent = $('#progress-wrap-" . $random_id . "').attr('data-progress-percent');
									$('#progress-bar-" . $random_id . "').animate({
										width: percent + '%'
									}, 500);
								}
								$('#progress-bar-" . $random_id . "').waypoint(progressBar, { offset: '100%', triggerOnce: false });
								
							});
	
						</script>";
		}
			

	return $output;
}
add_shortcode("chronologyitem", "chronologyitem_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Chronology Item", 'translogistic' ),
		"base" => "chronologyitem",
		"class" => "",
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/chronology-years.png',
		"description" =>esc_html__( 'Chronology item widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Year",'translogistic' ),
				"description" => esc_html__( "You can enter the year.",'translogistic' ),
				"param_name" => "year",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Chronology Text",'translogistic' ),
				"description" => esc_html__( "You can enter the text.",'translogistic' ),
				"param_name" => "chronologytext",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Percent",'translogistic' ),
				"description" => esc_html__( "You can enter the percent. Select from 1 to 100.", 'translogistic' ),
				"param_name" => "percent",
				"value" => ""
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Text Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "textcolor",
				"value" => ""
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Progress Background Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "bgcolor",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Text Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "textfontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Extra Class Name",'translogistic' ),
				"description" => esc_html__( "Please write your custom class.",'translogistic' ),
				"param_name" => "customclass",
				"value" => "",
			)
		)
	) );
}
/*------------- CHRONOLOGY END  -------------*/

/*------------- CARGO TRACKING FORM START -------------*/
function cargo_tracking_form_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'title' => '',
			'text' => '',
			'bgstyle' => '',
			'designstyle' => '',
			'titlefontsize' => '',
			'textfontsize' => '',
			'titlecolor' => '',
			'textcolor' => '',
			'customclass' => ''
		), $atts
	);
	
	$output = '';
	
	$cargo_tracking_form_page = esc_attr( get_theme_mod( 'cargo_query_results_page' ) );
	$cargo_tracking_form_page_url = get_permalink ( $cargo_tracking_form_page );

	if( !empty( $atts['customclass'] ) ) {
		$customClass = ' ' . esc_attr( $atts['customclass'] );
	} else {
		$customClass = "";
	}

	if( !empty( $atts['designstyle'] ) ) {
		$designstyle = ' ' . esc_attr( $atts['designstyle'] );
	} else {
		$designstyle = "";
	}

	if( !empty( $atts['bgstyle'] ) ) {
		$bgstyle = esc_attr( $atts['bgstyle'] );
	} else {
		$bgstyle = "";
	}	
	
	if( !empty( $atts['titlecolor'] ) ) {
		$text_color = 'color:' . esc_attr( $atts['titlecolor'] ) . ';';
	}
	else {
		$text_color ="";
	}

	if( !empty( $atts['titlefontsize'] ) ) {
		$titlefontsize = 'font-size:' . esc_attr( $atts['titlefontsize'] ) . ';';
	}
	else {
		$titlefontsize ="";
	}
	
	if( !empty( $atts['titlecolor'] ) or !empty( $atts['titlefontsize'] ) ) {
		$titleStyle = ' style="' . $text_color . $titlefontsize . '"';
	} else {
		$titleStyle = "";
	}
	
	if( !empty( $atts['textcolor'] ) ) {
		$textcolor = 'color:' . esc_attr( $atts['textcolor'] ) . ';';
	}
	else {
		$textcolor ="";
	}

	if( !empty( $atts['textfontsize'] ) ) {
		$textfontsize = 'font-size:' . esc_attr( $atts['textfontsize'] ) . ';';
	}
	else {
		$textfontsize ="";
	}
	
	if( !empty( $atts['textcolor'] ) or !empty( $atts['textfontsize'] ) ) {
		$textStyle = ' style="' . $textcolor . $textfontsize . '"';
	} else {
		$textStyle = "";
	}
	
	$output .= '<div class="cargo-tracking-form' . esc_attr( $designstyle ) . esc_attr( $customClass ) . '">';
		$output .= '<form action="' . esc_url( $cargo_tracking_form_page_url ) . '" method="post" enctype="multipart/form-data">';
			if( $bgstyle == "white" or  $bgstyle == "dark" ) {
				$output .= '<div class="cargo-tracking-form-wrapper ' . $bgstyle . '">';
			}
			if( !empty( $atts['title'] ) ) {
				$output .= '<h3' . $titleStyle . '>' . esc_attr( $atts['title'] ) . '</h3>';
			}
			if( !empty( $atts['text'] ) ) {
				$allowed_html = array ( 'i' => array(), 'strong' => array() );
				$output .= '<p' . $textStyle . '>' . wp_kses( $atts['text'] , $allowed_html ) . '</p>';
			}
			$output .= '<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-info-circle"></i></span>
								<input type="text" name="cargo_tracking_code_text" placeholder="' . esc_html__( "Cargo Tracking Number",'translogistic' ) . '">
								<input type="submit" value="' . esc_html__( "Track",'translogistic' ) . '">
							  </div>';
			if( $bgstyle == "white" or  $bgstyle == "dark" ) {
				$output .= '</div>';
			}
		$output .= '</form>';
	$output .= '</div>';

	return $output;
}
add_shortcode("cargo_tracking_form", "cargo_tracking_form_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Cargo Tracking Form", 'translogistic' ),
		"base" => "cargo_tracking_form",
		"class" => "",
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/cargo-tracking-form.png',
		"description" =>esc_html__( 'Cargo tracking form widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Title",'translogistic' ),
				"description" => esc_html__( "You can enter the title.",'translogistic' ),
				"param_name" => "title",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Text",'translogistic' ),
				"description" => esc_html__( "You can enter the text.", 'translogistic' ),
				"param_name" => "text",
				"value" => "",
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Background",'translogistic' ),
				"description" => esc_html__( "You can select the background style.",'translogistic' ),
				"param_name" => "bgstyle",
				"value" => array(
					esc_html__( "None", 'translogistic' ) => "none",
					esc_html__( "White", 'translogistic' ) => "white",
					esc_html__( "Dark", 'translogistic' ) => "dark"
				)
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Color Style",'translogistic' ),
				"description" => esc_html__( "You can select the design style.",'translogistic' ),
				"param_name" => "designstyle",
				"value" => array(
					esc_html__( "Default", 'translogistic' ) => "default",
					esc_html__( "White", 'translogistic' ) => "white",
					esc_html__( "Slider Version v1", 'translogistic' ) => "slideversionv1",
					esc_html__( "Slider Version v2", 'translogistic' ) => "slideversionv2",
					esc_html__( "Slider Version v3", 'translogistic' ) => "slideversionv3",
					esc_html__( "Query Page Version", 'translogistic' ) => "querypageversion",
					esc_html__( "Theme One Color", 'translogistic' ) => "themeonecolor",
					esc_html__( "Theme Two Color", 'translogistic' ) => "themetwocolor",
					esc_html__( "Theme Three Color", 'translogistic' ) => "themethreecolor",
					esc_html__( "Theme Four Color", 'translogistic' ) => "themefourcolor"
				)
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Title Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "titlefontsize",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Text Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "textfontsize",
				"value" => "",
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Title Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "titlecolor",
				"value" => ""
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Text Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "textcolor",
				"value" => ""
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Extra Class Name",'translogistic' ),
				"description" => esc_html__( "Please write your custom class.",'translogistic' ),
				"param_name" => "customclass",
				"value" => "",
			)
		)
	) );
}

function cargo_tracking_form_query_listing_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'designstyle' => ''
		), $atts
	);
	
	$output = '';

		if( isset( $_POST['cargo_tracking_code_text'] ) ) {
			$cargo_tracking_code_post = esc_js( esc_sql( balanceTags( htmlspecialchars( esc_html( $_POST["cargo_tracking_code_text"] ) ) ) ) );
		
			if( !empty( $cargo_tracking_code_post ) ) {
			
				$output .= '<div class="cargo-tracking-list-form">';
				
					$args_latest_posts = array(
						'posts_per_page' => 1,
						'post_status' => 'publish',
						'ignore_sticky_posts'    => true,
						'post_type' => 'translogistic_cargo',
						'meta_query'=> array(
						array(
							'key' => 'cargo_tracking_code',
							'compare' => '=',
							'value' => $cargo_tracking_code_post,
						)),
					); 
					$wp_query = new WP_Query($args_latest_posts);
					while ( $wp_query->have_posts() ) :
					$wp_query->the_post();
					
						$cargo_tracking_code = get_post_meta( get_the_ID(), "cargo_tracking_code", true );

						$cargo_tracking_shipper_name = get_post_meta( get_the_ID(), "cargo_tracking_shipper_name", true );
						$cargo_tracking_shipper_tel = get_post_meta( get_the_ID(), "cargo_tracking_shipper_tel", true );
						$cargo_tracking_shipper_email = get_post_meta( get_the_ID(), "cargo_tracking_shipper_email", true );
						$cargo_tracking_shipper_address = get_post_meta( get_the_ID(), "cargo_tracking_shipper_address", true );
						$cargo_tracking_shipping_from = get_post_meta( get_the_ID(), "cargo_tracking_shipping_from", true );
						if( empty( $cargo_tracking_shipping_from ) ) {
							$cargo_tracking_shipping_from = "-";
						}

						$cargo_tracking_receiver_name = get_post_meta( get_the_ID(), "cargo_tracking_receiver_name", true );
						$cargo_tracking_receiver_tel = get_post_meta( get_the_ID(), "cargo_tracking_receiver_tel", true );
						$cargo_tracking_receiver_email = get_post_meta( get_the_ID(), "cargo_tracking_receiver_email", true );
						$cargo_tracking_receiver_address = get_post_meta( get_the_ID(), "cargo_tracking_receiver_address", true );
						$cargo_tracking_shipping_delivery_place = get_post_meta( get_the_ID(), "cargo_tracking_shipping_delivery_place", true );
						if( empty( $cargo_tracking_shipping_delivery_place ) ) {
							$cargo_tracking_shipping_delivery_place = "-";
						}


						$cargo_tracking_shipping_release_date = get_post_meta( get_the_ID(), "cargo_tracking_shipping_release_date", true );
						if( empty( $cargo_tracking_shipping_release_date ) ) {
							$cargo_tracking_shipping_release_date = "-";
						}
						$cargo_tracking_shipping_release_hour = get_post_meta( get_the_ID(), "cargo_tracking_shipping_release_hour", true );
						if( empty( $cargo_tracking_shipping_release_hour ) ) {
							$cargo_tracking_shipping_release_hour = "";
						} else {
							$cargo_tracking_shipping_release_hour = " " . $cargo_tracking_shipping_release_hour;							
						}
						$cargo_tracking_shipping_delivery_date = get_post_meta( get_the_ID(), "cargo_tracking_shipping_delivery_date", true );
						if( empty( $cargo_tracking_shipping_delivery_date ) ) {
							$cargo_tracking_shipping_delivery_date = "-";
						}
						$cargo_tracking_shipping_delivery_hour = get_post_meta( get_the_ID(), "cargo_tracking_shipping_delivery_hour", true );
						if( empty( $cargo_tracking_shipping_delivery_hour ) ) {
							$cargo_tracking_shipping_delivery_hour = "";
						} else {
							$cargo_tracking_shipping_delivery_hour = " " . $cargo_tracking_shipping_delivery_hour;							
						}

						$cargo_tracking_pcs = get_post_meta( get_the_ID(), "cargo_tracking_pcs", true );
						if( empty( $cargo_tracking_pcs ) ) {
							$cargo_tracking_pcs = "-";
						}
						$cargo_tracking_weight = get_post_meta( get_the_ID(), "cargo_tracking_weight", true );
						if( empty( $cargo_tracking_weight ) ) {
							$cargo_tracking_weight = "-";
						}
						$cargo_tracking_height = get_post_meta( get_the_ID(), "cargo_tracking_height", true );
						if( empty( $cargo_tracking_height ) ) {
							$cargo_tracking_height = "-";
						}
						$cargo_tracking_shipping_status = get_post_meta( get_the_ID(), "cargo_tracking_shipping_status", true );
						if( empty( $cargo_tracking_shipping_status ) ) {
							$cargo_tracking_shipping_status = "-";
						}
						$cargo_tracking_product = get_post_meta( get_the_ID(), "cargo_tracking_product", true );
						if( empty( $cargo_tracking_product ) ) {
							$cargo_tracking_product = "-";
						}
						$cargo_tracking_carrier = get_post_meta( get_the_ID(), "cargo_tracking_carrier", true );
						if( empty( $cargo_tracking_carrier ) ) {
							$cargo_tracking_carrier = "-";
						}
						$cargo_tracking_freight = get_post_meta( get_the_ID(), "cargo_tracking_freight", true );
						if( empty( $cargo_tracking_freight ) ) {
							$cargo_tracking_freight = "-";
						}
						$cargo_tracking_cc = get_post_meta( get_the_ID(), "cargo_tracking_cc", true );
						if( empty( $cargo_tracking_cc ) ) {
							$cargo_tracking_cc = "-";
						}
						$barcode_image_url = get_post_meta( get_the_ID(), "barcode_image_url", true );
						if( !empty( $barcode_image_url ) ) {
							$barcode_image_url = '<img src="' . esc_url( $barcode_image_url ) . '" height="100" width="100" alt="' . esc_html( 'Barcode', 'translogistic' ) . '">';
						} else {
							$barcode_image_url = "";
						}

						$cargo_tracking_additional_note = get_post_meta( get_the_ID(), "cargo_tracking_additional_note", true );
						if( empty( $cargo_tracking_additional_note ) ) {
							$cargo_tracking_additional_note = "-";
						}

						if( !empty( $atts['designstyle'] ) ) {
							$designstyle = ' ' . esc_attr( $atts['designstyle'] );
						} else {
							$designstyle = "";
						}
						
								
						if( !empty( $cargo_tracking_shipper_name ) or !empty( $cargo_tracking_shipper_tel ) or !empty( $cargo_tracking_shipper_email ) or !empty( $cargo_tracking_shipper_address ) or !empty( $cargo_tracking_receiver_name ) or !empty( $cargo_tracking_receiver_tel ) or !empty( $cargo_tracking_receiver_email ) or !empty( $cargo_tracking_receiver_address ) or !empty( $barcode_image_url ) or !empty( $cargo_tracking_carrier ) ) {
					
							$output .= '<div class="cargo-tracking-list-form-header' . $designstyle . ' three-column hidden-xs">';
								$output .= '<div class="cargo-tracking-list-form-col">' . esc_html__( "Shipper",'translogistic' ) . '</div>';
								$output .= '<div class="cargo-tracking-list-form-col">' . esc_html__( "Receiver",'translogistic' ) . '</div>';
								$output .= '<div class="cargo-tracking-list-form-col">' . esc_html__( "Barcode",'translogistic' ) . '</div>';
							$output .= '</div>';

							$output .= '<div class="cargo-tracking-list-form-body two-column">';
								if( !empty( $cargo_tracking_shipper_name ) or !empty( $cargo_tracking_shipper_tel ) or !empty( $cargo_tracking_shipper_email ) or !empty( $cargo_tracking_shipper_address ) ) {
									$output .= '<div class="cargo-tracking-list-form-col"><strong class="mobile-text">' . esc_html__( 'Shipper', 'translogistic' ) . '</strong>';
										if( !empty( $cargo_tracking_shipper_name ) ) {
											$output .= $cargo_tracking_shipper_name. '<br>';
										}
										if( !empty( $cargo_tracking_shipper_tel ) ) {
											$output .= $cargo_tracking_shipper_tel . '<br>';
										}
										if( !empty( $cargo_tracking_shipper_email ) ) {
											$output .= $cargo_tracking_shipper_email . '<br>';
										}
										if( !empty( $cargo_tracking_shipper_address ) ) {
											$output .= $cargo_tracking_shipper_address . '<br>';
										}
									$output .= '</div>';
								}
								if( !empty( $cargo_tracking_receiver_name ) or !empty( $cargo_tracking_receiver_tel ) or !empty( $cargo_tracking_receiver_email ) or !empty( $cargo_tracking_receiver_address ) ) {
									$output .= '<div class="cargo-tracking-list-form-col"><strong class="mobile-text">' . esc_html__( 'Receiver', 'translogistic' ) . '</strong>';
										if( !empty( $cargo_tracking_receiver_name ) ) {
											$output .= $cargo_tracking_receiver_name. '<br>';
										}
										if( !empty( $cargo_tracking_receiver_tel ) ) {
											$output .= $cargo_tracking_receiver_tel . '<br>';
										}
										if( !empty( $cargo_tracking_receiver_email ) ) {
											$output .= $cargo_tracking_receiver_email . '<br>';
										}
										if( !empty( $cargo_tracking_receiver_address ) ) {
											$output .= $cargo_tracking_receiver_address . '<br>';
										}
									$output .= '</div>';
								}
								if( !empty( $barcode_image_url ) ) {
									$output .= '<div class="cargo-tracking-list-form-col">' . $barcode_image_url . '</div>';
								}
							$output .= '</div>';
						}
					
						$output .= '<div class="cargo-tracking-list-form-header' . $designstyle . ' hidden-xs">';
							$output .= '<div class="cargo-tracking-list-form-col">' . esc_html__( "Origin",'translogistic' ) . '</div>';
							$output .= '<div class="cargo-tracking-list-form-col">' . esc_html__( "Cargo Release Date",'translogistic' ) . '</div>';
							$output .= '<div class="cargo-tracking-list-form-col">' . esc_html__( "Destination",'translogistic' ) . '</div>';
							$output .= '<div class="cargo-tracking-list-form-col">' . esc_html__( "Cargo Delivery Date",'translogistic' ) . '</div>';
						$output .= '</div>';

						$output .= '<div class="cargo-tracking-list-form-body">';
							$output .= '<div class="cargo-tracking-list-form-col"><strong class="mobile-text">' . esc_html__( 'Origin', 'translogistic' ) . '</strong>' . esc_attr( $cargo_tracking_shipping_from ) . '</div>';
							$output .= '<div class="cargo-tracking-list-form-col"><strong class="mobile-text">' . esc_html__( 'Cargo Release Date', 'translogistic' ) . '</strong>' . esc_attr( $cargo_tracking_shipping_release_date ) . esc_attr( $cargo_tracking_shipping_release_hour ) . '</div>';
							$output .= '<div class="cargo-tracking-list-form-col"><strong class="mobile-text">' . esc_html__( 'Destination', 'translogistic' ) . '</strong>' . esc_attr( $cargo_tracking_shipping_delivery_place ) . '</div>';
							$output .= '<div class="cargo-tracking-list-form-col"><strong class="mobile-text">' . esc_html__( 'Cargo Delivery Date', 'translogistic' ) . '</strong>' . esc_attr( $cargo_tracking_shipping_delivery_date ) . esc_attr( $cargo_tracking_shipping_delivery_hour ) . '</div>';
						$output .= '</div>';
					
						$output .= '<div class="cargo-tracking-list-form-header' . $designstyle . ' hidden-xs">';
							$output .= '<div class="cargo-tracking-list-form-col">' . esc_html__( "Qty",'translogistic' ) . '</div>';
							$output .= '<div class="cargo-tracking-list-form-col">' . esc_html__( "Weight",'translogistic' ) . '</div>';
							$output .= '<div class="cargo-tracking-list-form-col">' . esc_html__( "Height",'translogistic' ) . '</div>';
							$output .= '<div class="cargo-tracking-list-form-col">' . esc_html__( "Shipping Status",'translogistic' ) . '</div>';
						$output .= '</div>';

						$output .= '<div class="cargo-tracking-list-form-body">';
							$output .= '<div class="cargo-tracking-list-form-col"><strong class="mobile-text">' . esc_html__( 'Qty', 'translogistic' ) . '</strong>' . esc_attr( $cargo_tracking_pcs ) . '</div>';
							$output .= '<div class="cargo-tracking-list-form-col"><strong class="mobile-text">' . esc_html__( 'Weight', 'translogistic' ) . '</strong>' . esc_attr( $cargo_tracking_weight ) . '</div>';
							$output .= '<div class="cargo-tracking-list-form-col"><strong class="mobile-text">' . esc_html__( 'Height', 'translogistic' ) . '</strong>' . esc_attr( $cargo_tracking_height ) . '</div>';
							$output .= '<div class="cargo-tracking-list-form-col"><strong class="mobile-text">' . esc_html__( 'Shipping Status', 'translogistic' ) . '</strong>' . esc_attr( $cargo_tracking_shipping_status ) . '</div>';
						$output .= '</div>';
					
						$output .= '<div class="cargo-tracking-list-form-header' . $designstyle . ' hidden-xs">';
							$output .= '<div class="cargo-tracking-list-form-col">' . esc_html__( "Carrier",'translogistic' ) . '</div>';
							$output .= '<div class="cargo-tracking-list-form-col">' . esc_html__( "Product(s)",'translogistic' ) . '</div>';
							$output .= '<div class="cargo-tracking-list-form-col">' . esc_html__( "Total Freight",'translogistic' ) . '</div>';
							$output .= '<div class="cargo-tracking-list-form-col">' . esc_html__( "Payment Mode",'translogistic' ) . '</div>';
						$output .= '</div>';

						$output .= '<div class="cargo-tracking-list-form-body">';
							$output .= '<div class="cargo-tracking-list-form-col"><strong class="mobile-text">' . esc_html__( 'Carrier', 'translogistic' ) . '</strong>' . esc_attr( $cargo_tracking_carrier ) . '</div>';
							$output .= '<div class="cargo-tracking-list-form-col"><strong class="mobile-text">' . esc_html__( 'Product(s)', 'translogistic' ) . '</strong>' . esc_attr( $cargo_tracking_product ) . '</div>';
							$output .= '<div class="cargo-tracking-list-form-col"><strong class="mobile-text">' . esc_html__( 'Total Freight', 'translogistic' ) . '</strong>' . esc_attr( $cargo_tracking_freight ) . '</div>';
							$output .= '<div class="cargo-tracking-list-form-col"><strong class="mobile-text">' . esc_html__( 'Payment Mode', 'translogistic' ) . '</strong>' . esc_attr( $cargo_tracking_cc ) . '</div>';
						$output .= '</div>';
					
						$output .= '<div class="cargo-tracking-list-form-header' . $designstyle . ' one-column hidden-xs">';
							$output .= '<div class="cargo-tracking-list-form-col">' . esc_html__( "Additional Note",'translogistic' ) . '</div>';
						$output .= '</div>';

						$output .= '<div class="cargo-tracking-list-form-body one-column">';
							$output .= '<div class="cargo-tracking-list-form-col"><strong class="mobile-text">' . esc_html__( 'Additional Note', 'translogistic' ) . '</strong>' . esc_attr( $cargo_tracking_additional_note ) . '</div>';
						$output .= '</div>';
						
					endwhile;
					wp_reset_postdata();
				
				$output .= '</div>';
			
			} else {
				$output .= esc_html__( "Enter the cargo tracking code.", 'translogistic' );
			}
		
		}
		else {
			$cargo_tracking_code_post = "";
		}

	return $output;
}
add_shortcode("cargo_tracking_form_query_listing", "cargo_tracking_form_query_listing_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Cargo Tracking Form - Query Listing", 'translogistic' ),
		"base" => "cargo_tracking_form_query_listing",
		"class" => "",
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/cargo-tracking-form.png',
		"description" =>esc_html__( 'Cargo tracking form query listing widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Style",'translogistic' ),
				"description" => esc_html__( "You can select the design style.",'translogistic' ),
				"param_name" => "designstyle",
				"value" => array(
					esc_html__( "Theme One Color", 'translogistic' ) => "themeonecolor",
					esc_html__( "Theme Two Color", 'translogistic' ) => "themetwocolor",
					esc_html__( "Theme Three Color", 'translogistic' ) => "themethreecolor",
					esc_html__( "Theme Four Color", 'translogistic' ) => "themefourcolor"
				)
			),
		)
	) );
}
/*------------- CARGO TRACKING FORM END -------------*/

/*------------- CONTACT INFORMATION START -------------*/
function contact_information_shortcode( $atts, $content = null ) {
	$atts = shortcode_atts(
		array(
			'address' => '',
			'phone' => '',
			'mobilephone' => '',
			'fax' => '',
			'designstyle' => '',
			'textcolor' => '',
			'textfontsize' => '',
			'email' => '',
			'iconstyle' => '',
			'iconcolor' => '',
			'customrowone' => '',
			'customrowoneicon' => '',
			'customrowtwo' => ''
		), $atts
	);
	
	$output = '';

	if( !empty( $atts['designstyle'] ) ) {
		$designstyle = ' ' . esc_attr( $atts['designstyle'] );
	} else {
		$designstyle = "";
	}

	if( !empty( $atts['iconstyle'] ) ) {
		$iconstyle = ' ' . esc_attr( $atts['iconstyle'] );
	} else {
		$iconstyle = "";
	}
	
	if( !empty( $atts['textcolor'] ) ) {
		$textcolor = 'color:' . esc_attr( $atts['textcolor'] ) . ';';
	}
	else {
		$textcolor ="";
	}

	if( !empty( $atts['textfontsize'] ) ) {
		$textfontsize = 'font-size:' . esc_attr( $atts['textfontsize'] ) . ';';
	}
	else {
		$textfontsize ="";
	}
	
	if( !empty( $atts['textcolor'] ) or !empty( $atts['textfontsize'] ) ) {
		$textStyle = ' style="' . $textcolor . $textfontsize . '"';
	} else {
		$textStyle = "";
	}

		if( !empty( $atts['iconcolor'] ) ) {
			$iconcolor = 'color:' . esc_attr( $atts['iconcolor'] ) . ';';
		}
		else {
			$iconcolor ="";
		}
	
		if( !empty( $atts['iconcolor'] ) ) {
			$iconStyle = ' style="' . $iconcolor . '"';
		}
		else {
			$iconStyle ="";
		}
	
	if( !empty( $atts['customrowone'] ) or !empty( $atts['customrowtwo'] ) or !empty( $atts['address'] ) or !empty( $atts['phone'] ) or !empty( $atts['mobilephone'] ) or !empty( $atts['fax'] ) or !empty( $atts['email'] ) ) {
		$output .= '<div class="contact-information' . $designstyle . $iconstyle . '">';
		
			if( !empty( $atts['address'] ) ) {
				$output .= '<div class="contact-information-row">';
					$output .= '<div class="contact-information-icon"><i class="fa fa-map-marker"' . $iconStyle . '></i></div>';
					$output .= '<div class="contact-information-content"' . $textStyle . '><strong>' . esc_html__( "Address:", 'translogistic' ) . '</strong>' . esc_attr( $atts['address'] ) . '</div>';
				$output .= '</div>';
			}
			
			if( !empty( $atts['phone'] ) ) {
				$output .= '<div class="contact-information-row">';
					$output .= '<div class="contact-information-icon"><i class="fa fa-phone"' . $iconStyle . '></i></div>';
					$output .= '<div class="contact-information-content"' . $textStyle . '><strong>' . esc_html__( "Telephone:", 'translogistic' ) . '</strong>' . esc_attr( $atts['phone'] ) . '</div>';
				$output .= '</div>';
			}
			
			if( !empty( $atts['mobilephone'] ) ) {
				$output .= '<div class="contact-information-row">';
					$output .= '<div class="contact-information-icon"><i class="fa fa-mobile"' . $iconStyle . '></i></div>';
					$output .= '<div class="contact-information-content"' . $textStyle . '><strong>' . esc_html__( "Mobile Phone:", 'translogistic' ) . '</strong>' . esc_attr( $atts['mobilephone'] ) . '</div>';
				$output .= '</div>';
			}
			
			if( !empty( $atts['fax'] ) ) {
				$output .= '<div class="contact-information-row">';
					$output .= '<div class="contact-information-icon"><i class="fa fa-fax"' . $iconStyle . '></i></div>';
					$output .= '<div class="contact-information-content"' . $textStyle . '><strong>' . esc_html__( "Fax:", 'translogistic' ) . '</strong>' . esc_attr( $atts['fax'] ) . '</div>';
				$output .= '</div>';
			}
			
			if( !empty( $atts['email'] ) ) {
				$output .= '<div class="contact-information-row">';
					$output .= '<div class="contact-information-icon"><i class="fa fa-envelope"' . $iconStyle . '></i></div>';
					$output .= '<div class="contact-information-content"' . $textStyle . '><strong>' . esc_html__( "E-Mail:", 'translogistic' ) . '</strong>' . esc_attr( $atts['email'] ) . '</div>';
				$output .= '</div>';
			}
			
			if( !empty( $atts['customrowone'] ) or !empty( $atts['customrowtwo'] ) ) {
				$allowed_html = array ( 'strong' => array() );
				$output .= '<div class="contact-information-row contact-information-custom-row">';
					$output .= '<div class="contact-information-icon"><i class="fa fa-' . esc_attr( $atts['customrowoneicon'] ) . '"' . $iconStyle . '></i></div>';
					$output .= '<div class="contact-information-content"' . $textStyle . '>';
					if( !empty( $atts['customrowone'] ) ) {
						$output .= wp_kses( $atts['customrowone'] , $allowed_html );
					}
					if( !empty( $atts['customrowtwo'] ) ) {
						$output .= '<br/>';
						$output .= wp_kses( $atts['customrowtwo'] , $allowed_html );
					}
					$output .= '</div>';
				$output .= '</div>';
			}
			
		$output .= '</div>';
	}

	return $output;
}
add_shortcode("contact_information", "contact_information_shortcode");

if(function_exists('vc_map')){
	vc_map( array(
		"name" => esc_html__( "Contact Information", 'translogistic' ),
		"base" => "contact_information",
		"class" => "",
		"category" => esc_html__( "Translogistic Theme", 'translogistic' ),
		"icon" => get_template_directory_uri().'/assets/img/icons/contact-information.png',
		"description" =>esc_html__( 'Contact information widget.','translogistic' ),
		"params" => array(
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Address",'translogistic' ),
				"description" => esc_html__( "You can enter the address.",'translogistic' ),
				"param_name" => "address",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Phone",'translogistic' ),
				"description" => esc_html__( "You can enter the phone number.",'translogistic' ),
				"param_name" => "phone",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Mobile Phone",'translogistic' ),
				"description" => esc_html__( "You can enter the mobile phone number.",'translogistic' ),
				"param_name" => "mobilephone",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Fax",'translogistic' ),
				"description" => esc_html__( "You can enter the fax number.",'translogistic' ),
				"param_name" => "fax",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Email",'translogistic' ),
				"description" => esc_html__( "You can enter the email address.",'translogistic' ),
				"param_name" => "email",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Row One Text",'translogistic' ),
				"description" => esc_html__( "You can enter the text.",'translogistic' ),
				"param_name" => "customrowone",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Row Two Text",'translogistic' ),
				"description" => esc_html__( "You can enter the text.",'translogistic' ),
				"param_name" => "customrowtwo",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Row Icon",'translogistic' ),
				"description" => esc_html__( "You can enter the icon name. List of the icons is available in the documentation file. Example: edge, automobile, bel-o.", 'translogistic' ),
				"param_name" => "customrowoneicon",
				"value" => "",
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Style",'translogistic' ),
				"description" => esc_html__( "You can select the design style.",'translogistic' ),
				"param_name" => "designstyle",
				"value" => array(
					esc_html__( "Theme One Color", 'translogistic' ) => "themeonecolor",
					esc_html__( "Theme Two Color", 'translogistic' ) => "themetwocolor",
					esc_html__( "Theme Three Color", 'translogistic' ) => "themethreecolor",
					esc_html__( "Theme Four Color", 'translogistic' ) => "themefourcolor"
				)
			),
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Icon Style",'translogistic' ),
				"description" => esc_html__( "You can select the icon style.",'translogistic' ),
				"param_name" => "iconstyle",
				"value" => array(
					esc_html__( "Default", 'translogistic' ) => "default",
					esc_html__( "Alternative", 'translogistic' ) => "alternative"
				)
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__( "Custom Text Font Size",'translogistic' ),
				"description" => esc_html__( "You can enter the font size. Example: 15px.",'translogistic' ),
				"param_name" => "textfontsize",
				"value" => ""
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Text Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "textcolor",
				"value" => ""
			),
			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => esc_html__( "Custom Icon Color",'translogistic' ),
				"description" => esc_html__( "Please choose the color.",'translogistic' ),
				"param_name" => "iconcolor",
				"value" => ""
			)
		)
	) );
}
/*------------- CONTACT INFORMATION END -------------*/